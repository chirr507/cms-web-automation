# Robotframework Structure

## Usage

Go to project directory

Command for run Robotframework Script:

  
    robot --outputdir ./Results/Log/ Tests/{FILE_NAME}.robot


### Requirement

Create requirements.txt and copy list below

    Appium-Python-Client==1.0.2
    astroid==2.4.2
    certifi==2020.12.5
    chardet==4.0.0
    collection==0.1.6
    colorama==0.4.4
    decorator==4.4.2
    docutils==0.16
    et-xmlfile==1.0.1
    idna==2.9
    isort==5.7.0
    jdcal==1.4.1
    kitchen==1.2.6
    lazy-object-proxy==1.4.3
    mccabe==0.6.1
    numpy==1.20.1
    openpyxl==3.0.6
    pandas==1.2.2
    Pillow==8.1.0
    Pygments==2.8.0
    pylint==2.6.2
    Pypubsub==4.0.3
    python-dateutil==2.8.1
    pytz==2020.1
    pywin32==300
    PyYAML==5.4.1
    requests==2.25.1
    robotframework==3.2.2
    robotframework-appiumlibrary==1.5.0.7
    robotframework-pythonlibcore==2.2.1
    robotframework-requests==0.8.1
    robotframework-ride==1.7.4.2
    robotframework-seleniumlibrary==5.1.1
    selenium==3.141.0
    six==1.15.0
    toml==0.10.2
    urllib3==1.26.3
    wrapt==1.12.1
    wxPython==4.0.7.post2
    xlrd==2.0.1
    XlsxWriter==1.3.7

Command

    pip install -r /path/to/requirements.txt

