import openpyxl

def open_excel_workbook(filePath):
    return openpyxl.load_workbook(filePath, data_only=True)

def select_excel_worksheet(wb, sheetName):
    return wb[sheetName]

def save_excel_workbook(wb, filePath):
    wb.save(filePath)

def close_excel_workbook(wb):
    wb.close()

def write_cell(ws, row, column, data):
    ws.cell(int(row), int(column)).value = data

def write_cell_by_cell_detail(ws, cellDetail, data): # เขียนดาต้าลงไฟล์ที่มีช่องว่างรอเช่นชื่อไฟล์ upload
    ws.cell(int(cellDetail['row']), int(cellDetail['column'])).value = data

def read_excel_sheet_data_from_file(file, sheetName, cellDetail=False, testcaseId=None, headerRow=1, testcaseColumn=1, startDataRow=None): #อ่านค่าจาก excelแล้วreturnค่าออกไปใส่ตัวแปร
    wb = open_excel_workbook(file)
    ws = select_excel_worksheet(wb, sheetName)
    return read_excel_sheet(ws, cellDetail, testcaseId, headerRow, testcaseColumn, startDataRow)

def read_excel_sheet_data_from_workbook(wb, sheetName, cellDetail=False, testcaseId=None, headerRow=1, testcaseColumn=1, startDataRow=None): 
    ws = select_excel_worksheet(wb, sheetName)
    return read_excel_sheet(ws, testcaseId, headerRow, cellDetail, testcaseColumn, startDataRow)

def read_excel_sheet(ws, cellDetail=False, testcaseId=None, headerRow=1, testcaseColumn=1, startDataRow=None):
    dataArray = []
    headerArray = []
    for i in range(ws.max_column):
        headerCell = ws.cell(headerRow, i+1).value
        if headerCell != None:
            headerCell = headerCell.replace(" ", "_")
            headerArray.append(headerCell)
    for i in range(ws.max_row):
        rowIndex = i + 1
        if rowIndex <= headerRow:
            continue
        else:
            if startDataRow != None:
                if rowIndex < startDataRow:
                    continue
            dataDict = {}
            testcase = ws.cell(rowIndex, testcaseColumn).value
            if testcaseId != None:
                if testcase != testcaseId:
                    continue
            for i in range(len(headerArray)):
                dataCellIndex = i + 1
                headerVal = headerArray[i]
                dataCellVal = ws.cell(rowIndex, dataCellIndex).value
                if dataCellVal == '' or dataCellVal == ' ' or dataCellVal == '/' or dataCellVal == None or dataCellVal == 'None':
                    dataCellVal = ''
                if cellDetail == True:
                    cellDict = {}
                    cellDict["row"] = str(rowIndex)
                    cellDict["column"] = str(dataCellIndex)
                    cellDict["value"] = str(dataCellVal)
                    cellDict["columnName"] = headerVal
                    dataDict[headerVal] = cellDict
                else:
                    dataDict[headerVal] = str(dataCellVal)
            dataDict['Row_Index'] = rowIndex
            dataArray.append(dataDict)
    return dataArray

def find_column_index_in_row(ws, rowIndex, columnName):
    maxCol = ws.max_column
    columnNameSearch = columnName.replace(" ", "_")
    columnNameSearch = columnNameSearch.lower()
    for i in range(maxCol):
        index = i + 1
        columnNameRes = ws.cell(int(rowIndex), index).value
        columnNameRes = columnNameRes.replace(" ", "_")
        columnNameRes = columnNameRes.lower()
        if columnNameRes == columnNameSearch:
            return index
    return "Not found data in column"

def find_row_index_in_column(ws, columnIndex, uniqueKeyVal):
    maxRow = ws.max_row
    for i in range(maxRow):
        index = i + 1
        if ws.cell(index,columnIndex).value == uniqueKeyVal:
            return index
    return "Not found data in row"

def get_last_row(ws):
    return ws.max_row

def get_last_column(ws):
    return ws.max_column

def get_cell_value(ws, row, column):
    return ws.cell(row, column).value