 #coding=utf-8
from random import randint
import os

def generate_thai_citizen_id_number(count=1):
    citizenArr = []
    while len(citizenArr) < count:
        citizenId = generate_thai_citizen()
        print(citizenId)
        if len(citizenId) == 13:
            citizenArr.append(citizenId)
    if len(citizenArr) == 1:
        return citizenArr[0]
    else:
        return citizenArr

def filter_json_array(json_data, uniqueKey, uniqueKeyVal):
    output_json = list(filter(lambda x: x[uniqueKey] in str(uniqueKeyVal), json_data))
    return output_json
    
def filter_json_with_cell_detail(json_data, searchKey, searchValue):
    output_json = list(filter(lambda x: x[searchKey]['value'] in str(searchValue), json_data))
    return output_json

def search_first_in_json_by_key(jsonData, uniqueKey, uniqueKeyVal):
    for element in jsonData:
        if element[uniqueKey] == uniqueKeyVal:
            return element

def generate_thai_citizen():
    randomNum = randint(100000000000, 899999999999)
    totalSum = 0
    calStart = 13
    for element in str(randomNum):
        calDigit = int(element) * calStart
        totalSum = totalSum + calDigit
        calStart = calStart - 1
    modNum = totalSum % 11
    lastDigit = 11 - modNum
    return str(randomNum) + str(lastDigit)


def convert_string_to_number_format(str):
    numberData = str.replace(",", "")
    numberFloat = float(numberData)
    result = "{:,.2f}".format(numberFloat)
    return result

def convert_to_custom_number_format(data):
    floatData = float(data)
    result = "{:,.2f}".format(floatData)
    return result

def get_unique_from_json_array(data, field):
    values = []
    uniqueItem = []
    for i in data:
        if(i[field] not in uniqueItem):
            uniqueItem.append(i[field])
            values.append(i)
    print(uniqueItem)
    return uniqueItem

def get_unique_from_json_array_by_cellDetail(data, field):
    values = []
    uniqueItem = []
    for i in data:
        if(i[field]["value"] not in uniqueItem):
            uniqueItem.append(i[field]["value"])
            values.append(i)
    print(uniqueItem)
    return uniqueItem

def get_file_name_from_path(path):
    return os.path.basename(path)