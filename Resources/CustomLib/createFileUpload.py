#Create program for generate file upload

import xlrd
import datetime
import os


timeStamp = str(datetime.datetime.now()).replace(":","").replace(".","")
testDataFile = r"TestData\TestData.xlsx"
testDataSheetName = "TestData"
layoutWorkbook = r"TestData\LayoutFileUpload.xlsx"
resultFolder =  r"TestData\FileUpload\/" + timeStamp
os.mkdir(resultFolder)  # Create Output Folder

dataFile = xlrd.open_workbook(testDataFile)

#Create space
def textLen(lenT):
    text = str('')
    if lenT < 0:
        text = ""
    else:
        for i in range(lenT):
            text = text + " "

    return text

#Convert number to datetime
def xldate_to_datetime(xldate):

    tempDate = datetime.datetime(1900, 1, 1)
    deltaDays = datetime.timedelta(days=int(xldate-1))
    secs = (int((xldate%1)*86400)-60)
    detlaSeconds = datetime.timedelta(seconds=secs)
    TheTime = (tempDate + deltaDays + detlaSeconds )
    return TheTime.strftime('%d/%m/%Y')

#Create data follow as formate in File Layout
def getDataFormat(data, dataformat):

    dataReturn = str('')
    if dataformat == "DDMMYYYY":
        date = datetime.datetime.strptime(xldate_to_datetime(data), '%d/%m/%Y')
        dataReturn = date.strftime('%d%m%Y')
    elif dataformat == "YYYYMMDD.001":
        date = datetime.datetime.strptime(xldate_to_datetime(data), '%d/%m/%Y')
        dataReturn = date.strftime('%Y%m%d.001')
    elif dataformat == "DD/MM/YYYY":
        date = datetime.datetime.strptime(xldate_to_datetime(data), '%d/%m/%Y')
        dataReturn = date.strftime('%d/%m/%Y')
    elif dataformat == "DDMMYYYYHHMMSS":
        if data == "":
            dataReturn = textLen(len(dataformat))
        else:
            date = datetime.datetime.strptime(xldate_to_datetime(data), '%d/%m/%Y')
            dataReturn = date.strftime('%d%m%Y%H%M%S')
    elif dataformat == "00000000000000000.00":
        dataReturn = str("%.2f" % float(data)).zfill(20)
    elif dataformat == "000000000000000":
        dataReturn = str("%.2f" % float(data)).replace(".","").zfill(15)
    elif dataformat == "000000000000":
        dataReturn = str("%.2f" % float(data)).replace(".","").zfill(12)
    elif dataformat == "000000000.00":
        dataReturn = str("%.2f" % float(data)).zfill(12)
    elif dataformat == "00000000000":
        dataReturn = str(data).zfill(11)
    elif dataformat == "0000000000":
        dataReturn = str(data).zfill(10)
    elif dataformat == "000000":
        dataReturn = str(data).zfill(6)
    elif dataformat == "00000":
        dataReturn = str(data).zfill(5)
    elif dataformat == "00":
        dataReturn = str(data).zfill(2)
    elif dataformat == "Charge":
        if data == "Client":
            dataReturn = "0"
        elif "Bene" in data:
            dataReturn = "1"
        else:
            dataReturn = "2"
    elif dataformat == "DrCr":
        if data == "DDA":
            dataReturn = "D"
        else:
            dataReturn = "C"
        
    return dataReturn

#Create Text Line
def printLine(fileFormat, fileType, row, sumAmount, sumInst, totalInst, totalBatch, totalAmount):
    layoutFile = xlrd.open_workbook(layoutWorkbook)
    sheet = layoutFile.sheet_by_name(fileFormat)
    line = str('')
    for i in range(1, sheet.nrows):

        if sheet.cell_value(i, 0) == fileType:

            if sheet.cell_value(i, 2) != "":
                if sheet.cell_value(i, 8) == "Text":
                    if "Branch" in sheet.cell_value(i, 2) and getTestData(getFieldName("File Format"), row) == "CBOS":
                        line = line + str(getTestData(getFieldName("Bank Beneficiary"), row)) + str(getTestData(sheet.cell_value(i, 2), row)) + textLen(int(sheet.cell_value(i, 5)) - len(str(getTestData(getFieldName("Bank Beneficiary"), row)) + str(getTestData(sheet.cell_value(i, 2), row))))
                    else:
                        line = line + str(getTestData(sheet.cell_value(i, 2), row)) + textLen(int(sheet.cell_value(i, 5)) - len(str(getTestData(sheet.cell_value(i, 2), row))))

                elif sheet.cell_value(i, 2) == getFieldName("SumInst"):
                    line = line + getDataFormat(sumInst, sheet.cell_value(i, 8))
                elif sheet.cell_value(i, 2) == getFieldName("SumAmount"):
                    line = line + getDataFormat(sumAmount, sheet.cell_value(i, 8))
                elif sheet.cell_value(i, 2) == getFieldName("TotalInst"):
                    line = line + getDataFormat(totalInst, sheet.cell_value(i, 8))
                elif sheet.cell_value(i, 2) == getFieldName("TotalBatch"):
                    line = line + getDataFormat(totalBatch, sheet.cell_value(i, 8))
                elif sheet.cell_value(i, 2) == getFieldName("TotalAmount"):
                    line = line + getDataFormat(totalAmount, sheet.cell_value(i, 8))
                elif sheet.cell_value(i, 2) == "Today":
                    if sheet.cell_value(i, 8) == "YYYYMMDD.001":
                        date = datetime.datetime.now()
                        line = line + date.strftime('%Y%m%d.001')
                    if sheet.cell_value(i, 8) == "DD/MM/YYYY":
                        date = datetime.datetime.now()
                        line = line + date.strftime('%d/%m/%Y')
                else:

                    line = line + getDataFormat(getTestData(sheet.cell_value(i, 2), row), sheet.cell_value(i, 8))
            elif sheet.cell_value(i, 7) != "":
                
                if sheet.cell_value(i, 8) == "Text":
                    line = line + str((sheet.cell_value(i, 7))) + textLen(int(sheet.cell_value(i, 5)) - len(str(sheet.cell_value(i, 7))))
                else:
                    line = line + getDataFormat(sheet.cell_value(i, 7), sheet.cell_value(i, 8))
                
            else:
                line = line + textLen(int(sheet.cell_value(i, 5)))
    
    return line

#Get data from TestData.xls
def getTestData(fieldName, row):
    
    sheet = dataFile.sheet_by_name(testDataSheetName)
    rData = str('')
    for i in range(0, sheet.ncols):
        if sheet.cell_value(0, i) == fieldName:
            rData = sheet.cell_value(row, i)
            break
        
    return rData

#Get field name from TestData.xls
def getFieldName(fieldName):
    
    sheetAppendix = dataFile.sheet_by_name("Appendix")
    returnField = str('')

    for i in range(1, sheetAppendix.nrows):
        if sheetAppendix.cell_value(i, 0) == fieldName:
            returnField = sheetAppendix.cell_value(i, 1)
            break
        
    return returnField

#Summary amount of transactions by batch    
def calBatch(startLine, fileFormat, batchRef, myProduct, account, effDate, chargeTo):
    
    sheetAppendix = dataFile.sheet_by_name("Appendix")
    sheetData = dataFile.sheet_by_name(testDataSheetName)

    sumAmount = 0
    sumInst = 0
    newLine = startLine

    for i in range(startLine, sheetData.nrows):

        if fileFormat == "MyFormat" and fileFormat == getTestData(getFieldName("File Format"), i) and batchRef == getTestData(getFieldName("Batch Ref."), i) and myProduct == getTestData(getFieldName("Product"), i) and account == getTestData(getFieldName("Account"), i) and effDate == getDataFormat(getTestData(getFieldName("Eff. Date"), i), "DD/MM/YYYY"):
            sumInst = sumInst + 1
            newLine = newLine + 1
            sumAmount = sumAmount + getTestData(getFieldName("Amount"), i)
            
        elif fileFormat == "CBOS" and fileFormat == getTestData(getFieldName("File Format"), i) and batchRef == getTestData(getFieldName("Batch Ref."), i):
            sumInst = sumInst + 1
            newLine = newLine + 1
            sumAmount = sumAmount + getTestData(getFieldName("Amount"), i)
            
        elif fileFormat == "RTP" and fileFormat == getTestData(getFieldName("File Format"), i) and batchRef == getTestData(getFieldName("Batch Ref."), i) and account == getTestData(getFieldName("Account"), i):
            sumInst = sumInst + 1
            newLine = newLine + 1
            sumAmount = sumAmount + getTestData(getFieldName("Amount"), i)
            
        elif fileFormat == "F320" and fileFormat == getTestData(getFieldName("File Format"), i) and myProduct == getTestData(getFieldName("Product"), i) and effDate == getDataFormat(getTestData(getFieldName("Eff. Date"), i), "DD/MM/YYYY") and chargeTo == getTestData(getFieldName("Charge To"), i):
            sumInst = sumInst + 1
            newLine = newLine + 1
            sumAmount = sumAmount + getTestData(getFieldName("Amount"), i)
        else:
            break
    
    return ["%.2f" % sumAmount, sumInst, newLine]

#Summary batch and amount of all file format (For CBOS)
def sumBatch(fileFormat, startLine):

    
    sheet = dataFile.sheet_by_name(testDataSheetName)

    endLine = startLine
    totalBatch = 0
    totalInst = 0
    totalAmount = 0.00
            
    while endLine < sheet.nrows:

        if getTestData(getFieldName("File Format"), endLine) == fileFormat:

            getBatch = calBatch(endLine, fileFormat, getTestData(getFieldName("Batch Ref."), endLine), "", "", 0, "")

            totalBatch = totalBatch + 1
            totalInst = totalInst + getBatch[1]
            totalAmount = totalAmount + float(getBatch[0])
            
            endLine = getBatch[2] - 1
            
        endLine = endLine + 1

    return [totalInst, totalBatch, "%.2f" % totalAmount]

#Check file format in TestData.xls
def findFormat(fileFormat, startLine):

    
    sheet = dataFile.sheet_by_name(testDataSheetName)
    result = 0

    for i in range(startLine, sheet.nrows):
        if(getTestData(getFieldName("File Format"), i)) == fileFormat:
            result = 1
            break

    return result

#Create File Upload by Format
def createFileUpload():

    
    sheet = dataFile.sheet_by_name(testDataSheetName)
    log = open("log.txt", "w")

    if findFormat("MyFormat", int(getFieldName("StartLine")) - 1) == 1:
        MyFUpload = open(resultFolder + "\MyFo_" + timeStamp +".txt", "w")
        MyFUpload.write(printLine("MyFormat", "F", 0, 0, 0, 0, 0, 0) + "\n")
    if findFormat("CBOS", int(getFieldName("StartLine")) - 1) == 1:
        CBOSUpload = open(resultFolder + "\CBOS_" + timeStamp +".001", "w")
        getData = sumBatch("CBOS", int(getFieldName("StartLine")) - 1)
        CBOSUpload.write(printLine("CBOS", "H", 0, 0, 0, 0, getData[1], getData[2]) + "\n")
    
    endLine = int(getFieldName("StartLine")) - 1
            
    while endLine < sheet.nrows:

        if getTestData(getFieldName("File Format"), endLine) != "":
            
            getBatch = calBatch(endLine, getTestData(getFieldName("File Format"), endLine), getTestData(getFieldName("Batch Ref."), endLine), getTestData(getFieldName("Product"), endLine), getTestData(getFieldName("Account"), endLine), getDataFormat(getTestData(getFieldName("Eff. Date"), endLine), "DD/MM/YYYY"), getTestData(getFieldName("Charge To"), endLine))

            if getTestData(getFieldName("File Format"), endLine) == "MyFormat":

                MyFUpload.write(printLine("MyFormat", "B", endLine, float(getBatch[0]), getBatch[1], 0, 0, 0) + "\n")
        
                for i in range(endLine, getBatch[2]):

                    MyFUpload.write(printLine("MyFormat", "T", i, 0, 0, 0, 0, 0) + "\n")
                    log.write("Line " + str(i + 1) + ": Generate File Completed\n")
                    
                    if getTestData("WHT", i) == "Y":
                        MyFUpload.write(printLine("MyFormat", "W", i, 0, 0, 0, 0, 0) + "\n")

                    if getTestData("Invoice", i) == "Y":
                        MyFUpload.write(printLine("MyFormat", "I", i, 0, 0, 0, 0, 0) + "\n")
                        MyFUpload.write(printLine("MyFormat", "V", i, 0, 0, 0, 0, 0) + "\n")

                endLine = getBatch[2]

            elif getTestData(getFieldName("File Format"), endLine) == "CBOS":
            
                CBOSUpload.write(printLine("CBOS", "P", endLine, float(getBatch[0]), getBatch[1], 0, 0, 0) + "\n")

                for i in range(endLine, getBatch[2]):

                    CBOSUpload.write(printLine("CBOS", "I", i, 0, 0, 0, 0, 0) + "\n")
                    log.write("Line " + str(i + 1) + ": Generate File Completed\n")
                    
                    if getTestData("WHT", i) == "Y":
                        CBOSUpload.write(printLine("CBOS", "E", i, 0, 0, 0, 0, 0) + "\n")

                endLine = getBatch[2]

            elif getTestData(getFieldName("File Format"), endLine) == "RTP":

                RTPUpload = open(resultFolder + "\RTPF_" + timeStamp +".txt", "w")
                RTPUpload.write(printLine("RTP", "F", 0, 0, 0, 0, 0, 0) + "\n")
                RTPUpload.write(printLine("RTP", "B", endLine, float(getBatch[0]), getBatch[1], 0, 0, 0) + "\n")
        
                for i in range(endLine, getBatch[2]):

                    RTPUpload.write(printLine("RTP", "T", i, 0, 0, 0, 0, 0) + "\n")
                    log.write("Line " + str(i + 1) + ": Generate File Completed\n")
                    
                RTPUpload.close()
                endLine = getBatch[2]

            elif getTestData(getFieldName("File Format"), endLine) == "F320":

                chargeTo = ""
                if getTestData(getFieldName("Charge To"), endLine) == "Client":
                    chargeTo = "OUR_"
                elif "Bene" in getTestData(getFieldName("Charge To"), endLine):
                    chargeTo = "BEN_"
                else:
                    chargeTo = "SHARE_"
                    
                F320Upload = open(resultFolder + "\F320_" + chargeTo + timeStamp +".txt", "w")
                F320Upload.write(printLine("F320", "1", endLine, float(getBatch[0]), getBatch[1], 0, 0, 0) + "\n")
        
                for i in range(endLine, getBatch[2]):

                    F320Upload.write(printLine("F320", "2", i, 0, 0, 0, 0, 0) + "\n")
                    log.write("Line " + str(i + 1) + ": Generate File Completed\n")

                F320Upload.write(printLine("F320", "3", endLine, 0, getBatch[1], 0, 0, 0) + "\n")    
                F320Upload.close()
                endLine = getBatch[2]
                
            else:

                log.write("Line " + str(endLine + 1) + ": Incorrect File Format Name\n")
                endLine = getBatch[2] + 1

        else:

            log.write("Line " + str(endLine + 1) + ": Not Specific File Format\n")
            endLine = endLine + 1
            
    if findFormat("MyFormat", int(getFieldName("StartLine")) - 1) == 1:
        MyFUpload.close()
        return resultFolder
    if findFormat("CBOS", int(getFieldName("StartLine")) - 1) == 1:
        getData = sumBatch("CBOS", int(getFieldName("StartLine")) - 1)
        CBOSUpload.write(printLine("CBOS", "T", 0, 0, 0, getData[0], getData[1], getData[2]) + "\n")
        CBOSUpload.close()
        return resultFolder
    if findFormat("RTP", int(getFieldName("StartLine")) - 1) == 1:
        RTPUpload.close()
        return resultFolder
filepath = createFileUpload()
print(filepath)  
createFileUpload()
