*** Keywords ***
F_CASH_Open_Cashlink_Website
    C - Open Browser Ignore Certificate  ${environment.${defaultEnvironment}.URL}  headless=${headlessFlag}

F_CASH_RTP_Get All Username and Password
    ${loginData}  Read Excel Sheet Data From_File  TestData\\CMS_WEB_SignOn.xlsx  Auth
    ${makerData}  Filter Json Array  ${loginData}  ID  ${configurationsData}[MakerID][value]
    ${checkerData}  Filter Json Array  ${loginData}  ID  ${configurationsData}[CheckerID][value]
    Set Suite Variable  ${makerData}  ${makerData}[0]
    Set Suite Variable  ${checkerData}  ${checkerData}[0]

F_CASH_Get All Username and Password
    ${loginData}  Read Excel Sheet Data From_File  TestData\\CMS_WEB_SignOn.xlsx  Auth
    ${makerData}  Filter Json Array  ${loginData}  ID  ${cmsData}[MakerID][value]
    ${checkerData}  Filter Json Array  ${loginData}  ID  ${cmsData}[CheckerID][value]
    Set Suite Variable  ${makerData}  ${makerData}[0]
    Set Suite Variable  ${checkerData}  ${checkerData}[0]

F_CASH_eChanel All Username and Password
    ${loginData}  Read Excel Sheet Data From_File  TestData\\CMS_WEB_SignOn.xlsx  Auth
    ${makerData}  Filter Json Array  ${loginData}  ID  ${eChanelData}[MakerID][value]
    ${checkerData}  Filter Json Array  ${loginData}  ID  ${eChanelData}[CheckerID][value]
    Set Suite Variable  ${makerData}  ${makerData}[0]
    Set Suite Variable  ${checkerData}  ${checkerData}[0]

F_CASH_Get Data Start Row
    [Documentation]  Get Start row from TestData.xlsx at Appendix Sheet (StartLine)
    ${wb}  Open Excel Workbook  TestData\\TestData.xlsx
    ${ws}  Select Excel Worksheet  ${wb}  Appendix
    ${startDataRow}  Get Cell Value  ${ws}  ${15}  ${2}
    Set Suite Variable  ${startDataRow}  ${startDataRow}
    Close Excel Workbook  ${wb} 

F_RTP_Get Data Proxy ID
    [Documentation]  Get Biller ID row from TestData.xlsx at TestData Sheet 
    ${wb}  Open Excel Workbook  TestData\\TestData.xlsx
    ${ws}  Select Excel Worksheet  ${wb}  TestData
    ${proxyIdData}  Get Cell Value  ${ws}  ${startDataRow}  ${18}
    Set Suite Variable  ${proxyIdData}    ${proxyIdData}
    Close Excel Workbook  ${wb} 

F_RTP_Get Data Product Config
    [Documentation]  Get Biller ID row from TestData.xlsx at TestData Sheet 
    ${wb}  Open Excel Workbook  TestData\\CMS_WEB_Product_Config.xlsx
    ${ws}  Select Excel Worksheet  ${wb}  Setup
    ${rows}  CustomExcelLibrary.get_last_row  ${ws}
    FOR  ${i}  IN RANGE  2  ${rows}   
        ${clientData}       CustomExcelLibrary.get_cell_value       ${ws}  ${i}  ${1}
        ${productData}      CustomExcelLibrary.get_cell_value       ${ws}  ${i}  ${2}
        ${autoSubmitData}   CustomExcelLibrary.get_cell_value       ${ws}  ${i}  ${5}
        ${autoAuthData}     CustomExcelLibrary.get_cell_value       ${ws}  ${i}  ${6}
        ${autoSend}         CustomExcelLibrary.get_cell_value       ${ws}  ${i}  ${7}
        Set Suite Variable  ${clientData}       ${clientData} 
        Set Suite Variable  ${productData}      ${productData}
        Set Suite Variable  ${autoSubmitData}   ${autoSubmitData}
        Set Suite Variable  ${autoAuthData}     ${autoAuthData}
        Set Suite Variable  ${autoSend}         ${autoSend}
        

        IF  '${productData}' == 'RTPSEND' and '${makerData}[CLIENT]' == '${clientData}'
            Log To Console  found setup data of : ${productData} 
            Log To Console  Auto Submit :${autoSubmitData} / Auto Auth :${autoAuthData} / Auto Send :${autoSend}
            Set Suite Variable  ${autoSubmitData}   ${autoSubmitData}
            Set Suite Variable  ${autoAuthData}     ${autoAuthData}
            Set Suite Variable  ${autoSend}         ${autoSend}
            Exit For Loop  
        
        END 
    END
    Close Excel Workbook  ${wb} 

F_CASH_Get Data Product Config
    [Documentation]  Get Biller ID row from TestData.xlsx at TestData Sheet 
    [Arguments]     ${bankProduct}
    ${wb}  Open Excel Workbook  TestData\\CMS_WEB_Product_Config.xlsx
    ${ws}  Select Excel Worksheet  ${wb}  Setup
    ${rows}  CustomExcelLibrary.get_last_row  ${ws}
    FOR  ${i}  IN RANGE  2  ${rows}   
        ${clientData}       CustomExcelLibrary.get_cell_value       ${ws}  ${i}  ${1}
        ${productData}      CustomExcelLibrary.get_cell_value       ${ws}  ${i}  ${2}
        ${autoSubmitData}   CustomExcelLibrary.get_cell_value       ${ws}  ${i}  ${5}
        ${autoAuthData}     CustomExcelLibrary.get_cell_value       ${ws}  ${i}  ${6}
        ${autoSend}         CustomExcelLibrary.get_cell_value       ${ws}  ${i}  ${7}
        Set Suite Variable  ${clientData}       ${clientData} 
        Set Suite Variable  ${productData}      ${productData}
        Set Suite Variable  ${autoSubmitData}   ${autoSubmitData}
        Set Suite Variable  ${autoAuthData}     ${autoAuthData}
        Set Suite Variable  ${autoSend}         ${autoSend}
        

        IF  '${productData}' == '${bankProduct}' and '${makerData}[CLIENT]' == '${clientData}'
            Log To Console  Found setup data of product : ${productData} 
            Log To Console  Auto Submit :${autoSubmitData} / Auto Auth :${autoAuthData} / Auto Send :${autoSend}
            Set Suite Variable  ${autoSubmitData}   ${autoSubmitData}
            Set Suite Variable  ${autoAuthData}     ${autoAuthData}
            Set Suite Variable  ${autoSend}         ${autoSend}
            Exit For Loop  
        
        END 
    END
    Close Excel Workbook  ${wb} 
F_CASH_Login Cashlink Maker
    [Documentation]  Login Cashlink Website
    # Login Cashlink
    C - Input Text  ${CMS_Username}  ${makerData}[USER_NAME]
    C - Input Text  ${CMS_Password}  ${makerData}[PASSWORD]
    C - Input Text  ${CMS_Client}  ${makerData}[CLIENT]
    C - Click Element  ${CMS_Login_BTN}
    ${status}  Run Keyword And Return Status  Wait Until Element Is Visible  //*[@id="messageArea"]/ul/li[text()="user already logged on"]  timeout=3
    IF  ${status} == ${True}
        Log To Console  user already logged on
        
    END

F_CASH_Login Cashlink Checker
    [Documentation]  Login Cashlink Website
    # Login Cashlink
    C - Input Text  ${CMS_Username}  ${checkerData}[USER_NAME]
    C - Input Text  ${CMS_Password}  ${checkerData}[PASSWORD]
    C - Input Text  ${CMS_Client}  ${checkerData}[CLIENT]
    C - Click Element  ${CMS_Login_BTN}
    ${status}  Run Keyword And Return Status  Wait Until Element Is Visible  //*[@id="messageArea"]/ul/li[text()="user already logged on"]  timeout=3
    IF  ${status} == ${True}
        Log To Console  user already logged on
        
    END
   
F_CASH_Logout RTP and CMS
    F_CASH_Logout RTP 
    C - Click Element  ${CMS_Navbar_Logout_Btn}
    Sleep  1.5
    Close Window 

F_CASH_Logout RTP
    Unselect Frame  
    C - Click Element  ${RTP_Logout_Button}  # Click Logout
    Sleep  1.5
    Close Window 
    Switch Window  MAIN 
    
F_RTP_Logout_2Window
    [Documentation]  Logout RTP and Cashlink Website
    ${filePath}  Join Path  ${TEST_RESULT_PATH}  CMS_TxnData.xlsx
    Save Excel Workbook  ${workbook}  ${filePath}
    Close Excel Workbook  ${workbook}
    #----- Logout 2 windows--------------------------- 
    ${logoutBtnStatus}  Run Keyword And Return Status  Wait Until Element Is Visible  ${RTP_Logout_Button}    timeout=1.5
    Unselect Frame 
    IF  '${logoutBtnStatus}' == '${True}'
        C - Click Element  ${RTP_Logout_Button}
        Close Window 
        Switch Window  MAIN 
    END
    # C - Click Element  ${CMS_Navbar_Logout_Btn}
    ${logoutBtnStatusCMS}  Run Keyword And Return Status  Wait Until Element Is Visible  ${CMS_Navbar_Logout_Btn}    timeout=1.5
    IF  '${logoutBtnStatusCMS}' == '${True}'
        C - Click Element  ${CMS_Navbar_Logout_Btn}
        Close Window 
    END
    Sleep  1  reason=Wait for Logout Success
    #-------------------------------------------------
F_RTP_Logout_1Window
    [Documentation]  Logout RTP and Cashlink Website
    #----- Logout 2 windows---------------------------
    Unselect Frame  
    C - Click Element  ${RTP_Logout_Button} 
    Close Window 
    Switch Window  MAIN   
    
    #-------------------------------------------------

F_CASH_Create Batch Text File
    [Documentation]  Create text file and set 'filePath' and 'fileName' to Suite Variable
    Log To Console   กำลังสร้างไฟล์..... 
    @{fileListArray}  Create List
    IF  '${cmsData}[FileType][value]' == 'Generate'
        ${fileUploadDir}  createFileUpload
        ${filePath}  Join Path  ${EXECDIR}  ${fileUploadDir}
        ${files}  C - Get All File In Path  ${filePath}
        FOR  ${file}  IN  @{files}
            ${fullPath}  Join Path  ${filePath}  ${file}
            Append To List  ${fileListArray}  ${fullPath}
        END
    ELSE
        ${files}  C - Get All File In Path  ${cmsData}[FileLocation][value]
        FOR  ${file}  IN  @{files}
            ${filePath}  Join Path  ${cmsData}[FileLocation][value]  ${file}
            Append To List  ${fileListArray}  ${filePath}
        END
    END
    # Loop ${fileListArray} for create string
    FOR  ${i}  IN  @{fileListArray}
        Set Suite Variable  ${fileListText}  ${i}\n${i}
        Log To Console  ${i}
    END 

    Write Cell By Cell Detail  ${mainWorksheet}  ${cmsData}[FileUpload]  ${fileListText}
    Log To Console   สร้างไฟล์เสร็จแล้ว >.<!!
    Set Suite Variable  @{fileList}  @{fileListArray}

F_CASH_Get CMS Data Output File
    # Read CMS File
    ${workbook}  Open Excel Workbook  TestData\\CMS_Data.xlsx
    ${mainWorksheet}  Select Excel Worksheet  ${workbook}  CMS_Data
    ${echanelWorksheet}  Select Excel Worksheet  ${workbook}  eChannel_Data
    ${fileWorksheet}  Select Excel Worksheet  ${workbook}  Output_File
    ${batchWorksheet}  Select Excel Worksheet  ${workbook}  Output_Batch
    ${insWorksheet}  Select Excel Worksheet  ${workbook}  Output_Ins_Auth
    ${txnWorksheet}  Select Excel Worksheet  ${workbook}  Output_Ins_SentToBank
    ${RTP_Batchsheet}  Select Excel Worksheet  ${workbook}  RTP_Batch
    ${RTP_Instsheet}  Select Excel Worksheet  ${workbook}   RTP_Inst
    ${eChanelData}  Read Excel Sheet  ${echanelWorksheet}  cellDetail=${True}
    ${eChanelDataTxn}  Read Excel Sheet  ${echanelWorksheet}  cellDetail=${True}
    ${cmsData}  Read Excel Sheet  ${mainWorkSheet}  cellDetail=${True}
    ${resultPath}  Join Path  ${TEST_RESULT_PATH}  CMS_TxnData.xlsx
    Set Suite Variable  ${workbook}  ${workbook}
    Set Suite Variable  ${fileWorksheet}  ${fileWorksheet}
    Set Suite Variable  ${mainWorksheet}  ${mainWorksheet}
    Set Suite Variable  ${echanelWorksheet}  ${echanelWorksheet}
    Set Suite Variable  ${batchWorksheet}  ${batchWorksheet}
    Set Suite Variable  ${insWorksheet}  ${insWorksheet}
    Set Suite Variable  ${txnWorksheet}  ${txnWorksheet}
    Set Suite Variable  ${RTP_Batchsheet}  ${RTP_Batchsheet}
    Set Suite Variable  ${RTP_Instsheet}  ${RTP_Instsheet}
    Set Suite Variable  ${eChanelData}  ${eChanelData}[0]
    Set Suite Variable  ${eChanelDataTxn}  ${eChanelDataTxn}
    Set Suite Variable  ${cmsData}  ${cmsData}[0]
    Set Suite Variable  ${resultPath}  ${resultPath}
    
F_CASH__RTP_Get Test Data
    ${workbook}  Open Excel Workbook  TestData\\RTP_PAY_DATA.xlsx
    ${configurationsWorksheet}  Select Excel Worksheet  ${workbook}  Configurations
    ${batchDetailInputWorksheet}  Select Excel Worksheet  ${workbook}  Data
    ${batchDetailOutpurWorksheet}  Select Excel Worksheet  ${workbook}  Output_RTP
    ${cmsOutpurWorksheet}  Select Excel Worksheet  ${workbook}  Output_CMS
    ${configurationsData}  Read Excel Sheet  ${configurationsWorksheet}  cellDetail=${True}
    ${batchDetailInputDataArray}  Read Excel Sheet  ${batchDetailInputWorksheet}  cellDetail=${True}
    ${resultPath}  Join Path  ${TEST_RESULT_PATH}  CMS_TxnData.xlsx
    ${productConfigData}  CustomExcelLibrary.Read Excel Sheet Data From File  TestData\\CMS_WEB_Product_Config.xlsx  Setup
    Set Suite Variable  ${workbook}  ${workbook}
    Set Suite Variable  ${configurationsWorksheet}  ${configurationsWorksheet}
    Set Suite Variable  ${batchDetailInputWorksheet}  ${batchDetailInputWorksheet}
    Set Suite Variable  ${configurationsData}  ${configurationsData}[0]
    Set Suite Variable  ${batchDetailInputDataArray}  ${batchDetailInputDataArray}
    Set Suite Variable  ${batchDetailOutpurWorksheet}  ${batchDetailOutpurWorksheet}
    Set Suite Variable  ${productConfigData}  ${productConfigData}

F_CASH_Get TestINQ Data
    [Documentation]  Get data from excel file and set to suite variable (For inquire Account Summry)
    ${workbook}         open_excel_workbook  TestData\\CMS_TxnData.xlsx    
    ${mainWorksheet}    Select Excel Worksheet  ${workbook}  CMS_Data
    ${batchWorksheet}   Select Excel Worksheet  ${workbook}  Output_Batch
    ${insWorksheet}     Select Excel Worksheet  ${workbook}  Output_Ins_Auth
    ${txnWorksheet}     Select Excel Worksheet  ${workbook}  Output_Ins_SentToBank
    ${RTP_Batchsheet}   Select Excel Worksheet  ${workbook}  RTP_Batch
    ${RTP_Instsheet}    Select Excel Worksheet  ${workbook}   RTP_Inst
    ${todayStmSheet}    Select Excel Worksheet  ${workbook}   Today_Stm 
    ${cmsData}          Read Excel Sheet  ${mainWorksheet}  cellDetail=${True}
    ${instInqData}      Read Excel Sheet  ${insWorksheet}  cellDetail=${True}  
    ${batchInqData}     Read Excel Sheet  ${batchWorksheet}  cellDetail=${True}
    #${resultPath}       Join Path  ${TEST_RESULT_PATH}  CMS_INQ_Result.xlsx 
    
    Set Suite Variable  ${workbook}  ${workbook}
    Set Suite Variable  ${mainWorksheet}  ${mainWorksheet}
    Set Suite Variable  ${batchWorksheet}  ${batchWorksheet}
    Set Suite Variable  ${insWorksheet}  ${insWorksheet}
    Set Suite Variable  ${txnWorksheet}  ${txnWorksheet}
    Set Suite Variable  ${instInqData}   ${instInqData}
    Set Suite Variable  ${batchInqData}  ${batchInqData}
    Set Suite Variable  ${RTP_Batchsheet}  ${RTP_Batchsheet}
    Set Suite Variable  ${RTP_Instsheet}  ${RTP_Instsheet}
    Set Suite Variable  ${cmsData}  ${cmsData}[0]
    Set Suite Variable  ${todayStmSheet}  ${todayStmSheet}
    #Set Suite Variable  ${resultPath}  ${resultPath}

F_CASH_Get StatusINQ Data
    [Documentation]  Get data from excel file and set to suite variable (For inquire Account Summry)
    ${workbook}         open_excel_workbook  ${resultFilePath}   
    ${mainWorksheet}    Select Excel Worksheet  ${workbook}  CMS_Data
    ${batchWorksheet}   Select Excel Worksheet  ${workbook}  Output_Batch
    ${insWorksheet}     Select Excel Worksheet  ${workbook}  Output_Ins_Auth
    ${txnWorksheet}     Select Excel Worksheet  ${workbook}  Output_Ins_SentToBank
    ${RTP_Batchsheet}   Select Excel Worksheet  ${workbook}  RTP_Batch
    ${RTP_Instsheet}    Select Excel Worksheet  ${workbook}   RTP_Inst
    ${todayStmSheet}    Select Excel Worksheet  ${workbook}   Today_Stm 
    ${cmsData}          Read Excel Sheet  ${mainWorksheet}  cellDetail=${True}
    ${instInqData}      Read Excel Sheet  ${insWorksheet}  cellDetail=${True}  
    ${batchInqData}     Read Excel Sheet  ${batchWorksheet}  cellDetail=${True}
    #${resultPath}       Join Path  ${TEST_RESULT_PATH}  CMS_INQ_Result.xlsx 
    
    Set Suite Variable  ${workbook}  ${workbook}
    Set Suite Variable  ${mainWorksheet}  ${mainWorksheet}
    Set Suite Variable  ${batchWorksheet}  ${batchWorksheet}
    Set Suite Variable  ${insWorksheet}  ${insWorksheet}
    Set Suite Variable  ${txnWorksheet}  ${txnWorksheet}
    Set Suite Variable  ${instInqData}   ${instInqData}
    Set Suite Variable  ${batchInqData}  ${batchInqData}
    Set Suite Variable  ${RTP_Batchsheet}  ${RTP_Batchsheet}
    Set Suite Variable  ${RTP_Instsheet}  ${RTP_Instsheet}
    Set Suite Variable  ${cmsData}  ${cmsData}[0]
    Set Suite Variable  ${todayStmSheet}  ${todayStmSheet}
    #Set Suite Variable  ${resultPath}  ${resultPath}
F_CASH_Get CMS Raw Data File
    [Documentation]  Get raw data from excel file and set to suite variable (ไฟล์ TestData สำหรับ
    ...  Create Text File)
    ${rawData}  Read Excel Sheet Data From File  TestData\\TestData.xlsx  TestData  startDataRow=${startDataRow}
    Set Suite Variable  ${rawData}  ${rawData}

Sum Raw Data Amount
    [Arguments]  ${rawDataList}
    ${summary}  Set Variable  ${0}
    FOR  ${item}  IN  @{rawDataList}
        ${amount}  Convert To Number  ${item}[Amount]
        ${summary}  Evaluate  ${summary} + ${amount}
    END
    ${summary}  Convert To Custom Number Format  ${summary}
    [Return]  ${summary}

F_CASH_Upload Text File
    [Arguments]  ${filePath}  ${MapCode}
    #C - Click Element    ${CMS_Sidebar_Payment_Upload}    # Click Payment Sidebar (Expand)
    C - Click Element  ${CMS_Upload_Button}
    C - Select From List By Label  ${CMS_Upload_Client_Select}  ${MapCode}  
    ## should get mapping file by datasheet 
    ## if MyFormate select PMT_CMSBAY - CMS-Payment,CBOS ... เจนไปทำต่อให้เรียกจาก data sheet 

    Choose File  ${CMS_Upload_File_Field}  ${filePath}
    C - Click Element  ${CMS_Upload_Submit}

F_RTP_Upload Text File
    [Arguments]  ${filePath}  ${proxyId}
    
    C - Click Element   ${RTP_Upload_Button}
    C - Select From List By Label   ${RTP_Dropdow_ProxyID}  BILLERID:${proxyId}
    Sleep  3s  
    Choose File  ${RTP_Input_FileUpload}  ${filePath}
    C - Click Element  ${RTP_Submit_Upload_Button}

F_CASH_eChanel Generate Log Transaction
    [Arguments]    ${newLog}
    Set Suite Variable  ${transactionLog}   ${transactionLog}\n${newLog}
    Write Cell By Cell Detail  ${mainWorksheet}   ${eChanelData}[TransactionLog]  ${transactionLog}

F_CASH_Generate Log Transaction
    [Arguments]    ${newLog}
    Set Suite Variable  ${transactionLog}  ${transactionLog}\n${newLog}
    Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[TransactionLog]  ${transactionLog}
    
F_CASH_eChanel Teardown
    Save Excel Workbook  ${workbook}  ${resultPath}
    Close Excel Workbook  ${workbook}
    ${logoutBtnStatus}  Run Keyword And Return Status  Wait Until Element Is Visible  ${CMS_Navbar_Logout_Btn}  timeout=3
    IF  '${logoutBtnStatus}' == '${True}'
        C - Click Element  ${CMS_Navbar_Logout_Btn}
    END
    Close All Browsers   

F_CASH_CMS Teardown
    Save Excel Workbook  ${workbook}  ${resultPath}
    Close Excel Workbook  ${workbook}
    ${logoutBtnStatus}  Run Keyword And Return Status  Wait Until Element Is Visible  ${CMS_Navbar_Logout_Btn}  timeout=3
    IF  '${logoutBtnStatus}' == '${True}'
        C - Click Element  ${CMS_Navbar_Logout_Btn}
    END
F_CASH_CMS TC_STINQ Teardown
    Save Excel Workbook  ${workbook}  ${resultFilePath}
    Close Excel Workbook  ${workbook}
    ${logoutBtnStatus}  Run Keyword And Return Status  Wait Until Element Is Visible  ${CMS_Navbar_Logout_Btn}  timeout=3
    IF  '${logoutBtnStatus}' == '${True}'
        C - Click Element  ${CMS_Navbar_Logout_Btn}
    END
F_CASH_CMS TC_ACCINQ Teardown
    ${filePath}  Join Path  ${TEST_RESULT_PATH}  CMS_INQ_Result.xlsx 
    Set Suite Variable  ${resultFilePath}   ${filePath}
    
    Save Excel Workbook  ${workbook}  ${filePath} 
    Close Excel Workbook  ${workbook}
    ${logoutBtnStatus}  Run Keyword And Return Status  Wait Until Element Is Visible  ${CMS_Navbar_Logout_Btn}  timeout=3
    IF  '${logoutBtnStatus}' == '${True}'
        C - Click Element  ${CMS_Navbar_Logout_Btn}
    END

F_RTP_CMS Teardown
    ${filePath}  Join Path  ${TEST_RESULT_PATH}  Result.xlsx    
    Save Excel Workbook  ${workbook}  ${filePath}
    Close Excel Workbook  ${workbook}
    ${logoutBtnStatus}  Run Keyword And Return Status  Wait Until Element Is Visible  ${RTP_Logout_Button}    timeout=1.5
    IF  '${logoutBtnStatus}' == '${True}'
        C - Click Element  ${RTP_Logout_Button}
        Close Window 
        Switch Window  MAIN 
    END
    F_CASH_Logout CMS

F_CASH_Logout CMS
    ${logoutBtnStatusCMS}  Run Keyword And Return Status  Wait Until Element Is Visible  ${CMS_Navbar_Logout_Btn}    timeout=1.5
    IF  '${logoutBtnStatusCMS}' == '${True}'
        C - Click Element  ${CMS_Navbar_Logout_Btn}
        Close Window 
    END
    Sleep  1  reason=Wait for Logout Success

F_CASH_Click Navbar Payment Menu
    C - Click Element    ${CMS_Navbar_Payment_Menu}    # Click Payment Navbar

F_CASH_Click Navbar Service Menu
    C - Click Element    ${CMS_Navbar_Service_Menu}    # Click Service Navbar

F_CASH_Click link BayIneternal
    C - Click Element    ${CMS_BAY Internal_Menu}      # Click link BayIneternal
    Sleep  3s  
F_CASH_Click Sidebar RTP_Instruction Menu
    C - Click Element   ${RTP_Siderbar_Instruction_Menu}    # Click RTP_Instruction Sidebar (Expand)

F_CASH_Click Sidebar RTP_Receiving Menu
    C - Click Element   ${RTP_Siderbar_Receiving}
    Sleep  4
    F_CMS_RTP_Wait until Loader Is Not Visible

F_CMS_RTP_Wait until Loader Is Not Visible
    Sleep  2s
    ${status}  Run Keyword And Return Status  Wait Until Element Is Not Visible  //div[@class="loader" and @style="display: block;"]  timeout=60
    IF  ${status} != ${True}
        F_CMS_RTP_Wait until Loader Is Not Visible
        ${status}  Run Keyword And Return Status  Wait Until Element Is Visible  ${RTP_Alert_Message}  timeout=3
        IF  ${status} == ${True}
            ${failText}  Get Text  ${RTP_Alert_Message}
            Fail  msg=Failed: ${failText}
        ELSE
            Fail  msg=Failed: Please Contact Administrator
        END
    END


F_CASH_Click Sidebar Payment Menu

    #C - Click Element    ${CMS_Sidebar_Payment}    # Click Payment Sidebar (Expand)
    C - Click Element By Javascript  ${CMS_Sidebar_Payment} 

C - Get All File In Path
    [Arguments]  ${path}
    ${fileArray}  OperatingSystem.List Files In Directory  ${path}
    [Return]  ${fileArray}

F_CASH_Filter File Format
    [Arguments]  ${fileFormatName}

    IF  '${fileFormatName}' == 'MyFo'
        ${rawDataByFormat}  Filter Json Array  ${rawData}  Format_file  MyFormat
        
    ELSE IF  '${fileFormatName}' == 'CBOS'
        ${rawDataByFormat}  Filter Json Array  ${rawData}  Format_file  CBOS
        
    ELSE IF  '${fileFormatName}' == 'F320'
        ${rawDataByFormat}  Filter Json Array  ${rawData}  Format_file  F320
            
    ELSE IF  '${fileFormatName}' == 'RTPF'
        ${rawDataByFormat}  Filter Json Array  ${rawData}  Format_file  RTP
        
    END
    [Return]  ${rawDataByFormat} 

F_CASH_Find_RTP_Header_Index
    [Arguments]  ${requireHeaderIndexParamList}
    # Get Header Index Dict Bill Alert
    
    ${getHeaderCount}  Get Element Count  ${RTP_Result_Table_Header}
    ${getHeaderArray}  Create List  
    ${headerIndexDict}  Create Dictionary  
    FOR  ${headerIndex}  IN RANGE  1  ${getHeaderCount}+${1}
        ${headerText}  C - Get Text  (${RTP_Result_Table_Header})[${headerIndex}]
        IF  '${headerText.strip()}' == ''
            ${headerText}  Set Variable  None
        END
        ${headerFoundStatus}  Run Keyword And Return Status  List Should Contain Value  ${requireHeaderIndexParamList}  ${headerText}
        IF  ${headerFoundStatus} == ${True}
            Set To Dictionary  ${headerIndexDict}  ${headerText}=${headerIndex}
        END
    END
    [Return]  ${headerIndexDict}


F_CASH_Find_Payments_Summary_Table_Header_Index
    ${requireHeaderIndexParamList}  Create List  Batch Reference No  My Product  Entry Date  Total Amount  Status
    ${getHeaderCount}  Get Element Count  //*[@id="content"]//*[@id="gridTable"]/thead/tr/th
    ${getHeaderArray}  Create List  
    ${headerIndexDict}  Create Dictionary  
    FOR  ${headerIndex}  IN RANGE  1  ${getHeaderCount}+${1}
        ${headerText}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/thead/tr/th)[${headerIndex}]
        IF  '${headerText.strip()}' == ''
            ${headerText}  Set Variable  None
        END
        ${headerFoundStatus}  Run Keyword And Return Status  List Should Contain Value  ${requireHeaderIndexParamList}  ${headerText}
        IF  ${headerFoundStatus} == ${True}
            Set To Dictionary  ${headerIndexDict}  ${headerText}=${headerIndex}
        END
    END
    [Return]  ${headerIndexDict}

F_CASH_Click RTP Bill Alert Tab
    C - Click Element  ${RTP_Product_Type_Bill_Alert}

F_CASH_Click RTP Pay Alert Tab
    C - Click Element  ${RTP_Product_Type_Pay_Alert}

F_CASH_Get Pay Alert Table Header
    ${requireHeaderIndexParamList}  Create List  Entry Date  Amount  Status
    ${headerDict}  F_CASH_Find_RTP_Header_Index  ${requireHeaderIndexParamList}
    [Return]  ${headerDict}

F_CASH_Get Bill Alert Table Header
    ${requireHeaderIndexParamList}  Create List  Reference 1  Reference 2  Entry Date  Amount  Status
    ${headerDict}  F_CASH_Find_RTP_Header_Index  ${requireHeaderIndexParamList}
    [Return]  ${headerDict}

F_CASH_Submit Pay Alert Transaction
    F_CASH_Open_Cashlink_Website
    F_CASH_Login Cashlink Maker
    Go to  ${cmsPaymentSummaryUrl}
    ${batchDetailInputDataArray}  Read Excel Sheet  ${batchDetailInputWorksheet}  cellDetail=${True}
    ${paymentSummaryHeaderIndex}  F_CASH_Find_Payments_Summary_Table_Header_Index
    FOR  ${batchDetailData}  IN  @{batchDetailInputDataArray}
        
        ${RunFlag}  Set Variable  ${batchDetailData}[Run_Flag][value]
        ${Pay_Flag}  Set Variable  ${batchDetailData}[Pay_Flag][value]
        ${myProduct}  Set Variable  ${batchDetailData}[Product_Type][value]
        ${payResult}  Set Variable  ${batchDetailData}[Pay_Result][value]
        
        IF  '${myProduct}' == 'Pay Alert'
            ${myProduct}  Set Variable  RTPPAYCT
        ELSE
            ${myProduct}  Set Variable  RTPPAYBP
        END

        ${productConfig}  Filter Json Array  ${productConfigData}   PRODUCT  ${myProduct}  
        ${productConfig}  Filter Json Array  ${productConfig}       CLIENT   ${makerData}[CLIENT] 
        ${productConfig}  Set Variable  ${productConfig}[0]

        ${RunFlag}  Set Variable  ${batchDetailData}[Run_Flag][value]
        ${testCaseRef}  Set Variable  ${batchDetailData}[TCS_Ref][value]
        ${Pay_Flag}  Set Variable  ${batchDetailData}[Pay_Flag][value]
        ${Product_Type}  Set Variable  ${batchDetailData}[Product_Type][value]
        ${Sender_Proxy_Type}  Set Variable  ${batchDetailData}[Sender_Proxy_Type][value]
        ${Sender_Proxy_ID}  Set Variable  ${batchDetailData}[Sender_ProxyID][value]
        ${Receiver_Proxy_Type}  Set Variable  ${batchDetailData}[Receiver_ProxyType][value]
        ${Receiver_Proxy_ID}  Set Variable  ${batchDetailData}[Receiver_ProxyID][value]
        ${Raw_Payment_Amount}  Set Variable  ${batchDetailData}[Amount][value]
        ${Payment_Amount}  Replace String  ${Raw_Payment_Amount}  '  ${EMPTY}  count=-1
        ${Entry_Date}  Set Variable  ${batchDetailData}[EntryDate][value]
        ${Ref1}  Set Variable  ${batchDetailData}[Reference1][value]
        ${Ref2}  Set Variable  ${batchDetailData}[Reference2][value]
        ${Ref3}  Set Variable  ${batchDetailData}[Reference3][value]
        ${PayResult}  Set Variable  ${batchDetailData}[Pay_Result][value]

        IF  '${RunFlag}' == 'Y' and '${PayFlag}' == 'Y' and '${PayResult}' == 'Pass'
            ${foundDataStatus}  Set Variable  ${False}
            ${reloadCount}  Set Variable  ${50}
            FOR  ${reloadIterator}  IN RANGE  0  ${reloadCount}
                ${rowCount}  Get Element Count  //*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')]
                FOR  ${i}  IN RANGE  0  ${rowCount}
                    ${rowIndex}  Evaluate  ${i}+${1}
                    ${batchRefNo}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Batch Reference No]]
                    ${myProductScreen}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[My Product]]
                    ${entryDateScreen}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Entry Date]]
                    ${totalAmount}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Total Amount]]
                    ${status}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Status]]

                    ${reconcileStatus}  Set Variable  ${True}
                    IF  '${Payment_Amount}' == '${totalAmount}' and '${myProduct}' == '${myProductScreen.strip()}'

                        IF  '${Entry_Date}' != ''
                            IF  '${Entry_Date}' == '${entryDateScreen}'
                                ${foundDataStatus}  Set Variable  ${True}
                            ELSE
                                ${foundDataStatus}  Set Variable  ${False}
                            END
                        ELSE
                            ${foundDataStatus}  Set Variable  ${True}
                        END

                        IF  ${foundDataStatus} == ${True}
                            C - Click Element  (//*[@id="gridTable"]/tbody/tr/td[3]/a[@title="View Deposit"])[${rowIndex}]
                            C - Capture Screenshot  Batch_Detail_Screen
                            Sleep  2s
                            C - Click Element  //*[@id="divPayInst"]//table/tbody/tr/td[3]/a[@title="View Instrument"]
                            # Get Data From Batch Screen
                            ${paymentProductScreen}  C - Get Text  ${RTP_Detail_Payment_Product}
                            ${instructionReferenceNoScreen}  C - Get Text  ${RTP_Detail_Instruction_Reference_No}
                            ${debitAccount}  C - Get Text  ${RTP_Detail_Debit_Account}
                            ${paymentAmount}  C - Get Text  ${RTP_Detail_Payment_Amount}
                            ${taxIdScreen}  C - Get Text  ${RTP_Detail_TAXID}
                            ${RTPReferenceID}  C - Get Text  ${RTP_Detail_RTP_Reference_ID}
                            ${debitDate}  C - Get Text  ${RTP_Detail_Debit_Date}
                            ${senderProxyID}  C - Get Text  ${RTP_Detail_Sender_Proxy_ID}
                            ${receivingProxyID}  C - Get Text  ${RTP_Detail_Receiving_Proxy_ID}
                            ${reference1}  C - Get Text  ${RTP_Detail_Reference1}
                            ${reference2}  C - Get Text  ${RTP_Detail_Reference2}
                            ${reference3}  C - Get Text  ${RTP_Detail_Reference3}

                            # Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Debit_Account]  ${debitAccount.strip()}
                            # Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Payment_Date]  ${debitDate.strip()}
                            # Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Instrument_RefNo]  ${instructionReferenceNoScreen.strip()}

                            C - Capture Screenshot  Transaction_Detail_Screen
                            C - Click Element  ${CMS_Ins_Back_Button}
                            F_CMS_RTP_Wait until Loader Is Not Visible

                            # Check Auto Aprrove Config
                            IF  '${productConfig}[AUTO_SUBMIT]' == 'N'
                                ${expectedStatusForSubmit}  Set Variable  For Submit
                                IF  '${status.strip()}' == '${expectedStatusForSubmit}'
                                    C - Click Element  ${CMS_Submit_Btn}  # Click Submit
                                    C - Click Element  ${CMS_Confirm_Auth_Btn}  # Confirm Submit
                                    ${submitMessage}  C - Get Text  ${CMS_Auth_Message}
                                    ${submitStatus}  Run Keyword And Return Status  Should Contain  ${submitMessage}  ${totalAmount}
                                    IF  ${submitStatus} == ${True}
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_Result]  Pass
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_ResultMessage]  Submit Transaction Passed
                                    ELSE
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_Result]  Fail
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_ResultMessage]  Send to Bank Failed - ${submitMessage}
                                    END
                                ELSE
                                    Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_Result]  Fail
                                    Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_ResultMessage]  Auto Approve is '${productConfig}[AUTO_APPROVE]', But Status != ${expectedStatusForSubmit}
                                END
                            ELSE
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_Result]  Skip
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_ResultMessage]  Product Config AUTO_APPROVE is 'Y'
                            END
                            C - Click Element  ${CMS_Batch_Back_Button}
                            # Check Last Status is Send To Bank 
                            
                            Mouse Over  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Status]]
                            C - Capture Screenshot  Transaction_Last_Status
                            ${lastStatus}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Status]]
                            IF  '${lastStatus}' != 'For Submit'
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Overall_Result]  Failed: Expect Last Status is 'Sent to Bank' - Actual status is ${lastStatus}
                                ${reconcileStatus}  Set Variable  ${False}
                            ELSE
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Overall_Result]  Pass: Status as is Expect
                            END
                            Exit For Loop
                        END
                    END
                END

                IF  ${foundDataStatus} == ${True}
                    Exit For Loop  
                ELSE
                    IF  ${reloadIterator} == ${reloadCount}-1
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submi_Result]  Fail
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submi_ResultMessage]  Not Found Transaction on Screen for Submit
                    ELSE
                        Reload Page  
                        Sleep  2s
                    END
                END
            END
        ELSE
            Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submi_Result]  Run flag not equal 'Y' or Pay_Result not equal 'Pass' >> Skip this transaction
        END
    END