*** Keywords ***
C - Suite Setup
    C - Setup Session Number
    C - Setup Log Folder And File
    C - Setup Result Folder
    F_CASH_Get CMS Data Output File
    F_CASH_Get All Username and Password
    F_CASH_Get Data Start Row
    F_CASH_Get CMS Raw Data File
    F_RTP_Get Data Proxy ID
    F_RTP_Get Data Product Config
    
C - Initial Setup
    C - Setup Session Number
    C - Setup Log Folder And File
    C - Setup Result Folder
    
RTP - PAY Setup
    F_CASH__RTP_Get Test Data
    F_CASH_RTP_Get All Username and Password
    F_CASH_Get Data Start Row
    F_RTP_Get Data Proxy ID

INQ - Initial Setup
    F_CASH_Get TestINQ Data
    F_CASH_Get All Username and Password
    F_RTP_Get Data Product Config

INQ - Status Initial Setup
    F_CASH_Get StatusINQ Data
    F_CASH_Get All Username and Password
    F_RTP_Get Data Product Config

C - Initial eChanel Setup
    F_CASH_Get CMS Data Output File
    F_CASH_eChanel All Username and Password
    #F_RTP_Get Data Product Config

C - Suite Teardown
    Run Keyword If  '${teardownBrowser}' == 'Y'  Close All Browsers
    Run Keyword If  '${requireResult}' == 'Y'  CustomExcelLibrary.Save Excel Workbook  ${WORKBOOK}  ${TEST_RESULT_PATH}\\Result.xlsx
    CustomExcelLibrary.Close Excel Workbook  ${WORKBOOK}

C - Setup Session Number
    ${sessionTimeStamp}  C - Get Custom Timestamp
    Set Suite Variable  ${SESSION_TIME_STAMP}  ${sessionTimeStamp}  # Session Timestamp for Create Capture Folder And Result File Name

C - Setup Test Data Workbook
    ${workbook}  Set Variable  ${EXECDIR}\\TestData\\${TestDataFileName}
    ${wb}  CustomExcelLibrary.Open Excel Workbook  ${workbook}
    ${ws}  CustomExcelLibrary.Select Excel Worksheet  ${wb}  ${TestDataSheetName}
    Set Suite Variable  ${WORKBOOK}  ${wb}
    Set Suite Variable  ${WORKSHEET}  ${ws}
    ${testData}  Read Excel Sheet Data From Workbook  ${WORKBOOK}  ${TestDataSheetName}  cellDetail=${True}
    Set Suite Variable  ${ALL_TEST_DATA}  ${testData}

C - Setup Result Folder
    ${resultFolder}  Set Variable  ${EXECDIR}\\Result
    ${status}  Run Keyword And Return Status  Directory Should Exist  ${resultFolder}
    IF  ${status} != ${True}
        Create Directory  ${resultFolder}
    END
    ${resultFolderWithTimeStamp}  Set Variable  ${resultFolder}\\${SESSION_TIME_STAMP}
    Create Directory  ${resultFolderWithTimeStamp}
    Set Suite Variable  ${TEST_RESULT_PATH}  ${resultFolderWithTimeStamp}
    IF  '${captureScreen}' == 'Y'
        Set Suite Variable  ${CAPTURE_PATH}  ${resultFolderWithTimeStamp}\\Capture
        Create Directory  ${CAPTURE_PATH}
        SeleniumLibrary.Set Screenshot Directory  ${CAPTURE_PATH}
    END

C - Test Setup
    [Arguments]  ${testName}
    ${data}  Filter Json With Cell Detail  ${ALL_TEST_DATA}  TestCaseNumber  ${testName}
    Set Suite Variable  ${TEST DATA}  ${data}

C - Write Data To Cell
    [Arguments]  ${cellDetail}  ${data}
    CustomExcelLibrary.Write Cell  ${WORKSHEET}  ${cellDetail}[row]  ${cellDetail}[column]  ${data}

# ====== Web Element Common Keywords
C - Open Browser
    [Arguments]  ${url}
    Create Webdriver    ${DefaultBrowser}    executable_path=Resources\\ChromeDriver\\${driverVersion}\\chromedriver.exe
    Maximize Browser Window  
    Go To    ${url}
    C - Write Log Line  Open ${url} via ${DefaultBrowser}
    [Teardown]  C - Write Log Line  Open ${url} via ${DefaultBrowser}

C - Open Browser Ignore Certificate
    [Arguments]  ${url}  ${headless}=${False}
    ${chrome_options}  Evaluate  sys.modules['selenium.webdriver'].ChromeOptions()  sys, selenium.webdriver
    Run Keyword If  '${headless}' == '${True}'  Call Method  ${chrome_options}  add_argument  headless
    Run Keyword If  '${ignoreCert}' == '${True}'  Call Method  ${chrome_options}  add_argument  ignore-certificate-errors
    Call Method  ${chrome_options}  add_argument  disable-gpu
    ${options}  Call Method  ${chrome_options}  to_capabilities     
    Create Webdriver  ${DefaultBrowser}    executable_path=Resources\\ChromeDriver\\${driverVersion}\\chromedriver.exe    desired_capabilities=${options}
    Go To    ${url}
    # Open Browser  ${url}  browser=${DefaultBrowser}  
    Maximize Browser Window
    [Teardown]  C - Write Log Line  Open ${url} via ${DefaultBrowser}

C - Element Visible Status
    [Arguments]  ${locator}
    ${status}  Run Keyword And Return Status  Wait Until Element Is Visible  ${locator}  timeout=${elementTimeout}
    Run Keyword If  ${status} != ${True}  Run Keywords  C - Write Log Line  Element: ${locator} is not visible on screen in ${elementTimeout} second  AND  Fail
    ...  ELSE  C - Write Log Line  Element: ${locator} is visible on screen

C - Input Text
    [Arguments]  ${locator}  ${text}
    ${status}  C - Element Visible Status  ${locator}
    Run Keyword If  '${text}' == '/'  C - Write Log Line  Skip!! > Input ${text} to Element: ${locator}
    ...  ELSE  Run Keywords  Set Focus To Element  ${locator}  AND  SeleniumLibrary.Input Text  ${locator}  ${text}
    C - Write Log Line  Input ${text} to Element: ${locator}
    

C - Input Text by Cell Detail
    [Arguments]  ${locator}  ${cellDetail}
    ${status}  C - Element Visible Status  ${locator}
    Run Keyword If  '${cellDetail}[value]' == '/'  C - Write Log Line  Skip!! > Input ${cellDetail}[value] to Element: ${locator}
    ...  ELSE IF  '${cellDetail}[value]' == '#'  C - Get Text And Write To Excel  ${locator}  ${cellDetail}
    ...  ELSE  Run Keywords  Set Focus To Element  ${locator}  AND  SeleniumLibrary.Input Text  ${locator}  ${cellDetail}[value]
    C - Write Log Line  Input ${cellDetail}[value] to Element: ${locator}

C - Click Element
    [Arguments]  ${locator}
    ${status}  C - Element Visible Status  ${locator}
    Set Focus To Element  ${locator}
    SeleniumLibrary.Click Element  ${locator}
    C - Write Log Line  Click Element: ${locator}

C - Select From List By Label
    [Arguments]  ${locator}  ${label}  ${writeColumn}=None
    ${status}  C - Element Visible Status  ${locator}
    SeleniumLibrary.Select From List By Label  ${locator}  ${label}
    C - Write Log Line  Select label: ${label} From ${locator}

C - Select From List By Value
    [Arguments]  ${locator}  ${value}  ${writeColumn}=None
    ${status}  C - Element Visible Status  ${locator}
    SeleniumLibrary.Select From List By Value  ${locator}  ${value}
    C - Write Log Line  Select value: ${value} From ${locator}

C - Select Radio Button
    [Arguments]  ${groupName}  ${value}  ${writeColumn}=None
    ${status}  C - Element Visible Status  ${locator}
    SeleniumLibrary.Select Radio Button  ${groupName}  ${value}
    C - Write Log Line  Select radio button: ${value} from group name ${groupName}

C - Select Checkbox
    [Arguments]  ${locator}  ${writeColumn}=None
    ${status}  C - Element Visible Status  ${locator}
    SeleniumLibrary.Select Checkbox  ${locator}
    C - Write Log Line  Select checkbox: ${locator}

C - Get Text
    [Arguments]  ${locator}
    ${status}  C - Element Visible Status  ${locator}
    ${text}  SeleniumLibrary.Get Text  ${locator}
    C - Write Log Line  Get text: ${locator} and result = ${text}
    [Return]  ${text}

C - Get Attribute
    [Arguments]  ${locator}
    ${status}  C - Element Visible Status  ${locator}
    ${attribute}  SeleniumLibrary.Get Element Attribute  ${locator}  class
    C - Write Log Line  Get Attribute: ${locator} and result = ${attribute}
    [Return]  ${attribute}

C - Get Text And Write To Excel
    [Arguments]  ${locator}  ${cellDetail}
    ${status}  C - Element Visible Status  ${locator}
    ${text}  SeleniumLibrary.Get Text  ${locator}
    CustomExcelLibrary.Write Cell  ${WORKSHEET}  ${cellDetail}[row]  ${cellDetail}[column]  ${text}
    C - Write Log Line  Get text: ${locator} and result = ${text}
    [Return]  ${text}

C - Get Value
    [Arguments]  ${locator}
    ${status}  C - Element Visible Status  ${locator}
    ${value}  SeleniumLibrary.Get Value  ${locator}
    C - Write Log Line  Get value: ${locator} and result = ${value}
    [Return]  ${value}

C - Click Element By Javascript
    [Arguments]  ${locator}
    ${status}  C - Element Visible Status  ${locator}
    SeleniumLibrary.Execute Javascript  document.evaluate("${locator}", document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0).click();
    C - Write Log Line  Click Element: ${locator} by Execute Javascript

# ====== Timestamp Common Keywords
C - Get Custom Timestamp
    [Arguments]  ${onlyDate}=${False}
    ${resultFormat}  Run Keyword If  ${onlyDate} == ${False}  Set Variable  %Y%m%d%H%M%S
    ...  ELSE  Set Variable  %Y%m%d
    ${dateTime}  DateTime.Get Current Date  time_zone=local  increment=0  result_format=timestamp  exclude_millis=True
    ${date}  DateTime.Convert Date  ${dateTime}  result_format=${resultFormat}  exclude_millis=True  date_format=%Y-%m-%d %H:%M:%S
    [Return]  ${date}
    
# ====== Capture Common Keywords
C - Create Capture Screen Folder
    ${mainCaptureDir}  Set Variable  ${EXECDIR}\\CaptureScreen
    Set Suite Variable  ${MAIN_CAPTURE_DIR}  ${mainCaptureDir}
    ${result}  Run Keyword And Return Status  OperatingSystem.Directory Should Exist  ${MAIN_CAPTURE_DIR}
    Run Keyword If  ${result} != ${True}  Run Keywords  OperatingSystem.Create Directory  ${MAIN_CAPTURE_DIR}  AND  Sleep  0.5

C - Setup Capture Screen Result Folder
    Run Keyword If  '${captureScreen}' == 'Y'  Run Keywords  C - Create Capture Screen Folder  AND  OperatingSystem.Create Directory  ${MAIN_CAPTURE_DIR}\\${SESSION_TIME_STAMP}  AND  SeleniumLibrary.Set Screenshot Directory  ${MAIN_CAPTURE_DIR}\\${SESSION_TIME_STAMP}

C - Capture Screenshot
    [Arguments]  ${fileName}
    ${timeStamp}  C - Get Custom Timestamp
    ${fullFileName}  Set Variable  ${fileName}_${timeStamp}.png
    Run Keyword If  '${captureScreen}' == 'Y'  Capture Page Screenshot  filename=${fullFileName}
    
C - Capture Element Screenshot  
    [Arguments]  ${locator}  ${fileName}
    ${timeStamp}  C - Get Custom Timestamp
    ${fullFileName}  Set Variable  ${fileName}_${timeStamp}.png
    Run Keyword If  '${captureScreen}' == 'Y'  Capture Element Screenshot  ${locator}  filename=${fullFileName}

# ====== Create log file and write log
C - Setup Log Folder And File
    Run Keyword If  '${writeLogAction}' == 'Y'  Run Keywords  C - Create Log Folder  AND  C - Create Log File

C - Create Log Folder
    ${logDir}  Set Variable  ${EXECDIR}\\LogFile
    Set Suite Variable  ${LOG_DIR}  ${logDir}
    ${result}  Run Keyword And Return Status  OperatingSystem.Directory Should Exist  ${LOG_DIR}
    Run Keyword If  ${result} != ${True}  Run Keywords  OperatingSystem.Create Directory  ${LOG_DIR}  AND  Sleep  0.5

C - Create Log File
    ${logFileName}  C - Get Custom Timestamp  onlyDate=${True}
    Set Suite Variable  ${logFileName}  ${LOG_FILE_NAME}
    ${logFilePath}  Set Variable  ${EXECDIR}\\LogFile\\Log_${LOG_FILE_NAME}.txt
    ${existStatus}  Run Keyword And Return Status  File Should Exist  ${logFilePath}
    Run Keyword If  ${existStatus} == ${False}  Create File  ${logFilePath}
    Set Suite Variable  ${LOG_FILE_PATH}  ${logFilePath}

C - Generate Log Line
    [Arguments]  ${logText}
    ${logTimeStamp}  DateTime.Get Current Date  time_zone=local  increment=0  result_format=timestamp  exclude_millis=False
    ${logLine}  Set Variable  \n[${logTimeStamp}] - ${logText}
    [Return]  ${logLine}

C - Write Log Line
    [Arguments]  ${text}
    ${logLine}  C - Generate Log Line  ${text}
    Run Keyword If  '${writeLogAction}' == 'Y'  Append To File  ${LOG_FILE_PATH}  ${logLine}  encoding=UTF-8

C - Capture Full Page Screen by Headless Chrome
    [Arguments]  ${screenElement}  ${fileName}
    ${size}  SeleniumLibrary.Get Element Size  ${screenElement}
    ${height}  Set Variable  ${size}[1]
    IF  '${headlessFlag}' == 'True'
        SeleniumLibrary.Set Window Size  1920  ${height}
    END
    ${timeStamp}  C - Get Custom Timestamp
    ${fullFileName}  Set Variable  ${fileName}_${timeStamp}.png
    Run Keyword If  '${captureScreen}' == 'Y'  Capture Element Screenshot  ${screenElement}  filename=${fullFileName}
