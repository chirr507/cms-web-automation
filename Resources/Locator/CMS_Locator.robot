# '*************************************************************************************************
#* Date                * Changed By     * Description                           * version
#---------------------------------------------------------------------------------------------------
#* 20/10/2021            Chirayut R.       Initial version                         1.0
#* 28/12/2021            Jenjira H.        Collection Page version                 2.0
#* 08/03/2022            Jenjira H.        Get RTP locator                         3.0
#****************************************************************************************************

#---------------------------------------------------------------------------------------------------
# COMMON Locator 
#---------------------------------------------------------------------------------------------------

*** Variables ***
# E-CHANNEL
${eChanel_Process}                  /html/body/irisresponse/processname   #ECUSTUPLD ,ECESSUPLD
${eChanel_Status}                   /html/body/irisresponse/status   #0 = Success
${eChanel_ServErr}           //*[@id="header"]/h1
${fileNameTXT1}          xpath=//*[@id="fileName"]
${referenceNoTXT}       xpath=//*[@id="referenceNo"]
${seqNoTXT}             xpath=//*[@id="seqNo"]
${DeclarationRefTXT}    xpath=//*[@id="DeclarationRef"]
${DeclarationNoTXT}     xpath=//*[@id="DeclarationNo"]
${branchTXT}            xpath=//*[@id="branch"]
${NameTXT}              xpath=//*[@id="Name"]
${payerAccountNoTXT}    xpath=//*[@id="payerAccountNo"]
${payerBankCodeTXT}     xpath=//*[@id="payerBankCode"]
${payerBankBranchTXT}   xpath=//*[@id="payerBankBranch"]
${PayerTaxNoTXT}        xpath=//*[@id="PayerTaxNo"]
${PayAmountTXT}         xpath=//*[@id="PayAmount"]
${TransmitDateTimeTXT}      xpath=//*[@id="TransmitDateTime"]
${receiverAccountNoTXT}     xpath=//*[@id="receiverAccountNo"]
${receiverBankCodeTXT}      xpath=//*[@id="receiverBankCode"]
${receiverBankBranchTXT}    xpath=//*[@id="receiverBankBranch"]
${receiverAccountName}   //*[@id="receiverAccountName"]
${CompanyNameTXT}       xpath=//*[@id="CompanyName"]
${IssueNoTXT}           xpath=//*[@id="IssueNo"]
${IssueDateTXT}         xpath=//*[@id="IssueDate"]
${FilenameTXT}          xpath=//tr[20]/td[2]/input
${TransactionTypeTXT}       xpath=//*[@id="transactionType"]
${PayerTaxIDTXT}        xpath=//*[@id="Dbtr_Othr_Id1"]
${PayerBranchTXT}       xpath=//*[@id="Dbtr_Othr_Id2"]
${submitBTN}            xpath=//*[@id="submit"]
${resetBTN}             xpath=//*[@id="reset"]
${BROWSER}              chrome
${BASE_URL}             file:///D:/ECustomsTxnSimulator.html
${Dbtr_Othr_Id1}             //*[@id="Dbtr_Othr_Id1"]   #payee Tax
${Dbtr_Othr_Id2}            //*[@id="Dbtr_Othr_Id2"]    #payer BR


#-----------------------------------------------------------------------------------------------------


# Account Menu : Account Summary
#---------------------------------------------------------------------------------------------------

${CMS_Account_Menu}                 //*[@id="mbar_Account"]/a/span[text()="Account"]
${CMS_Account_LinkURL}              https://uat2.krungsribizonline.com/GCPCW/mod_Account.form
${CMS_Sidebar_AccountSummary}       //*[@id="acord_Account"]//a[text()="Account Summary"]
${CMS_PageTitle_ACCO}               //*[@id="pageTitle"]
${CMS_ACCO_Sider_SA}                //p[contains(@class, "acord_grid_head") and text()="Savings Account"]  #//*[@id="br_link"]/p[text()="Savings Account"]   #class="acord_grid_head acord_open" / class="acord_grid_head acord_close"
${CMS_ACCO_Sider_CA}                //p[contains(@class, "acord_grid_head") and text()="Current Account"]
${CMS_ACCO_Table}                   //*[@id="gridTable"]/../div[contains(@style, "display: block;")]/table/tbody/tr
${CMS_Back_Button}                  //*[@id="btnBack"]
${CMS_Select_Account}               //*[@id="accountId"]
${CMS_Select_DateType}              //*[@id="dateType"]
${CMS_Stetment_Table}               //*[@id="gridTable"]/tbody/tr
${CMS_Info_Close}                   //div[@class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable" and contains(@style, "display: block;")]//span[@class="ui-icon ui-icon-closethick"]

#---------------------------------------------------------------------------------------------------

# CMS Auth
${CMS_Username}                     //*[@id="ssoLoginId"]
${CMS_Password}                     //*[@id="txtpassword"]
${CMS_Client}                       //*[@id="client"]
${CMS_Login_BTN}                    //*[@id="login"]
${CMS_Navbar_Logout_Btn}            //*[@id="btnLogOut"]

${CMS_Upload_Table}                 //*[@id="gridTable"]/tbody/tr
${CMS_Batch_Table}                  //*[@id="gridTable"]//tbody/tr
                                    
${CMS_Upload_Button}                //*[@id="btnUpld"]
${CMS_Upload_Client_Select}         //*[@id="clientMapCode"]
${CMS_Upload_File_Field}            //*[@id="file"]
${CMS_Upload_Submit}                //*[@id="uploadInstrumentFile"]/..//button/span[text()="Upload"]


${CMS_Batch_Submit_All_Button}      //*[@id="btnClose" and text()="Submit Transaction"]
${CMS_Batch_Submit_All_OK_Button}   //button/span[text()="Ok"]

${CMS_Ins_Title}                    //*[@id="pageTitle" and contains(text(), "Instrument View")]
${CMS_Ins_DebitAcc_Field}           //*[@id="lbl_accountNo"]
${CMS_Ins_Back_Button}              //*[@id="btnViewBack"]


${CMS_Container}                    //body

${CMS_Auth_Btn}                     //*[@id="btnAuth"]
${CMS_Submit_Btn}                   //*[@id="btnSubmit"]
${CMS_Auth_Confirm_Btn}             //button/span[text()="Ok"]
${CMS_Auth_SummitAll_Msg}           //*[@id="messageArea"]
${CMS_SendAll_Btn}                  //*[@id="btnMySend"]  # for my send
${CMS_SendAll_Confirm}              //button/span[text()="Ok"]
${CMS_Send_Btn}                     //*[@id="btnSend"]  # for my send


${CMS_Batch_Back_Button}            //*[@id="btnBack"]

#---------------------------------------------------------------------------------------------------
# PAYMENT Menu
#---------------------------------------------------------------------------------------------------

${CMS_Navbar_Payment_Menu}          //*[@id="mbar_Payments"]
${CMS_Sidebar_Payment}              //*[@id='acord_Payments']
${CMS_Sidebar_Payment_Upload}       //*[@id="acord_Payments"]//a[contains(text(), 'Payment File Upload')]
${CMS_Sidebar_Payment_Summary}      //*[@id="acord_Payments"]//a[contains(text(), 'Payments Summary')]



#---------------------------------------------------------------------------------------------------
# COLLECTION Menu
#---------------------------------------------------------------------------------------------------
${CMS_Navbar_Collection_Menu}           //*[@id="mbar_Collections"]
${CMS_Sidebar_Collection}               //*[@id="acord_Collections"]
${CMS_Sidebar_Collection_Summary}       //*[@id="acord_Collections"]//a[contains(text(), 'Collections Summary')]
${CMS_Sidebar_Collection_Upload}        //*[@id="acord_Collections"]//a[contains(text(), 'Collection File Upload')]
${CMS_URL_Collection_Summary}           https://uat2.krungsribizonline.com/GCPCW/showDepositList.form
${CMS_URL_Collection_Upload}            https://uat2.krungsribizonline.com/GCPCW/collTaskStatusList.form

${CMS_BatchSummary_Submit}              //*[@id="btnSubmit"]
${CMS_BatchSummary_Authorize}           //*[@id="btnAuth"]
${CMS_BatchSummary_Send}                //*[@id="btnSend"]
${CMS_BatchSummary_Confirm_OK}          //button/span[text()="Ok"]

${CMS_Ins_CreditAcc}                    //*[@id="divProduct"]/div[2]//span[@class="w24 rounded inline disabled"]
${CMS_DDA_Button_Back}                  //*[@id="btnBack"]      

${CMS_DDA_InsAuth_Button}               //*[@id="btnMyAuth"]
${CMS_DDA_InsSend_Btn}                  //*[@id="btnSend"]


#---------------------------------------------------------------------------------------------------
# RTP Menu
#---------------------------------------------------------------------------------------------------
${RTP_PageBody}                         //body      #[@id="pageBody"]
${CMS_Navbar_Service_Menu}              //*[@id="mbar_Services"]
${CMS_BAY Internal_Menu}                //*[@id="frmMain"]/div//a[contains(text(), 'Bay Internal')]
${RTP_Siderbar_Instruction_Menu}        //*[@id="side"]//a[contains(text(), 'Instruction')]
${RTP_Siderbar_Upload}                  //*[@id="side"]//a[contains(text(), 'Upload/Download File')]
${RTP_Siderbar_Sending}                 //*[@id="side"]//a[contains(text(), "RTP Sending")]
${RTP_Siderbar_SendingAuth}             //*[@id="side"]//a[contains(text(), "RTP Sending Auth")]
${RTP_Siderbar_SendingInquiry}             //*[@id="side"]//a[contains(text(), "RTP Sending Inquiry")]
${RTP_Upload_Button}                    //div/button[contains(text(), "Upload")]   #/html/body/div[2]/div[3]/div[4]/button   
${RTP_Logout_Button}                    //*[@id="userInfo"]/div[2]/button
${RTP_Dropdow_ProxyID}                  //div/select[@ng-model="senderId"]
${RTP_Browse_File_Button}               //*[@id="uploadbtn"]
${RTP_Input_FileUpload}                 //input[@type="file"]
${RTP_Submit_Upload_Button}             //div/span/button[contains(text(), "Upload")]
${RTP_PopUp_Message}                    //div[@alert-dialog="$root.showmessage"]//div[@ng-bind-html="message | trusted"]    #//div[@ng-bind-html="message | trusted"]    
${RTP_PopUp_Message_Close_Button}       //button[@aria-label="Close"]
${RTP_Dropdow_DayFilter}                //select[@ng-model="dayfilter"]
${RTP_Table_Upload}                     //table[@class="table table-striped"]/tbody/tr
${RTP_Table_Sending}                    //tbody/tr
${RTP_Submit_Button}                    //button[contains(text(), ' Submit')]
${RTP_Discard_Button}                   //button[contains(text(), ' Discard')]
${RTP_Back_Button}                      //div/a[@ng-click="back();"]
${RTP_Auth_Button}                      //button[@ng-click="actionbtn('APPROVE_NEW_REQUEST')"]
${RTP_Reject_Button}                    //button[@ng-click="actionbtn('REJECT_NEW_REQUEST')"]
${RTP_SessionTimeout}                   //div[@class="wrapper"]//div[@class="col-xs-12 text-center center"]/h1   #Session Timeout กรุณา Login ก่อนใช้ด้วย
#---------- By Chirayut ---------------
${RTP_Siderbar_Receiving}               //*[@id="side"]//a[contains(text(), 'RTP Receiving')]
${RTP_Result_Table}                     //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table
${RTP_Result_Table_Row}                 //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr
${RTP_Result_Table_Header}              //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/thead/tr/th
${RTP_Alert_Message}                    //div[@alert-dialog="$root.showmessage"]//div[@ng-bind-html="message | trusted"]
${RTP_Alert_Close_Button}               //div[@alert-dialog="$root.showmessage"]//button

${RTP_Product_Type_Bill_Alert}          //*[@id="authinbox-tab"]/a[@ng-click="pickaction('bill')"]
${RTP_Product_Type_Pay_Alert}           //*[@id="authinbox-tab"]/a[@ng-click="pickaction('credit')"]

${RTP_Modal_Input_Product}                          //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//label[text()="Product"]/..//input
${RTP_Modal_Input_RefId}                            //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//input[@ng-model="detailitem.rtpReferenceId"]
${RTP_Modal_Input_BillerSenderAccountNo}            //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//input[@ng-model="detailitem.senderAccountNo"]
${RTP_Modal_Input_SenderProxyId}                    //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//input[@ng-model="detailitem.senderProxyId"]
${RTP_Modal_Input_SenderProxyType}                  //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//input[@ng-model="detailitem.senderProxyType"]
${RTP_Modal_Input_ReceiverProxyId}                  //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//input[@ng-model="detailitem.receiverProxyId"]
${RTP_Modal_Input_ReceiverProxyType}                //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//input[@ng-model="detailitem.receiverProxyType"]
${RTP_Modal_Input_Amount}                           //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//label[text()="RTP Amount"]/..//input
${RTP_Modal_Input_Entry_Date}                       //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//label[text()="Entry Date"]/..//input
${RTP_Modal_Input_Due_Date}                         //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//label[contains(text(),"Due Date")]/..//input
${RTP_Modal_Input_Biller_Reference_1}               //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//label[text()="Biller Reference 1 "]/..//input
${RTP_Modal_Input_Biller_Reference_2}               //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//label[text()="Biller Reference 2 "]/..//input
${RTP_Modal_Input_Biller_Reference_3}               //div[@payment-detail-dialog="showpaymentdetail"]//div[@class="modal-content"]//div[@class="modal-body"]//label[text()="Biller Reference 3 "]/..//input

${RTP_iframe_Content}                               //*[@id="content"]//iframe
${RTP_Select_Dropdown_receiverId}                   //select[@ng-model="receiverId"]
${RTP_Dropdown_List_Total_Page}                     //select[@ng-model="pager.currentPage"]
${RTP_Enquiry_Result_Row}                           //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr
${RTP_Close_Popup_Button}                           //div[@payment-detail-dialog="showpaymentdetail"]//button[@aria-label="Close"]
${RTP_Pay_Button}                                   //button[@ng-click="pay()"]
${RTP_Confirm_Pay_Button}                           //button[@ng-click="paywithconfirm()"]

${CMS_Confirm_Auth_Btn}                             //div[@aria-labelledby="ui-dialog-title-totalCountDialog"]//button//span[text()="Ok"]
${CMS_Auth_Message}                                 //*[@id="messageArea"]/h3/b

${RTP_Detail_Payment_Product}                       //*[@id="lbl_bankProduct"]
${RTP_Detail_Instruction_Reference_No}              //*[@id="lbl_referenceNo"]
${RTP_Detail_Debit_Account}                         //*[@id="lbl_accountNo"]
${RTP_Detail_Payment_Amount}                        //*[@id="lbl_amount"]
${RTP_Detail_Debit_Date}                            //*[@id="lbl_activationDate"]
${RTP_Detail_Bill_Payment_Expand_Section}           //*[@id="divMyprodEnrich"]
${RTP_Detail_TAXID}                                 //*[@id="divMyprodEnrich"]//label[contains(text(), 'TAXID')]/..//label[@class="rounded w20 greyback inline"]
${RTP_Detail_RTP_Reference_ID}                      //*[@id="divMyprodEnrich"]//label[contains(text(), 'RTP Reference ID')]/..//label[contains(@class,"rounded")]
${RTP_Detail_Sender_Proxy_ID}                       //*[@id="divMyprodEnrich"]//label[contains(text(), 'Sender Proxy ID')]/..//span[contains(@class,"rounded")]
${RTP_Detail_Receiving_Proxy_ID}                    //*[@id="divMyprodEnrich"]//label[contains(text(), 'Receiving Proxy ID')]/..//span[contains(@class,"rounded")]
${RTP_Detail_Reference1}                            //*[@id="divMyprodEnrich"]//label[contains(text(), 'Reference1')]/..//span[contains(@class,"rounded")]
${RTP_Detail_Reference2}                            //*[@id="divMyprodEnrich"]//label[contains(text(), 'Reference2')]/..//span[contains(@class,"rounded")]
${RTP_Detail_Reference3}                            //*[@id="divMyprodEnrich"]//label[contains(text(), 'Reference3')]/..//span[contains(@class,"rounded")]
