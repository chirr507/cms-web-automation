*** Settings ***
Library  SeleniumLibrary
Library  String
Library  DateTime
Library  OperatingSystem
Library  Collections

Resource  CustomLib\\_init_.robot
# Resource  Variables\\_init_.robot
Resource  Locator\\_init_.robot
Resource  Keywords\\_init_.robot
Variables  Configurations\\Configurations.yaml