*** Settings ***
Resource    ${EXECDIR}\\Resources\\__init_resources__.robot
Suite Setup    C - Suite Setup
Test Teardown  F_CASH_CMS Teardown
# '*************************************************************************************************
#* Date                * Changed By     * Description                           * version
#---------------------------------------------------------------------------------------------------
#* 20/10/2021            Chirayut R.       Initial version                         1.0
#* 20/11/2021            Jenjira H.        MappingfileUpload version               2.0
#* 27/12/2021            Jenjira H.        Output transaction version              3.0
#****************************************************************************************************
***Test Cases***    
TC_CMS_001
    [Documentation]    Generate ไฟล์ Batch และอัพโหลดที่ Cashlink
    F_CASH_Create Batch Text File
    Set Suite Variable  ${transactionLog}  ${EMPTY}        # Define Empty Variable for Write Log
    # Set transaction iterator
    ${fileIterator}  Set Variable  ${1}  
    FOR  ${file}  IN  @{fileList}  # Loop File

        ${fileIteratorRow}  Evaluate  ${fileIterator} + 1  # Set File Iterator
        ${fileName}  Get File Name From Path  ${file}  # Get File Name from Path
        F_CASH_Generate Log Transaction  ********** File: ${fileName} **********

        # Check File Format for Filter data
        
        ${fileFormatName}  Get Substring  ${fileName}  0  4
        ${rawDataByFormat}  F_CASH_Filter File Format    ${fileFormatName}

        #---------------------------------------------
        # Check File Format for Filter data
        #---------------------------------------------
        
        ${wbFileMapping}    open_excel_workbook  TestData\\CMS_WEB_FileMapping.xlsx
        ${wsFileMapping}    select_excel_worksheet  ${wbFileMapping}  INPUT_MAPPING     # sheetname in data file
        ${lastRows}     CustomExcelLibrary.get_last_row  ${wsFileMapping}
        #Log To Console  จำนวนแถวทั้งหมดในไฟล์ : ${lastRows}  

        # Loop Find File Format Name 
        FOR  ${i}   IN RANGE  2   ${lastRows} 
            ${clientData}  CustomExcelLibrary.get_cell_value    ${wsFileMapping}  ${i}  ${1}
            ${formatTypeData}  CustomExcelLibrary.get_cell_value     ${wsFileMapping}  ${i}  ${2}
            ${formatNameData}  CustomExcelLibrary.get_cell_value   ${wsFileMapping}  ${i}  ${3}
            Set Suite Variable  ${clientData}   ${clientData} 
            Set Suite Variable  ${formatTypeData}   ${formatTypeData}
            Set Suite Variable  ${formatNameData}   ${formatNameData}

            ${formatTypeCode}  Get Substring  ${formatTypeData}  0  4
  
            IF  '${formatTypeCode}' == '${fileFormatName}' and '${makerData}[CLIENT]' == '${clientData}'
                Log To Console  ข้อมูลตรงกันใช้ไฟล์แมพด้วยชื่อ : ${formatNameData}
                Set Suite Variable  ${MapCode}  ${formatNameData}
                Exit For Loop  
            
            END 

        END 
        Log To Console  เริ่มเปิดเว็บไซต์ Cashlink
        F_CASH_Open_Cashlink_Website
        F_CASH_Login Cashlink Maker

        #F_CASH_Click Navbar Payment Menu    # Click Payment Navbar
        #Sleep  5
        #F_CASH_Click Sidebar Payment Menu    # Click Payment Sidebar (Expand)
        Go To  https://uat2.krungsribizonline.com/GCPCW/taskStatusList.form  ## กรณีรันแล้วไม่เห็นเมนูข้างๆ

        ${fileUploadStats}  Set Variable  ${False}   #  Set default upload status to false
        
        IF  '${cmsData}[UploadFlag][value]' == 'Y'
            # ------------------------------------------------------------------------------
            # Upload File Section (Maker)
            # ------------------------------------------------------------------------------
            F_CASH_Generate Log Transaction  Maker Upload ===================
            F_CASH_Upload Text File  ${file}   ${MapCode}    # Step for Upload Text File to Cashlink
            ${uploadTableCount}  Get Element Count  ${CMS_Upload_Table}  # Count file upload table
            ${reconcileParameterStatus}  Set Variable  ${True}
            ${foundFileStatus}  Set Variable  ${False}
            FOR  ${i}  IN RANGE  1  ${uploadTableCount} + 1  # Loop for retry to refresh upload screen
                ${fileNameScreen}  C - Get Text  (//*[@id="gridTable"]//tbody/tr/td[2])[${i}]
                ${fullFileName}  Set Variable  ${makerData}[CLIENT]-${fileName}
                IF  '${fullFileName}' == '${fileNameScreen}'
                    ${foundFileStatus}  Set Variable  ${True}
                    FOR  ${j}  IN RANGE  1  ${reloadTime} + 1  # reloadTime from yaml file
                        # Get Data
                        ${statusScreen}  C - Get Text  (//*[@id="gridTable"]//tbody/tr/td[11])[${i}]
                        ${uploadCountScreen}  C - Get Text  (//*[@id="gridTable"]//tbody/tr/td[6])[${i}]
                        ${totalAmountScreen}  C - Get Text  (//*[@id="gridTable"]//tbody/tr/td[8])[${i}]
                        Write Cell  ${fileWorksheet}  ${fileIteratorRow}  4  ${statusScreen}
                        Write Cell  ${fileWorksheet}  ${fileIteratorRow}  1  ${file}
                        Write Cell  ${fileWorksheet}  ${fileIteratorRow}  2  ${uploadCountScreen}
                        Write Cell  ${fileWorksheet}  ${fileIteratorRow}  3  ${totalAmountScreen}
                        
                        IF  '${statusScreen}' == 'Completed'
                            ${amountConverted}  Convert String To Number Format  ${totalAmountScreen}
                            ${rawDataLen}  Get Length  ${rawDataByFormat}
                            ${rawTotalAmount}  Sum Raw Data Amount  ${rawDataByFormat}
                            ${uniqueBatch}  Get Unique From Json Array  ${rawDataByFormat}  Batch_Reference_No
                            #  Reconcile Uploaded Data
                            IF  '${rawDataLen}' == '${uploadCountScreen}'
                                F_CASH_Generate Log Transaction  Transaction Count Pass - Expect: ${rawDataLen} == Actual: ${uploadCountScreen}
                            ELSE
                                F_CASH_Generate Log Transaction  Transaction Count Fail - Expect: ${rawDataLen} != Actual: ${uploadCountScreen}
                                ${reconcileParameterStatus}  Set Variable  ${False}
                            END
                            IF  '${rawTotalAmount}' == '${amountConverted}'
                                F_CASH_Generate Log Transaction  Total Amount Pass - Expect: ${rawTotalAmount} == Actual: ${amountConverted}
                            ELSE
                                F_CASH_Generate Log Transaction  Total Amount Fail - Expect: ${rawTotalAmount} != Actual: ${amountConverted}
                                ${reconcileParameterStatus}  Set Variable  ${False}
                            END
                            # Set File upload status
                            IF  ${reconcileParameterStatus} == ${True}
                                ${fileUploadStats}  Set Variable  ${True}
                            ELSE
                                ${fileUploadStats}  Set Variable  ${False}
                            END

                            # Exit For loop when found batch file and status change to complete
                            Exit For Loop
                        ELSE
                            IF  '${statusScreen}' != 'New'
                                F_CASH_Generate Log Transaction  Upload File: ${fileName} - Incomplete, Status is ${statusScreen}
                                ${fileUploadStats}  Set Variable  ${False}
                                C - Click Element  ${CMS_Navbar_Logout_Btn}
                                #F_CASH_Generate Log Transaction    Upload File Successs!! but has status ${statusScreen}
                                Exit For Loop
                                
                            ELSE
                                Sleep  5
                                Reload Page
                                Continue For Loop
                            END
                        END
                        IF  ${j} == ${reloadTime}
                            F_CASH_Generate Log Transaction  File: ${fileName} - Status not change to Complete, Actual Status is ${statusScreen}
                            ${fileUploadStats}  Set Variable  ${False}
                        END
                    END  #end if reload

                    # If found file upload in table, Exit Loop in table
                    IF  ${foundFileStatus} == ${True}
                        Exit For Loop
                    END
                END  #if filename same
                IF  '${i}' == '${uploadTableCount}'
                    Log To Console  '${i}' == '${uploadTableCount}'
                    F_CASH_Generate Log Transaction  Not found uploaded file: ${fileName}
                    Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[UploadResult]  Failed!
                END
                IF  '${fileUploadStats}' == '${True}'
                    Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[UploadResult]  Passed!
                    Exit For Loop
                END
            END  #for loop retry to refresh
        END  # if upload flag Y/N
        C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Upload_File_${fileName}  # Capture Screen after upload file

        # ------------------------------------------------------------------------------
        # Verify Batch Section (Maker)
        # ------------------------------------------------------------------------------
        IF  '${cmsData}[SubmitFlag][value]' == 'Y' and '${fileUploadStats}' == '${True}'
            ${uniqueBatch}  Get Unique From Json Array  ${rawDataByFormat}  Batch_Reference_No

            #C - Click Element  ${CMS_Sidebar_Payment_Summary}  # Click Summary Menu in Sidebar
            Go To  https://uat2.krungsribizonline.com/GCPCW/showPaymentsListBatch.form
            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Batch_Data  # Capture screen
            ${countBatchTable}  Get Element Count  ${CMS_Batch_Table}  # Count table of batch screen
            ${batchIterator}  Set Variable  ${1}
            ${insIteratorRow}  Set Variable  ${1}
            FOR  ${batchName}  IN  @{uniqueBatch}
                ${reconcileBatchStatus}  Set Variable  ${True}  # Set default status as True (Success)
                ${batchIteratorRow}  Evaluate  ${batchIterator} + 1  # Set row for write data to excel
                ${batchArray}    Filter Json Array    ${rawDataByFormat}    Batch_Reference_No    ${batchName}  # Filter Json data by Batch Array
                ${batchSummaryTrans}    Get Length    ${batchArray}  # Get Transaction count of batch
                ${batchSummaryAmt}    Sum Raw Data Amount    ${batchArray}  # Summary amount of batch
                
                FOR  ${i}  IN RANGE  1  ${countBatchTable}
                    ${batch}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${i}]
                    IF  '${batchName}' == '${batch}'
                        ${totalInst}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[8])[${i}]
                        ${totalAmt}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${i}]
                        ${batchStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[11])[${i}]
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  1  ${file}
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  2  ${batch}
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  3  ${totalInst}
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  4  ${totalAmt}
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  5  ${batchStatusScreen}
                        IF  '${batchStatusScreen}' == 'For Submit'
                            # -----------------------------
                            IF  '${batchSummaryTrans}' == '${totalInst}'
                                F_CASH_Generate Log Transaction    Batch Transaction Count Pass - Expect: ${batchSummaryTrans} == Actual: ${totalInst}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Transaction Count Fail - Expect: ${batchSummaryTrans} != Actual: ${totalInst}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                            # -----------------------------
                            IF  '${batchSummaryAmt}' == '${totalAmt}'
                                F_CASH_Generate Log Transaction    Batch Amount Pass - Expect: ${batchSummaryAmt} == Actual: ${totalAmt}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Amount Fail - Expect: ${batchSummaryAmt} != Actual: ${totalAmt}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                        ELSE
                            F_CASH_Generate Log Transaction    Status != For Submit - Actual Status: ${batchStatusScreen}
                            ${reconcileBatchStatus}  Set Variable  ${False}
                            Exit For Loop
                        END

                        C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[3]/a[@title="View Deposit"])[${i}]
  
                        ${insIterator}  Set Variable  ${1}
                        FOR  ${instruction}  IN  @{batchArray}
                            ${insReconcileStatus}  Set Variable  ${True}
                            ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1
                            ${instructionRefScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${insIterator}]
                            ${instructionBankBranchScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[9])[${insIterator}]
                            ${instructionBeneficiaryAccountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${insIterator}]
                            ${instructionEffectiveDateScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[11])[${insIterator}]
                            ${instructionAmountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${insIterator}]
                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[${insIterator}]

                            Write Cell  ${insWorksheet}  ${insIteratorRow}  1  ${batch}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  2  ${instructionRefScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  3  ${instructionBeneficiaryAccountScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  5  ${instructionAmountScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  6  ${instructionStatusScreen}

                            F_CASH_Generate Log Transaction    Instruction Ref ${InstructionRefScreen} **********************
                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                            #--------------------------------------------------------------------------
                            #   Submit Transaction | Maker
                            #--------------------------------------------------------------------------
                            # เมื่อ Status เท่ากับ For Submit จะตรวจสอบข้อมูลใน Instruction
                            IF  '${instructionStatusScreen}' == 'For Submit'
                                IF  '${instruction}[Instruction_Reference_No]' == '${InstructionRefScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Ref Pass - Expect: ${instruction}[Instruction_Reference_No] == Actual: ${InstructionRefScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Ref Fail - Expect: ${instruction}[Instruction_Reference_No] != Actual: ${InstructionRefScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END
                                IF  '${instruction}[Beneficiary/Drawer_Account]' == '${instructionBeneficiaryAccountScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Beneficiary Pass - Expect: ${instruction}[Beneficiary/Drawer_Account] == Actual: ${instructionBeneficiaryAccountScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Beneficiary Fail - Expect: ${instruction}[Beneficiary/Drawer_Account] != Actual: ${instructionBeneficiaryAccountScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END
                                ${instructionAmountConverted}  Convert To Custom Number Format  ${instruction}[Amount]
                                IF  '${instructionAmountConverted}' == '${instructionAmountScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Amount Pass - Expect: ${instructionAmountConverted} == Actual: ${instructionAmountScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Amount Fail - Expect: ${instructionAmountConverted} != Actual: ${instructionAmountScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END

                                # Click to View Instruction Detail
                                C - Click Element By Javascript  (//*[@id='gridTable']/tbody/tr/td[3]/a)[${insIterator}]
                                Wait Until Element Is Visible  ${CMS_Ins_Title}
                                ${debitAccount}  C - Get Text  ${CMS_Ins_DebitAcc_Field}
                                F_CASH_Generate Log Transaction    Debit Account: ${debitAccount}
                                Write Cell  ${insWorksheet}  ${insIteratorRow}  4  ${debitAccount}
                                C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_Detail_${instructionRefScreen}  # Capture screen
                                C - Click Element  ${CMS_Ins_Back_Button}
                            ELSE
                                ${insReconcileStatus}  Set Variable  ${False}
                                F_CASH_Generate Log Transaction    ${instruction}: Status != For Submit - Actual Status: ${batchStatusScreen}
                            END
                            IF  '${insReconcileStatus}' == '${True}'
                                Write Cell  ${insWorksheet}  ${insIteratorRow}  7  Pass!!
                            ELSE
                                Write Cell  ${insWorksheet}  ${insIteratorRow}  7  Fail!!
                            END

                            ${insIterator}  Evaluate  ${insIterator} + 1
                            
                        END

                        # ${authIterator}  Set Variable  ${1}
                        # ${insIterator}  Set Variable  ${1}
                        # FOR  ${instruction}  IN  @{batchArray}
                        #     # Code For Auth Partial
                        #     IF  '${instruction}[Auth_Flag]' == 'Y'
                        #         C - Click Element By Javascript  (//*[@id='gridTable']/tbody/tr/td[2]/a)[${insIterator}]
                        #         ${authIterator}  Evaluate  ${authIterator} + 1
                        #     END
                        #     ${insIterator}  Evaluate  ${insIterator} + 1
                        # END
                        # # Click Submit All
                        # IF  ${authIterator} > ${1}
                        #     C - Click Element  ${CMS_Auth_Btn}
                        #     C - Click Element  ${CMS_Auth_Confirm_Btn}
                        #     Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                        # END
                        # C - Click Element  ${CMS_Batch_Back_Button}

                        C - Click Element  ${CMS_Batch_Submit_All_Button}  # Click Submit All Button
                        C - Click Element  ${CMS_Batch_Submit_All_OK_Button}  # Click Ok Button for Confirm to Submit All
                        Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                        C - Click Element  ${CMS_Batch_Back_Button}
                        Exit For Loop
                    END
                END
                
                ${batchIterator}  Evaluate  ${batchIterator} + 1

                IF  '${reconcileBatchStatus}' == '${False}'
                    Write Cell  ${batchWorksheet}  ${batchIteratorRow}  6  Failed!!
                    Continue For Loop  
                ELSE
                    Write Cell  ${batchWorksheet}  ${batchIteratorRow}  6  Passed!!
                END
            END
            C - Click Element  ${CMS_Navbar_Logout_Btn}
        END

        # ------------------------------------------------------------------------------
        # Verify Batch Section (Checker)
        # ------------------------------------------------------------------------------
        IF  '${cmsData}[AuthFlag][value]' == 'Y' and '${fileUploadStats}' == '${True}'
            F_CASH_Login Cashlink Checker
            # C - Click Element  ${CMS_Navbar_Payment_Menu}  # Click Payment Navbar
            # C - Click Element  ${CMS_Sidebar_Payment}  # Click Payment Sidebar (Expand)
            # C - Click Element  ${CMS_Sidebar_Payment_Summary}  # Click Summary Menu in Sidebar
            Go To  https://uat2.krungsribizonline.com/GCPCW/showPaymentsListBatch.form
            F_CASH_Generate Log Transaction    Checker ===================
            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Checker_Batch  # Capture screen
            ${batchIterator}  Set Variable  ${1}
            ${insIteratorRow}  Set Variable  ${1}
            FOR  ${batchName}  IN  @{uniqueBatch}
                ${reconcileBatchStatus}  Set Variable  ${True}  # Set default status as True (Success)
                ${batchIteratorRow}  Evaluate  ${batchIterator} + 1  # Set row for write data to excel
                ${batchArray}    Filter Json Array    ${rawDataByFormat}    Batch_Reference_No    ${batchName}  # Filter Json data by Batch Array
                ${batchSummaryTrans}    Get Length    ${batchArray}  # Get Transaction count of batch
                ${batchSummaryAmt}    Sum Raw Data Amount    ${batchArray}  # Summary amount of batch
                
                FOR  ${i}  IN RANGE  1  ${countBatchTable}
                    ${batch}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${i}]
                    IF  '${batchName}' == '${batch}'
                        ${totalInst}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[8])[${i}]
                        ${totalAmt}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${i}]
                        ${batchStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[11])[${i}]
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  5  ${batchStatusScreen}
                        IF  '${batchStatusScreen}' == 'Submitted'
                            # -----------------------------
                            IF  '${batchSummaryTrans}' == '${totalInst}'
                                F_CASH_Generate Log Transaction    Batch Transaction Count Pass - Expect: ${batchSummaryTrans} == Actual: ${totalInst}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Transaction Count Fail - Expect: ${batchSummaryTrans} != Actual: ${totalInst}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                            # -----------------------------
                            IF  '${batchSummaryAmt}' == '${totalAmt}'
                                F_CASH_Generate Log Transaction    Batch Amount Pass - Expect: ${batchSummaryAmt} == Actual: ${totalAmt}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Amount Fail - Expect: ${batchSummaryAmt} != Actual: ${totalAmt}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                        ELSE
                            F_CASH_Generate Log Transaction    Status != For Submit - Actual Status: ${batchStatusScreen}
                            ${reconcileBatchStatus}  Set Variable  ${False}
                            Exit For Loop
                        END
                        #--------------------------------------------------------------------------
                        #   Authorize Transaction | Checker
                        #--------------------------------------------------------------------------  
                        C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[3]/a[@title="View Deposit"])[${i}]
                        
                        ${insIterator}  Set Variable  ${1}
                        
                        FOR  ${instruction}  IN  @{batchArray}
                            ${insReconcileStatus}  Set Variable  ${True}
                            ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1
                            ${instructionRefScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${insIterator}]
                            ${instructionBankBranchScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[9])[${insIterator}]
                            ${instructionBeneficiaryAccountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${insIterator}]
                            ${instructionEffectiveDateScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[11])[${insIterator}]
                            ${instructionAmountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${insIterator}]
                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[${insIterator}]

                            Write Cell  ${insWorksheet}  ${insIteratorRow}  1  ${batch}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  2  ${instructionRefScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  3  ${instructionBeneficiaryAccountScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  5  ${instructionAmountScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  6  ${instructionStatusScreen}

                            F_CASH_Generate Log Transaction    Instruction Ref ${InstructionRefScreen} **********************
                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                            
                            # เมื่อ Status เท่ากับ For My Auth จะตรวจสอบข้อมูลใน Instruction
                            IF  '${instructionStatusScreen}' == 'For My Auth'
                                IF  '${instruction}[Instruction_Reference_No]' == '${InstructionRefScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Ref Pass - Expect: ${instruction}[Instruction_Reference_No] == Actual: ${InstructionRefScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Ref Fail - Expect: ${instruction}[Instruction_Reference_No] != Actual: ${InstructionRefScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END
                                IF  '${instruction}[Beneficiary/Drawer_Account]' == '${instructionBeneficiaryAccountScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Beneficiary Pass - Expect: ${instruction}[Beneficiary/Drawer_Account] == Actual: ${instructionBeneficiaryAccountScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Beneficiary Fail - Expect: ${instruction}[Beneficiary/Drawer_Account] != Actual: ${instructionBeneficiaryAccountScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END
                                ${instructionAmountConverted}  Convert To Custom Number Format  ${instruction}[Amount]
                                IF  '${instructionAmountConverted}' == '${instructionAmountScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Amount Pass - Expect: ${instructionAmountConverted} == Actual: ${instructionAmountScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Amount Fail - Expect: ${instructionAmountConverted} != Actual: ${instructionAmountScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END

                                # Click to View Instruction Detail  -- So this step doing well in submit step --
                                # C - Click Element By Javascript  (//*[@id='gridTable']/tbody/tr/td[3]/a)[${insIterator}]
                                # Wait Until Element Is Visible  ${CMS_Ins_Title}
                                # C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_Detail_${instructionRefScreen}  # Capture screen
                                # C - Click Element  ${CMS_Ins_Back_Button}
                            ELSE
                                ${insReconcileStatus}  Set Variable  ${False}
                                F_CASH_Generate Log Transaction    ${instruction}[Instruction_Reference_No]: Status != For My Auth - Actual Status: ${batchStatusScreen}
                            END
                            IF  '${insReconcileStatus}' == '${True}'
                                Write Cell  ${insWorksheet}  ${insIteratorRow}  7  Pass!!
                            ELSE
                                Write Cell  ${insWorksheet}  ${insIteratorRow}  7  Fail!!
                                Log To Console    Go to ${instructionStatusScreen}
                            END
                            ${insIterator}  Evaluate  ${insIterator} + 1
                        END
                        # ---------------------------------------------------------------------------
                        #    Click authorize by flag in TestData file
                        # ---------------------------------------------------------------------------
                        ${authIterator}  Set Variable  ${1}
                        ${insIterator}  Set Variable  ${1}
                         FOR  ${instruction}  IN  @{batchArray}
                             # Code For Auth Partial
                             IF  '${instruction}[Auth_Flag]' == 'Y'
                                 C - Click Element By Javascript  (//*[@id='gridTable']/tbody/tr/td[2]/a)[${insIterator}]
                                 ${authIterator}  Evaluate  ${authIterator} + 1
                             END
                             ${insIterator}  Evaluate  ${insIterator} + 1
                         END
                        # # Click Auth All
                         IF  ${authIterator} > ${1}
                             C - Click Element  ${CMS_Auth_Btn}
                             C - Click Element  ${CMS_Auth_Confirm_Btn}
                             Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                             ${instructionMessageScreen}   C - Get Text  //*[@id="messageArea"]/h3/b
                             Log To Console  ${instructionMessageScreen}
                             #Write Cell  ${insWorksheet}  ${insIteratorRow}  8  ${instructionMessageScreen}
                         END
                         
                        C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                        C - Click Element  ${CMS_Batch_Back_Button}
                        Exit For Loop
                        
                    END
                END
                ${batchIterator}  Evaluate  ${batchIterator} + 1
                IF  '${reconcileBatchStatus}' == '${False}'
                    Write Cell  ${batchWorksheet}  ${batchIteratorRow}  6  Failed!!
                    Continue For Loop  
                ELSE
                    Write Cell  ${batchWorksheet}  ${batchIteratorRow}  6  Passed!!
                END
            END
            
            C - Click Element  ${CMS_Navbar_Logout_Btn}
            F_CASH_Generate Log Transaction   ${instructionMessageScreen}
            F_CASH_Generate Log Transaction    Authorize All Successs!! 
        END
            #--------------------------------------------------------------------------
            #   Send To Bank Transaction | Checker
            #--------------------------------------------------------------------------  
        IF  '${cmsData}[SendAllFlag][value]' == 'Y' and '${fileUploadStats}' == '${True}'
            F_CASH_Login Cashlink Checker
            # C - Click Element  ${CMS_Navbar_Payment_Menu}  # Click Payment Navbar
            # C - Click Element  ${CMS_Sidebar_Payment}  # Click Payment Sidebar (Expand)
            # C - Click Element  ${CMS_Sidebar_Payment_Summary}  # Click Summary Menu in Sidebar

           Go To  https://uat2.krungsribizonline.com/GCPCW/showPaymentsListBatch.form
            
            ${batchIterator}  Set Variable  ${1}
            ${insIteratorRow}  set Variable  ${1}
            FOR  ${batchName}  IN  @{uniqueBatch}
                ${reconcileBatchStatus}  Set Variable  ${True}  # Set default status as True (Success)
                ${batchArray}    Filter Json Array    ${rawDataByFormat}    Batch_Reference_No    ${batchName}  # Filter Json data by Batch Array
                FOR  ${i}  IN RANGE  1  ${countBatchTable}
                    ${batch}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${i}]
                    IF  '${batchName}' == '${batch}'
                        C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[3]/a[@title="View Deposit"])[${i}]
                        ${sendIterator}  Set Variable  ${1}
                        ${insIterator}  Set Variable  ${1}
                        FOR  ${instruction}  IN  @{batchArray}
                            IF  '${instruction}[SendAll_Flag]' == 'Y'
                                C - Click Element By Javascript  (//*[@id='gridTable']/tbody/tr/td[2]/a)[${insIterator}]
                                ${sendIterator}  Evaluate  ${sendIterator} + 1
                            END 

                            ${insIterator}  Evaluate  ${insIterator} + 1
                        END
                        IF  ${sendIterator} > ${1}
                            C - Click Element  ${CMS_SendAll_Btn}  ## Send All Button
                            C - Click Element  ${CMS_SendAll_Confirm}
                            ${instructionMessageScreen}   C - Get Text  //*[@id="messageArea"]/h3/b
                            Log To Console  ${instructionMessageScreen}                            
                            Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                        END

                        ## ใส่ผล output last status ##
                        ${insIterator}  Set Variable  ${1}
                        
                        FOR  ${instruction}  IN  @{batchArray}
                            ${insReconcileStatus}  Set Variable  ${True}
                            ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1
                            ${instructionRefScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${insIterator}]
                            ${instructionBankBranchScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[9])[${insIterator}]
                            ${instructionBeneficiaryAccountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${insIterator}]
                            ${instructionEffectiveDateScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[11])[${insIterator}]
                            ${instructionAmountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${insIterator}]
                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[${insIterator}]

                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  1  ${batch}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  2  ${instructionRefScreen}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  3  ${instructionBeneficiaryAccountScreen}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  4  ${debitAccount}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  5  ${instructionAmountScreen}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  6  ${instructionStatusScreen}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  8   ${instructionMessageScreen}
                            ${instMessage}  Get Substring  ${instructionMessageScreen}  0  16
                            #Log To Console  ${instMessage}
                            IF  '${instMessage}' == 'You have already'
                                Write Cell  ${txnWorksheet}  ${insIteratorRow}  7   Passed!!
                            ELSE 
                                Write Cell  ${txnWorksheet}  ${insIteratorRow}  7   Failed!!
                            END                             
                           
                             
                            ${insIterator}  Evaluate  ${insIterator} + 1
                        END 
                        C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                        C - Click Element  ${CMS_Batch_Back_Button}
                        Exit For Loop
                    END
              
                END
               
            END

            # Click Send to Bank All 
            C - Click Element  ${CMS_Navbar_Logout_Btn}
            F_CASH_Generate Log Transaction    ${instructionMessageScreen}
            F_CASH_Generate Log Transaction    Send to Bank All Successs!!
        END

        # Write Result
        IF  '${fileUploadStats}' == '${True}'
            Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[UploadResult]  Passed!!
        ELSE
            Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[UploadResult]  Failed!!
        END
        # Set File Iterator
        ${fileIterator}  Evaluate  ${fileIterator} + 1
    END

