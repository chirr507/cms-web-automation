*** Settings ***
Suite Setup       Open Website
Suite Teardown    Close Browser
Test Template     Submit Ecustom
Library           SeleniumLibrary
Library           BuiltIn
Library           String
Library           DataDriver    file=D:\\TestRobot\\testdata\\ECustomData.xlsx    sheet_name=ECustom

*** Variables ***
${fileNameTXT1}          xpath=//*[@id="fileName"]
${referenceNoTXT}       xpath=//*[@id="referenceNo"]
${seqNoTXT}             xpath=//*[@id="seqNo"]
${DeclarationRefTXT}    xpath=//*[@id="DeclarationRef"]
${DeclarationNoTXT}     xpath=//*[@id="DeclarationNo"]
${branchTXT}            xpath=//*[@id="branch"]
${NameTXT}              xpath=//*[@id="Name"]
${payerAccountNoTXT}    xpath=//*[@id="payerAccountNo"]
${payerBankCodeTXT}     xpath=//*[@id="payerBankCode"]
${payerBankBranchTXT}   xpath=//*[@id="payerBankBranch"]
${PayerTaxNoTXT}        xpath=//*[@id="PayerTaxNo"]
${PayAmountTXT}         xpath=//*[@id="PayAmount"]
${TransmitDateTimeTXT}      xpath=//*[@id="TransmitDateTime"]
${receiverAccountNoTXT}     xpath=//*[@id="receiverAccountNo"]
${receiverBankCodeTXT}      xpath=//*[@id="receiverBankCode"]
${receiverBankBranchTXT}    xpath=//*[@id="receiverBankBranch"]
${CompanyNameTXT}       xpath=//*[@id="CompanyName"]
${IssueNoTXT}           xpath=//*[@id="IssueNo"]
${IssueDateTXT}         xpath=//*[@id="IssueDate"]
${FilenameTXT}          xpath=//tr[20]/td[2]/input
${TransactionTypeTXT}       xpath=//*[@id="transactionType"]
${PayerTaxIDTXT}        xpath=//*[@id="Dbtr_Othr_Id1"]
${PayerBranchTXT}       xpath=//*[@id="Dbtr_Othr_Id2"]
${submitBTN}            xpath=//*[@id="submit"]
${resetBTN}             xpath=//*[@id="reset"]
${BROWSER}              chrome
${BASE_URL}             file:///D:/ECustomsTxnSimulator.html


*** Test Cases ***
TC001 Input Test Data    Using    ${fileName1}    ${referenceNo}    ${seqNo}    ${DeclarationRef}    ${DeclarationNo}    ${branch}    ${Name}    ${payerAccountNo}      
    ...     ${payerBankCode}    ${payerBankBranch}    ${PayerTaxNo}    ${PayAmount}    ${TransmitDateTime}    ${receiverAccountNo}    ${receiverBankCode}     
    ...     ${receiverBankBranch}    ${CompanyName}    ${IssueNo}    ${IssueDate}    ${Filename}    ${TransactionType}    ${PayerTaxID}    ${PayerBranch}

*** Keywords ***
Open Website
    #[Arguments]    ${BASE_URL}    ${BROWSER}
    Open Browser    file:///D:/ECustomsTxnSimulator.html    chrome
    Maximize Browser Window

Input Text field
    [Arguments]    ${fieldNameTXT}    ${filedValue}
    Clear Element Text    ${fieldNameTXT}
    Input Text    ${fieldNameTXT}    ${filedValue}

Click Submit button
    Click Button    ${submitBTN}

Submit Ecustom
    [Arguments]    ${fileName1}    ${referenceNo}    ${seqNo}    ${DeclarationRef}    ${DeclarationNo}    ${branch}    ${Name}    ${payerAccountNo}    ${payerBankCode}     
    ...     ${payerBankBranch}    ${PayerTaxNo}    ${PayAmount}    ${TransmitDateTime}    ${receiverAccountNo}    ${receiverBankCode}    ${receiverBankBranch}    
    ...     ${CompanyName}     ${IssueNo}    ${IssueDate}    ${Filename}    ${TransactionType}    ${PayerTaxID}    ${PayerBranch}
    Open Website
    Input Text field    ${fileNameTXT1}    ${fileName1}
    Input Text field    ${referenceNoTXT}    ${referenceNo}
    #Input Text field    ${seqNoTXT}    ${seqNo}
    Input Text field    ${DeclarationRefTXT}    ${DeclarationRef}
    Input Text field    ${DeclarationNoTXT}    ${DeclarationNo}
    Input Text field    ${branchTXT}    ${branch}
    Input Text field    ${NameTXT}    ${Name}
    Input Text field    ${payerAccountNoTXT}    ${payerAccountNo}
    #Input Text field    ${payerBankCodeTXT}    ${payerBankCode}
    Input Text field    ${payerBankBranchTXT}    ${payerBankBranch}
    Input Text field    ${PayerTaxNoTXT}    ${PayerTaxNo}
    Input Text field    ${PayAmountTXT}    ${PayAmount}
    Input Text field    ${TransmitDateTimeTXT}    ${TransmitDateTime}
    Input Text field    ${receiverAccountNoTXT}    ${receiverAccountNo}
    #Input Text field    ${receiverBankCodeTXT}    ${receiverBankCode}
    Input Text field    ${receiverBankBranchTXT}    ${receiverBankBranch}
    #Input Text field    ${CompanyNameTXT}    ${CompanyName}
    #Input Text field    ${IssueNoTXT}    ${IssueNo}
    #Input Text field    ${IssueDateTXT}    ${IssueDate}
    Input Text field    ${FilenameTXT}    ${Filename}
    Input Text field    ${TransactionTypeTXT}    ${TransactionType}
    Input Text field    ${PayerTaxIDTXT}    ${PayerTaxID}
    Input Text field    ${PayerBranchTXT}    ${PayerBranch}
    Click Submit button

Close browser
    close all browsers