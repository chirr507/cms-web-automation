*** Settings ***
Resource    ${EXECDIR}\\Resources\\__init_resources__.robot
Suite Setup    C - Suite Setup
Test Teardown  F_CASH_CMS Teardown
# '*************************************************************************************************
#* Date                * Changed By     * Description                           * version
#---------------------------------------------------------------------------------------------------
#* 20/10/2021            Chirayut R.       Initial version                         1.0
#* 28/12/2021            Jenjira H.        MappingfileUpload(DDA) version          2.0
#* 26/01/2022            Jenjira H.        Process Flow by AuthLevel version       3.0
#****************************************************************************************************
***Test Cases***    
TC_CMS_001
    [Documentation]    Generate ไฟล์ Batch และอัพโหลดที่ Cashlink
    # robot Tests/S_CMS_Collection_Txn_20211228r1.robot
    F_CASH_Create Batch Text File
    Set Suite Variable  ${transactionLog}  ${EMPTY}        # Define Empty Variable for Write Log
    # Set transaction iterator
    ${fileIterator}  Set Variable  ${1}  
    FOR  ${file}  IN  @{fileList}  # Loop File

        ${fileIteratorRow}  Evaluate  ${fileIterator} + 1  # Set File Iterator
        ${fileName}  Get File Name From Path  ${file}  # Get File Name from Path
        F_CASH_Generate Log Transaction  ********** File: ${fileName} **********

        # Check File Format for Filter data
        
        ${fileFormatName}  Get Substring  ${fileName}  0  4
        ${rawDataByFormat}  F_CASH_Filter File Format    ${fileFormatName}

        #---------------------------------------------
        # Check File Format for Filter data
        #---------------------------------------------
        
        ${wbFileMapping}    open_excel_workbook  TestData\\CMS_WEB_FileMapping.xlsx
        ${wsFileMapping}    select_excel_worksheet  ${wbFileMapping}  DDA_MAPPING     # sheetname in data file
        ${lastRows}     CustomExcelLibrary.get_last_row  ${wsFileMapping}
        #Log To Console  จำนวนแถวทั้งหมดในไฟล์ : ${lastRows}  

        # Loop Find File Format Name 
        FOR  ${i}   IN RANGE  2   ${lastRows} 
            ${clientData}  CustomExcelLibrary.get_cell_value    ${wsFileMapping}  ${i}  ${1}
            ${formatTypeData}  CustomExcelLibrary.get_cell_value     ${wsFileMapping}  ${i}  ${2}
            ${formatNameData}  CustomExcelLibrary.get_cell_value   ${wsFileMapping}  ${i}  ${3}
            Set Suite Variable  ${clientData}   ${clientData} 
            Set Suite Variable  ${formatTypeData}   ${formatTypeData}
            Set Suite Variable  ${formatNameData}   ${formatNameData}

            #Log To Console   Mapping : ${formatNameData}
            ${formatTypeCode}  Get Substring  ${formatTypeData}  0  4
  
            IF  '${formatTypeCode}' == '${fileFormatName}' and '${makerData}[CLIENT]' == '${clientData}'
                Log To Console  ข้อมูลตรงกันใช้ไฟล์แมพด้วยชื่อ : ${formatNameData}
                Set Suite Variable  ${MapCode}  ${formatNameData}
                Exit For Loop  
            
            END 

        END 
        
        #---------------------------------------------
        # Check Set up product usage Auth Level
        #---------------------------------------------
        Set Suite Variable  ${CMS_Product}  DDA
        ${wbSetup}    open_excel_workbook  TestData\\CMS_WEB_Product_Config.xlsx
        ${wsSetup}    select_excel_worksheet  ${wbSetup}  Setup     # sheetname in data file
        ${lastRows}     CustomExcelLibrary.get_last_row  ${wsSetup}
        Log To Console    จำนวนแถวทั้งหมดในไฟล์ : ${lastRows}  
        Log To Console    ${checkerData}[CLIENT]
        # Loop Find Auth Level 
        FOR  ${j}   IN RANGE  2   ${lastRows} 
            ${clientData}  CustomExcelLibrary.get_cell_value    ${wsSetup}  ${j}  ${1}
            ${productData}  CustomExcelLibrary.get_cell_value     ${wsSetup}  ${j}  ${2}
            ${authLevelData}  CustomExcelLibrary.get_cell_value   ${wsSetup}  ${j}  ${4}
            Set Suite Variable  ${clientData}   ${clientData} 
            Set Suite Variable  ${productData}   ${productData}
            Set Suite Variable  ${authLevelData}   ${authLevelData}
            Log To Console  ${clientData} ... ${productData} ... ${authLevelData}

            IF  '${checkerData}[CLIENT]' == '${clientData}' and '${CMS_Product}' == '${productData}'
                
                Set Suite Variable   ${setAuthLavel}   ${authLevelData}
                Log To Console  แมพเจอข้อมูล Set Auth Level : ${setAuthLavel} 
                Exit For Loop  
                                        
            END 

        END  ##------End Check Set up product usage Auth Level------------------------------


        Log To Console  เริ่มเปิดเว็บไซต์ Cashlink
        F_CASH_Open_Cashlink_Website
        F_CASH_Login Cashlink Maker

        #F_CASH_Click Navbar Payment Menu    # Click Payment Navbar
        #F_CASH_Click Sidebar Payment Menu    # Click Payment Sidebar (Expand)
        Go To  ${CMS_URL_Collection_Upload}  ## กรณีรันแล้วไม่เห็นเมนูข้างๆ

        ${fileUploadStats}  Set Variable  ${False}   #  Set default upload status to false
        
        IF  '${cmsData}[UploadFlag][value]' == 'Y'
            # ------------------------------------------------------------------------------
            # Upload File Section (Maker)
            # ------------------------------------------------------------------------------
            F_CASH_Generate Log Transaction  Maker Upload ===================
            F_CASH_Upload Text File  ${file}   ${MapCode}    # Step for Upload Text File to Cashlink
            ${uploadTableCount}  Get Element Count  ${CMS_Upload_Table}  # Count file upload table
            ${reconcileParameterStatus}  Set Variable  ${True}
            ${foundFileStatus}  Set Variable  ${False}
            FOR  ${i}  IN RANGE  1  ${uploadTableCount} + 1  # Loop for retry to refresh upload screen
                ${fileNameScreen}  C - Get Text  (//*[@id="gridTable"]//tbody/tr/td[2])[${i}]
                ${fullFileName}  Set Variable  ${makerData}[CLIENT]-${fileName}
                IF  '${fullFileName}' == '${fileNameScreen}'
                    ${foundFileStatus}  Set Variable  ${True}
                    FOR  ${j}  IN RANGE  1  ${reloadTime} + 1  # reloadTime from yaml file
                        # Get Data
                        ${statusScreen}  C - Get Text  (//*[@id="gridTable"]//tbody/tr/td[11])[${i}]
                        ${uploadCountScreen}  C - Get Text  (//*[@id="gridTable"]//tbody/tr/td[6])[${i}]
                        ${totalAmountScreen}  C - Get Text  (//*[@id="gridTable"]//tbody/tr/td[8])[${i}]
                        Write Cell  ${fileWorksheet}  ${fileIteratorRow}  4  ${statusScreen}
                        Write Cell  ${fileWorksheet}  ${fileIteratorRow}  1  ${file}
                        Write Cell  ${fileWorksheet}  ${fileIteratorRow}  2  ${uploadCountScreen}
                        Write Cell  ${fileWorksheet}  ${fileIteratorRow}  3  ${totalAmountScreen}
                        
                        IF  '${statusScreen}' == 'Completed'
                            ${amountConverted}  Convert String To Number Format  ${totalAmountScreen}
                            ${rawDataLen}  Get Length  ${rawDataByFormat}
                            ${rawTotalAmount}  Sum Raw Data Amount  ${rawDataByFormat}
                            ${uniqueBatch}  Get Unique From Json Array  ${rawDataByFormat}  Batch_Reference_No
                            #  Reconcile Uploaded Data
                            IF  '${rawDataLen}' == '${uploadCountScreen}'
                                F_CASH_Generate Log Transaction  Transaction Count Pass - Expect: ${rawDataLen} == Actual: ${uploadCountScreen}
                            ELSE
                                F_CASH_Generate Log Transaction  Transaction Count Fail - Expect: ${rawDataLen} != Actual: ${uploadCountScreen}
                                ${reconcileParameterStatus}  Set Variable  ${False}
                            END
                            IF  '${rawTotalAmount}' == '${amountConverted}'
                                F_CASH_Generate Log Transaction  Total Amount Pass - Expect: ${rawTotalAmount} == Actual: ${amountConverted}
                            ELSE
                                F_CASH_Generate Log Transaction  Total Amount Fail - Expect: ${rawTotalAmount} != Actual: ${amountConverted}
                                ${reconcileParameterStatus}  Set Variable  ${False}
                            END
                            # Set File upload status
                            IF  ${reconcileParameterStatus} == ${True}
                                ${fileUploadStats}  Set Variable  ${True}
                            ELSE
                                ${fileUploadStats}  Set Variable  ${False}
                            END

                            # Exit For loop when found batch file and status change to complete
                            Exit For Loop
                        ELSE
                            IF  '${statusScreen}' != 'New'
                                F_CASH_Generate Log Transaction  Upload File: ${fileName} - Incomplete, Status is ${statusScreen}
                                ${fileUploadStats}  Set Variable  ${False}
                                C - Click Element  ${CMS_Navbar_Logout_Btn}
                                #F_CASH_Generate Log Transaction    Upload File Successs!! but has status ${statusScreen}
                                Exit For Loop
                                
                            ELSE
                                Sleep  5
                                Reload Page
                                Continue For Loop
                            END
                        END
                        IF  ${j} == ${reloadTime}
                            F_CASH_Generate Log Transaction  File: ${fileName} - Status not change to Complete, Actual Status is ${statusScreen}
                            ${fileUploadStats}  Set Variable  ${False}
                        END
                    END  #end if reload

                    # If found file upload in table, Exit Loop in table
                    IF  ${foundFileStatus} == ${True}
                        Exit For Loop
                    END
                END  #if filename same

                IF  '${i}' == '${uploadTableCount}'
                    Log To Console  '${i}' == '${uploadTableCount}'
                    F_CASH_Generate Log Transaction  Not found uploaded file: ${fileName}
                    Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[UploadResult]  Failed!
                END
                IF  '${fileUploadStats}' == '${True}'
                    Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[UploadResult]  Passed!
                    Exit For Loop
                END
            END  #for loop retry to refresh
        END  # if upload flag Y/N

        C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Upload_File_${fileName}  # Capture Screen after upload file

        # ------------------------------------------------------------------------------
        # Verify Batch Section (Maker)
        # ------------------------------------------------------------------------------

        IF  '${cmsData}[SubmitFlag][value]' == 'Y' and '${fileUploadStats}' == '${True}'
            ${uniqueBatch}  Get Unique From Json Array  ${rawDataByFormat}  Batch_Reference_No

            #C - Click Element  ${CMS_Sidebar_Payment_Summary}  # Click Summary Menu in Sidebar
            Go To   ${CMS_URL_Collection_Summary}
            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Batch_Data  # Capture screen

            ${countBatchTable}  Get Element Count  ${CMS_Batch_Table}  # Count table of batch screen
            ${batchIterator}  Set Variable  ${1}
            ${insIteratorRow}  Set Variable  ${1}
            FOR  ${batchName}  IN  @{uniqueBatch}
                ${reconcileBatchStatus}  Set Variable  ${True}  # Set default status as True (Success)
                ${batchIteratorRow}  Evaluate  ${batchIterator} + 1  # Set row for write data to excel
                ${batchArray}    Filter Json Array    ${rawDataByFormat}    Batch_Reference_No    ${batchName}  # Filter Json data by Batch Array
                ${batchSummaryTrans}    Get Length    ${batchArray}  # Get Transaction count of batch
                ${batchSummaryAmt}    Sum Raw Data Amount    ${batchArray}  # Summary amount of batch
                

                FOR  ${i}  IN RANGE  1  ${countBatchTable}
                    ${batch}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[7])[${i}]
                    Log To Console   Batch No. :${batch}
                    IF  '${batchName}' == '${batch}'
                        ${product}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[8])[${i}]
                        ${totalInst}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${i}]
                        ${totalAmt}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${i}]
                        ${batchStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[${i}]
                                                
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  1  ${file}
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  2  ${product}
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  3  ${batch}
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  4  ${totalInst}
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  5  ${totalAmt}
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  6  ${batchStatusScreen}
                        
                        ${productStr}  Set Variable  ${product.strip()} 
                        
                        IF  '${batchStatusScreen}' == 'For Submit'
                            # -----------------------------
                            IF  '${batchSummaryTrans}' == '${totalInst}'
                                F_CASH_Generate Log Transaction    Batch Transaction Count Pass - Expect: ${batchSummaryTrans} == Actual: ${totalInst}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Transaction Count Fail - Expect: ${batchSummaryTrans} != Actual: ${totalInst}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                            # -----------------------------
                            IF  '${batchSummaryAmt}' == '${totalAmt}'
                                F_CASH_Generate Log Transaction    Batch Amount Pass - Expect: ${batchSummaryAmt} == Actual: ${totalAmt}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Amount Fail - Expect: ${batchSummaryAmt} != Actual: ${totalAmt}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                            # -----------------------------
                            IF  '${CMS_Product}' == '${productStr}'
                                F_CASH_Generate Log Transaction    Batch Product Pass - Expect: '${CMS_Product}' == Actual: '${productStr}'
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Product Fail - Expect: '${CMS_Product}' == Actual: '${productStr}'
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                        ELSE
                            F_CASH_Generate Log Transaction    Status != For Submit - Actual Status: ${batchStatusScreen}
                            ${reconcileBatchStatus}  Set Variable  ${False}
                            Exit For Loop
                        END
                        # -----------------------------
                        # View Instrument
                        # -----------------------------

                        C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[4]/a[@title="View Deposit"])[${i}]
                                            
                        ${insIterator}  Set Variable  ${1}
                        FOR  ${instruction}  IN  @{batchArray}
                            ${insReconcileStatus}  Set Variable  ${True}
                            ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1
                            ${instructionRefScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${insIterator}]
                            ${instructionProductScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[6])[${insIterator}]
                            ${instructionDebitAccountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[8])[${insIterator}]
                            ${instructionEffectiveDateScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[9])[${insIterator}]
                            ${instructionAmountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${insIterator}]
                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${insIterator}]

                            Write Cell  ${insWorksheet}  ${insIteratorRow}  2  ${batch}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  3  ${instructionRefScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  5  ${instructionDebitAccountScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  6  ${instructionAmountScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  7  ${instructionStatusScreen}

                            
                            F_CASH_Generate Log Transaction    Instruction Ref ${InstructionRefScreen} **********************
                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen   
                            #--------------------------------------------------------------------------
                            #   Submit Transaction | Maker
                            #--------------------------------------------------------------------------
                            # เมื่อ Status เท่ากับ For Submit จะตรวจสอบข้อมูลใน Instruction
                            IF  '${instructionStatusScreen}' == 'For Submit' 
                                IF  '${instruction}[Instruction_Reference_No]' == '${InstructionRefScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Ref Pass - Expect: ${instruction}[Instruction_Reference_No] == Actual: ${InstructionRefScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Ref Fail - Expect: ${instruction}[Instruction_Reference_No] != Actual: ${InstructionRefScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END
                                IF  '${instruction}[Beneficiary/Drawer_Account]' == '${instructionDebitAccountScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Drawer Account Pass - Expect: ${instruction}[Beneficiary/Drawer_Account] == Actual: ${instructionDebitAccountScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Drawer Account Fail - Expect: ${instruction}[Beneficiary/Drawer_Account] != Actual: ${instructionDebitAccountScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END
                                ${instructionAmountConverted}  Convert To Custom Number Format  ${instruction}[Amount]
                                IF  '${instructionAmountConverted}' == '${instructionAmountScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Amount Pass - Expect: ${instructionAmountConverted} == Actual: ${instructionAmountScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Amount Fail - Expect: ${instructionAmountConverted} != Actual: ${instructionAmountScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END

                                # Click to View Instruction Detail
                                C - Click Element By Javascript  (//*[@id='gridTable']/tbody/tr/td[3]/a)[${insIterator}]
                                Wait Until Element Is Visible  ${CMS_Ins_Title}
                                ${clientAccount}  C - Get Text  ${CMS_Ins_CreditAcc}
                                #${clientAccount.strip()}  for trim space
                                F_CASH_Generate Log Transaction    Instruction Client Account: ${clientAccount.strip()}
                                Write Cell  ${insWorksheet}  ${insIteratorRow}  4  ${clientAccount.strip()}
                                C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_Detail_${instructionRefScreen}  # Capture screen

                                IF  '${productStr}' == '${instructionProductScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Product Pass - Expect: Product == ${productStr} - Actual Product ${instructionProductScreen}
                                ELSE 
                                    ${insReconcileStatus}  Set Variable  ${False}
                                    Log To Console  Product on InstrumentScreen is not '${productStr}'
                                    F_CASH_Generate Log Transaction    Instruction Product Fail - Expect: Product != ${productStr} - Actual Product ${instructionProductScreen}
                                    C - Click Element  ${CMS_Navbar_Logout_Btn}
                                    Exit For Loop
                                    Fail  Product on InstrumentScreen is not '${productStr}'  
                                
                                END
                                C - Click Element  ${CMS_DDA_Button_Back}
                                

                            ELSE
                                ${insReconcileStatus}  Set Variable  ${False}
                                F_CASH_Generate Log Transaction    ${instruction}: Status != For Submit - Actual Status: ${batchStatusScreen}
                            END
                            IF  '${insReconcileStatus}' == '${True}'
                                Write Cell  ${insWorksheet}  ${insIteratorRow}  8  Pass!!
                            ELSE
                                Write Cell  ${insWorksheet}  ${insIteratorRow}  8  Fail!!
                            END

                            ${insIterator}  Evaluate  ${insIterator} + 1
                            
                        END

                        # product collection usage Submit @ Batch Summary Screen
                        C - Click Element  ${CMS_Batch_Back_Button}
                        C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[2]/a[@title="Select Record"])[${i}]
                        C - Click Element  ${CMS_BatchSummary_Submit}    # Click Submit Button at Batch Summary Screen
                        C - Click Element  ${CMS_BatchSummary_Confirm_OK}  # Click Ok Button for Confirm to Submit at Batch Summary Screen
                        Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                        #---- View Status after Authorize-------

                        C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[4]/a[@title="View Deposit"])[${i}]
                        C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                        
                        #------------------------------------------------------------
                           
                        ${insIterator}  Set Variable  ${1}
                        ${insIteratorRow}  Set Variable  ${1}
                        FOR  ${instruction}  IN  @{batchArray}
                            ${insReconcileStatus}  Set Variable  ${True}
                            ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1
                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${insIterator}]

                            Write Cell  ${insWorksheet}  ${insIteratorRow}  7  ${instructionStatusScreen}
                            

                            ${insIterator}  Evaluate  ${insIterator} + 1
                        END
                        C - Click Element  ${CMS_DDA_Button_Back}
                        #Exit For Loop
                                                
                    END
                END
                
                ${batchIterator}  Evaluate  ${batchIterator} + 1

                IF  '${reconcileBatchStatus}' == '${False}'
                    Write Cell  ${batchWorksheet}  ${batchIteratorRow}  7  Failed!!
                    Continue For Loop  
                ELSE
                    Write Cell  ${batchWorksheet}  ${batchIteratorRow}  7  Passed!!
                END
            END
            C - Click Element  ${CMS_Navbar_Logout_Btn}
        END

        # ------------------------------------------------------------------------------
        # Verify Batch Section (Checker)
        # ------------------------------------------------------------------------------
        IF  '${cmsData}[AuthFlag][value]' == 'Y' and '${fileUploadStats}' == '${True}'
            F_CASH_Login Cashlink Checker
            # C - Click Element  ${CMS_Navbar_Payment_Menu}  # Click Payment Navbar
            # C - Click Element  ${CMS_Sidebar_Payment}  # Click Payment Sidebar (Expand)
            # C - Click Element  ${CMS_Sidebar_Payment_Summary}  # Click Summary Menu in Sidebar
            Go To   ${CMS_URL_Collection_Summary}
            F_CASH_Generate Log Transaction    Checker ===================
            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Checker_Batch  # Capture screen
            ${batchIterator}  Set Variable  ${1}
            ${insIteratorRow}  Set Variable  ${1}
            ${insIteratorRow1}  Set Variable  ${1}
            FOR  ${batchName}  IN  @{uniqueBatch}
                ${reconcileBatchStatus}  Set Variable  ${True}  # Set default status as True (Success)
                ${batchIteratorRow}  Evaluate  ${batchIterator} + 1  # Set row for write data to excel
                ${batchArray}    Filter Json Array    ${rawDataByFormat}    Batch_Reference_No    ${batchName}  # Filter Json data by Batch Array
                ${batchSummaryTrans}    Get Length    ${batchArray}  # Get Transaction count of batch
                ${batchSummaryAmt}    Sum Raw Data Amount    ${batchArray}  # Summary amount of batch
                
                FOR  ${i}  IN RANGE  1  ${countBatchTable}
                    ${batch}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[7])[${i}]
                    IF  '${batchName}' == '${batch}'
                        ${totalInst}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${i}]
                        ${totalAmt}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${i}]
                        ${batchStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[${i}]
                        Write Cell  ${batchWorksheet}  ${batchIteratorRow}  6  ${batchStatusScreen}
                        IF  '${batchStatusScreen}' == 'Submitted'
                            # -----------------------------
                            IF  '${batchSummaryTrans}' == '${totalInst}'
                                F_CASH_Generate Log Transaction    Batch Transaction Count Pass - Expect: ${batchSummaryTrans} == Actual: ${totalInst}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Transaction Count Fail - Expect: ${batchSummaryTrans} != Actual: ${totalInst}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                            # -----------------------------
                            IF  '${batchSummaryAmt}' == '${totalAmt}'
                                F_CASH_Generate Log Transaction    Batch Amount Pass - Expect: ${batchSummaryAmt} == Actual: ${totalAmt}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Amount Fail - Expect: ${batchSummaryAmt} != Actual: ${totalAmt}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                        ELSE IF  '${batchStatusScreen}' == 'For My Auth' or '${batchStatusScreen}' == 'For Auth'
                            # -----------------------------
                            IF  '${batchSummaryTrans}' == '${totalInst}'
                                F_CASH_Generate Log Transaction    Batch Transaction Count Pass - Expect: ${batchSummaryTrans} == Actual: ${totalInst}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Transaction Count Fail - Expect: ${batchSummaryTrans} != Actual: ${totalInst}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                            # -----------------------------
                            IF  '${batchSummaryAmt}' == '${totalAmt}'
                                F_CASH_Generate Log Transaction    Batch Amount Pass - Expect: ${batchSummaryAmt} == Actual: ${totalAmt}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Amount Fail - Expect: ${batchSummaryAmt} != Actual: ${totalAmt}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                        ELSE
                            F_CASH_Generate Log Transaction    Status != For Submit - Actual Status: ${batchStatusScreen}
                            ${reconcileBatchStatus}  Set Variable  ${False}
                            Exit For Loop
                        END
                        #--------------------------------------------------------------------------
                        #   Authorize Transaction | Checker
                        #--------------------------------------------------------------------------  
                        C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[4]/a[@title="View Deposit"])[${i}]
                        
                        ${insIterator}  Set Variable  ${1}
                        
                        FOR  ${instruction}  IN  @{batchArray}
                            ${insReconcileStatus}  Set Variable  ${True}
                            ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1
                            ${instructionRefScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${insIterator}]
                            ${instructionDebitAccountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[8])[${insIterator}]
                            ${instructionProductScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[6])[${insIterator}]
                            ${instructionAmountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${insIterator}]
                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${insIterator}]
                            
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  1  ${instructionProductScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  2  ${batch}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  3  ${instructionRefScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  5  ${instructionDebitAccountScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  6  ${instructionAmountScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  7  ${instructionStatusScreen}

                            F_CASH_Generate Log Transaction    Instruction Ref ${InstructionRefScreen} **********************
                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                            
                            # เมื่อ Status เท่ากับ For My Auth จะตรวจสอบข้อมูลใน Instruction
                            IF  '${instructionStatusScreen}' == 'For My Auth' or '${instructionStatusScreen}' == 'For Auth'

                                IF  '${instruction}[Instruction_Reference_No]' == '${InstructionRefScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Ref Pass - Expect: ${instruction}[Instruction_Reference_No] == Actual: ${InstructionRefScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Ref Fail - Expect: ${instruction}[Instruction_Reference_No] != Actual: ${InstructionRefScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END
                                IF  '${instruction}[Beneficiary/Drawer_Account]' == '${instructionDebitAccountScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Beneficiary Pass - Expect: ${instruction}[Beneficiary/Drawer_Account] == Actual: ${instructionDebitAccountScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Beneficiary Fail - Expect: ${instruction}[Beneficiary/Drawer_Account] != Actual: ${instructionDebitAccountScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END
                                ${instructionAmountConverted}  Convert To Custom Number Format  ${instruction}[Amount]
                                IF  '${instructionAmountConverted}' == '${instructionAmountScreen}'
                                    F_CASH_Generate Log Transaction    Instruction Amount Pass - Expect: ${instructionAmountConverted} == Actual: ${instructionAmountScreen}
                                ELSE
                                    F_CASH_Generate Log Transaction    Instruction Amount Fail - Expect: ${instructionAmountConverted} != Actual: ${instructionAmountScreen}
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END
                                # -----------------------------
                                IF  '${productStr}' == '${instructionProductScreen}'
                                    F_CASH_Generate Log Transaction    Batch Product Pass - Expect: '${productStr}' == Actual: '${instructionProductScreen}'
                                ELSE
                                    F_CASH_Generate Log Transaction    Batch Product Fail - Expect: '${productStr}' == Actual: '${instructionProductScreen}'
                                    ${insReconcileStatus}  Set Variable  ${False}
                                END
                              
                            ELSE
                                ${insReconcileStatus}  Set Variable  ${False}
                                F_CASH_Generate Log Transaction    ${instruction}[Instruction_Reference_No]: Status != For My Auth - Actual Status: ${batchStatusScreen}
                            END
                            
                            IF  '${insReconcileStatus}' == '${True}'
                                Write Cell  ${insWorksheet}  ${insIteratorRow}  8  Pass!!
                            ELSE
                                Write Cell  ${insWorksheet}  ${insIteratorRow}  8  Fail!!
                                Log To Console    Go to ${instructionStatusScreen}
                            END
                            ${insIterator}  Evaluate  ${insIterator} + 1
                        END
                        
                        IF  '${setAuthLavel}' == 'Instrument'
                            # ---------------------------------------------------------------------------
                            #    Click authorize @ Instrument Detail Screen
                            # ---------------------------------------------------------------------------
                            ${authIterator}  Set Variable  ${1}
                            ${insIterator}  Set Variable  ${1}
                            FOR  ${instruction}  IN  @{batchArray}
                                # Code For Auth Partial
                                IF  '${instruction}[Auth_Flag]' == 'Y'
                                    C - Click Element By Javascript  (//*[@id='gridTable']/tbody/tr/td[2]/a)[${insIterator}]
                                    ${authIterator}  Evaluate  ${authIterator} + 1
                                END
                                ${insIterator}  Evaluate  ${insIterator} + 1
                            END
                                # # Click Auth by selected
                            IF  ${authIterator} > ${1}
                                C - Click Element  ${CMS_Auth_Btn}
                                C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                                C - Click Element  ${CMS_BatchSummary_Confirm_OK}  # Click Ok Button at Instrument Screen
                                Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                                ${instructionMessageScreen}   C - Get Text  //*[@id="messageArea"]/h3/b
                                Log To Console  ${instructionMessageScreen}
                                C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                            END
                                            
                        ELSE IF  '${setAuthLavel}' == 'Batch'
                            Log To Console  set auth level by Batch
                            
                            C - Click Element  ${CMS_Batch_Back_Button}
                            C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[2]/a[@title="Select Record"])[${i}]
                            C - Click Element  ${CMS_BatchSummary_Authorize}    # Click Submit Button at Batch Summary Screen
                            #C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Batch_Reference_No    ${batchName}  # Capture screen
                            C - Click Element  ${CMS_BatchSummary_Confirm_OK}  # Click Ok Button for Confirm to Submit at Batch Summary Screen
                            Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                            ${instructionMessageScreen}   C - Get Text  //*[@id="messageArea"]/h3/b
                            Log To Console  ${instructionMessageScreen}

                            #---- View Status after Authorize-------
                            C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[4]/a[@title="View Deposit"])[${i}]
                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                        
                        END

                        ${insIterator}  Set Variable  ${1}
                        FOR  ${instruction}  IN  @{batchArray}
                            ${insReconcileStatus}  Set Variable  ${True}
                            ${insIteratorRow1}  Evaluate  ${insIteratorRow1} + 1
                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${insIterator}]
                            ${instructionAmountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${insIterator}]
                            Log To Console   view status after Authorize
                            Log To Console   status : ${instructionStatusScreen}
                            #Write Cell  ${insWorksheet}  ${insIteratorRow1}  6  ${instructionAmountScreen} 
                            Write Cell  ${insWorksheet}  ${insIteratorRow1}  7  ${instructionStatusScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow1}  9  ${instructionMessageScreen}
                            
                            ${insIterator}  Evaluate  ${insIterator} + 1

                        END    
                                              
                        C - Click Element  ${CMS_DDA_Button_Back}  
                        #Exit For Loop
                        
                    END
                    
                END
                
                ${batchIterator}  Evaluate  ${batchIterator} + 1

                IF  '${reconcileBatchStatus}' == '${False}'
                    Write Cell  ${batchWorksheet}  ${batchIteratorRow}  7  Failed!!
                    Continue For Loop  
                ELSE
                    Write Cell  ${batchWorksheet}  ${batchIteratorRow}  7  Passed!!
                END
            END
            
            C - Click Element  ${CMS_Navbar_Logout_Btn}
            #F_CASH_Generate Log Transaction   ${instructionMessageScreen}
            F_CASH_Generate Log Transaction    Authorize All Successs!! 
        END
        #--------------------------------------------------------------------------
        #   Send To Bank Transaction | Checker
        #--------------------------------------------------------------------------  
        IF  '${cmsData}[SendAllFlag][value]' == 'Y' and '${fileUploadStats}' == '${True}'
            F_CASH_Login Cashlink Checker
            # C - Click Element  ${CMS_Navbar_Payment_Menu}  # Click Payment Navbar
            # C - Click Element  ${CMS_Sidebar_Payment}  # Click Payment Sidebar (Expand)
            # C - Click Element  ${CMS_Sidebar_Payment_Summary}  # Click Summary Menu in Sidebar

           Go To   ${CMS_URL_Collection_Summary}
            
            ${batchIterator}  Set Variable  ${1}
            ${insIteratorRow}  set Variable  ${1}

            FOR  ${batchName}  IN  @{uniqueBatch}

                ${reconcileBatchStatus}  Set Variable  ${True}  # Set default status as True (Success)
                ${batchArray}    Filter Json Array    ${rawDataByFormat}    Batch_Reference_No    ${batchName}  # Filter Json data by Batch Array
                Log To Console  Process SendTobank : ${batchName}
                FOR  ${i}  IN RANGE  1  ${countBatchTable}
                    ${batch}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[7])[${i}]
                    Log To Console  Process SendTobank :found batch in ${batchName} in batch summary

                    IF  '${batchName}' == '${batch}'
                        ${sendIterator}  Set Variable  ${1}
                        ${insIterator}  Set Variable  ${1}
                        ${insReconcileStatus}  Set Variable  ${True}
                        Log To Console  Process SendTobank : ${batchName} == ${batch}
                        Log To Console  instIterator : ${insIterator} 
                        C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[4]/a[@title="View Deposit"])[${i}]

                        FOR  ${instruction}  IN  @{batchArray}

                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${insIterator}]
                            ${instructionProductScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[6])[${insIterator}]
                            ${instructionRefScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${insIterator}]
                            Log To Console   ${instructionStatusScreen}
                            IF  '${instructionStatusScreen}' == 'Sent to Bank' or '${instructionStatusScreen}' == 'Sent to Processing'
                                Log To Console  Transaction Set Auto Sent to Bank 
                                F_CASH_Generate Log Transaction  Transaction Set Auto Sent to Bank
                                #Log To Console  ${instructionStatusScreen}
                                ${insReconcileStatus}  Set Variable  ${False}
                                Exit For Loop  
                            ELSE  
                                F_CASH_Generate Log Transaction  Transaction Status ${instructionStatusScreen}
                            END

                            ${insIterator}  Evaluate  ${insIterator} + 1
                        END
                        
                        IF  '${insReconcileStatus}' == '${True}' and '${setAuthLavel}' == 'Instrument'
                            Log To Console  set auth level by Instrument

                            ${sendIterator}  Set Variable  ${1}
                            ${insIterator}  Set Variable  ${1}
                            FOR  ${instruction}  IN  @{batchArray}
                                IF  '${instruction}[SendAll_Flag]' == 'Y'
                                    C - Click Element By Javascript  (//*[@id='gridTable']/tbody/tr/td[2]/a)[${insIterator}]
                                    ${sendIterator}  Evaluate  ${sendIterator} + 1
                                END 

                                ${insIterator}  Evaluate  ${insIterator} + 1
                            END
                            IF  ${sendIterator} > ${1}
                                C - Click Element  ${CMS_DDA_InsSend_Btn}  ## click Send to bank  Button
                                C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                                C - Click Element  ${CMS_SendAll_Confirm}
                                ${instructionMessageScreen}   C - Get Text  //*[@id="messageArea"]/h3/b
                                Log To Console  ${instructionMessageScreen}                            
                                Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                            END

                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen

                        ELSE IF  '${insReconcileStatus}' == '${True}' and '${setAuthLavel}' == 'Batch'
                            Log To Console  set auth level by Batch
                            C - Click Element  ${CMS_DDA_Button_Back} 
                            C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[2]/a[@title="Select Record"])[${i}]
                            C - Click Element  ${CMS_BatchSummary_Send}    # Click Submit Button at Batch Summary Screen
                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                            C - Click Element  ${CMS_BatchSummary_Confirm_OK}  # Click Ok Button for Confirm to Submit at Batch Summary Screen
                            Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                            ${instructionMessageScreen}   C - Get Text  //*[@id="messageArea"]/h3/b
                            Log To Console  ${instructionMessageScreen}

                            #---- View Status after Authorize-------
                            C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[4]/a[@title="View Deposit"])[${i}]
                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                        ELSE  
                            ${instructionMessageScreen}  Set Variable  Transaction Set Auto Sent to Bank
                            ${insReconcileStatus}  Set Variable  ${False}
                            #Exit For Loop
                        END
                                         
                        ## ใส่ผล output last status ##
                        ${insIterator}  Set Variable  ${1}
                        
                        FOR  ${instruction}  IN  @{batchArray}
                            
                            ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1
                            ${instructionRefScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${insIterator}]
                            ${instructionProductScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[6])[${insIterator}]
                            ${instructionDebitAccountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[8])[${insIterator}]
                            ${instructionEffectiveDateScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[9])[${insIterator}]
                            ${instructionAmountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${insIterator}]
                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${insIterator}]

                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  1  ${instructionProductScreen}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  2  ${batch}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  3  ${instructionRefScreen}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  5  ${instructionDebitAccountScreen}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  4  ${clientAccount.strip()}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  6  ${instructionAmountScreen}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  7  ${instructionStatusScreen}
                            Write Cell  ${txnWorksheet}  ${insIteratorRow}  9  ${instructionMessageScreen}
                            
                            
                            IF  '${insReconcileStatus}' == '${True}'
                                Write Cell  ${txnWorksheet}  ${insIteratorRow}  8  Passed!!
                            ELSE
                                Write Cell  ${txnWorksheet}  ${insIteratorRow}  8  Failed!!
                            END 

                            ${insIterator}  Evaluate  ${insIterator} + 1
                            
                        END 
                        C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen
                        C - Click Element  ${CMS_Batch_Back_Button}

                         ${insIterator}  Evaluate  ${insIterator} + 1
                    END
              
                END
                
                 
            END

            # Click Send to Bank All 
            C - Click Element  ${CMS_Navbar_Logout_Btn}
            #F_CASH_Generate Log Transaction    ${instructionMessageScreen}
            IF  '${insReconcileStatus}' == '${True}'
                F_CASH_Generate Log Transaction    Send to Bank All Successs!!
            ELSE  
                F_CASH_Generate Log Transaction    All Auto Send to Bank!!
            END
        END

        # Write Result
        IF  '${fileUploadStats}' == '${True}'
            Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[UploadResult]  Passed!!
            Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[Result]  All Passed
            Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[ResultPath]  ${resultPath}
        ELSE
            Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[UploadResult]  Failed!!
            Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[Result]  Some Process Failed!!
            Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[ResultPath]  ${resultPath}
        END
        # Set File Iterator
        ${fileIterator}  Evaluate  ${fileIterator} + 1
    END

