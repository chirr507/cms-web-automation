*** Settings ***
Resource    ${EXECDIR}\\Resources\\__init_resources__.robot
Suite Setup    C - Initial Setup
#Test Teardown  F_CASH_CMS INQ Teardown
# '*************************************************************************************************
#* Date                * Changed By     * Description                           * version
#---------------------------------------------------------------------------------------------------
#* 11/03/2022            Jenjira H.       Initial version                         1.0
#*                                        
#****************************************************************************************************
***Test Cases***    
TC_CMS_AccountSummary_INQ_001
    [Documentation]    Inquiry account statement 
    [Setup]  INQ - Initial Setup
    [Teardown]   F_CASH_CMS TC_ACCINQ Teardown
    #/S_CMS_DDA_Account_INQ_20220323.robot
    Set Suite Variable  ${transactionLog}  ${EMPTY}        # Define Empty Variable for Write Log
    
    # Open browser
    F_CASH_Open_Cashlink_Website
    # Login CMS
    F_CASH_Login Cashlink Maker
    Log To Console  Open Cashlink in browser and login 

    # Go to Account Summary Screen
    Go To  ${CMS_Account_LinkURL}
    Wait Until Element Is Visible  ${CMS_PageTitle_ACCO}   timeout=10s
   
    # Check Element
    ${slideopenSaving}   Get Element Attribute  //p[contains(@class, "acord_grid_head") and text()="Savings Account"]  class
    ${slideopenCurrent}  Get Element Attribute  //p[contains(@class, "acord_grid_head") and text()="Current Account"]  class
    ${countTableSA}  Get Element Count  ${CMS_ACCO_Table}
    #Log To Console  Rows count :${countTableSA}
    F_CASH_Generate Log Transaction  =========== Account Summary Page ============
    # check sider saving / current is open ?
    ${statusSaving}   Run Keyword And Return Status  Should Contain  ${slideopenSaving}    acord_open
    
   
    ${uniqueAcc}  get_unique_from_json_array_by_cellDetail  ${instInqData}  Bene_Account
    Log To Console  filter by Account : ${uniqueAcc}
    F_CASH_Generate Log Transaction  Inquiry statement by account : ${uniqueAcc}

    ${stmIteratorRow}  Set Variable  ${1}
    FOR  ${AccData}  IN  @{uniqueAcc}
        ${accType}  Get Substring  ${AccData}   3  4  
        Log To Console  Account : ${AccData} type : ${accType}

        IF  '${accType}' == '9' or '${accType}' == '1'
            Log To Console  Search Saving Account
            IF  ${statusSaving} !=${True}
                C - Click Element  ${CMS_ACCO_Sider_SA}
            END
            #//*[@id="gridTable"]/tbody/tr
            FOR  ${i}  IN RANGE  1  ${countTableSA}
                ${accountScreen}   C - Get Text  //*[@id="gridTable"]/../div[contains(@style, "display: block;")]/table/tbody/tr[${i}]/td[4]  #find account on screen
                #Log To Console  Account on screen :${accountScreen}  
                
                IF  '${accountScreen}' == '${AccData}'  # if account equal 
                    C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  INQ_SA_${AccData}
                    F_CASH_Generate Log Transaction  Get Today Statement Saving Account :${accountScreen}  #capture screen 
                    C - Click Element  //*[@id="brAccountNo"]/a[contains (text(),'${accountScreen}')]  #Click link to Account Activity screen
                    C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  INQ_STM_SA_${AccData}   #capture screen
                    ${coutStmAccTable}  Get Element Count  //*[@id="gridTable"]/tbody/tr/td[@class="dateCell w4"]  # get row in account activity
                    Log To Console  count table :${coutStmAccTable}

                    ${stmIterator}  Set Variable  ${1}
                    #-- Get Text on Screen--
                    FOR  ${stmActivity}  IN RANGE  1  ${coutStmAccTable}+${1}
                        ${stmIteratorRow}  Evaluate  ${stmIteratorRow} + 1  # Set row for write data to excel 
                        Log To Console  ${stmIteratorRow}

                        ${info}  Run Keyword And Return Status  Wait Until Element Is Visible  //*[@id="gridTable"]/tbody/tr[${stmIterator}]/td/a[@class="grid-link-icon infolink"]
                        IF  ${info} == ${True}
                            Log To Console  Get text in additional info link
                            C - Click Element  //*[@id="gridTable"]/tbody/tr[${stmIterator}]/td/a[@class="grid-link-icon infolink"]
                            
                            ${infoPIR_No}           C - Get Text  //div[@class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable" and contains(@style, "display: block;")]//span[@id="fld_pirRefNo"]
                            ${infoBatch_RefNo}      C - Get Text  //div[@class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable" and contains(@style, "display: block;")]//span[@id="fld_batchRefNo"]
                            ${infoTotalAmount}      C - Get Text  //div[@class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable" and contains(@style, "display: block;")]//span[@id="fld_totalPirAmnt"]
                            ${infoBankProduct}      C - Get Text  //div[@class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable" and contains(@style, "display: block;")]//span[@id="fld_bankProd"]
                            Write Cell  ${todayStmSheet}  ${stmIteratorRow}  1  ${infoPIR_No}
                            Write Cell  ${todayStmSheet}  ${stmIteratorRow}  2  ${infoBatch_RefNo}
                            Write Cell  ${todayStmSheet}  ${stmIteratorRow}  3  ${infoTotalAmount}
                            Write Cell  ${todayStmSheet}  ${stmIteratorRow}  4  ${infoBankProduct}
                            #-------------------------------------------------------
                            #  Reconclie Transaction with statement Activity
                            #-------------------------------------------------------

                            log To Console  PIR : ${infoPIR_No}
                            C - Click Element  ${CMS_Info_Close}
                        END               
                        ${stmDate}              C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[2])[${stmIterator}]
                        ${stmTime}              C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[3])[${stmIterator}]
                        ${stmTxnCode}           C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[4])[${stmIterator}]
                        ${stmRef}               C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[5])[${stmIterator}]
                        ${stmBackDate}          C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[6])[${stmIterator}]
                        ${stmDrAmount}          C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[7])[${stmIterator}]
                        ${stmCrAmount}          C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[8])[${stmIterator}]
                        ${stmBal}               C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[9])[${stmIterator}]
                        ${stmBankRefNo}         C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[10])[${stmIterator}]
                        #${stmBranch}            C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[11])[${stmIterator}]
                        #${stmITEM}              C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[12])[${stmIterator}]
                        #${stmInfo}              C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[13])[${stmIterator}]
                    
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  5  ${stmDate}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  6  ${stmTime}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  7  ${AccData}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  8  ${stmTxnCode}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  9  ${stmRef}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  10  ${stmBackDate}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  11  ${stmDrAmount}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  12  ${stmCrAmount}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  13  ${stmBal}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  14  ${stmBankRefNo}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  18  Completed!!

                        ${stmIterator}  Evaluate  ${stmIterator} + 1
                        
                    END
                    
                    F_CASH_Generate Log Transaction   Get Statement Completed!!
                    C - Click Element  ${CMS_Back_Button} 
                    Exit For Loop 
                
                END
            END

        ELSE IF  '${accType}' == '0'
            Log To Console  Search Current Account
            
            ${statusCurrent}  Run Keyword And Return Status  Should Contain  ${slideopenCurrent}   acord_open
            Wait Until Element Is Visible  ${CMS_ACCO_Table}  timeout=10
            ${countTableCA}  Get Element Count  ${CMS_ACCO_Table}
            #Log To Console  ${countTableCA}
            IF  ${statusCurrent} !=${True}
                Wait Until Element Is Visible  ${CMS_ACCO_Sider_CA}  timeout=10
                C - Click Element  ${CMS_ACCO_Sider_CA}
                Sleep  5s
            END
            FOR  ${i}  IN RANGE  1  ${countTableCA}
                
                Wait Until Element Is Visible  //*[@id="gridTable"]/../div[contains(@style, "display: block;")]/table/tbody/tr[${i}]/td[4]  timeout=20
                ${accountScreen}   C - Get Text  //*[@id="gridTable"]/../div[contains(@style, "display: block;")]/table/tbody/tr[${i}]/td[4]  #find account on screen
                #Log To Console  Account on screen :${accountScreen}  

                IF  '${accountScreen}' == '${AccData}'  # if account equal 
                    C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  INQ_CA_${AccData}
                    F_CASH_Generate Log Transaction  Get Today Statement ${accountScreen}
                    C - Click Element  //*[@id="brAccountNo"]/a[contains (text(),'${accountScreen}')]  #Click link to Account Activity screen
                    C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  INQ_STM_CA_${AccData}
                    ${coutStmAccTable}  Get Element Count  //*[@id="gridTable"]/tbody/tr/td[@class="dateCell w4"]  # get row in account activity
                    Log To Console  count table :${coutStmAccTable}

                    ${stmIterator}  Set Variable  ${1}
                    #-- Get Text on Screen--
                    FOR  ${stmActivity}  IN RANGE  1  ${coutStmAccTable}+${1}
                        ${stmIteratorRow}  Evaluate  ${stmIteratorRow} + 1  # Set row for write data to excel 
                        Log To Console  ${stmIteratorRow}
                        
                        ${info}  Run Keyword And Return Status  Wait Until Element Is Visible  //*[@id="gridTable"]/tbody/tr[${stmIterator}]/td/a[@class="grid-link-icon infolink"]
                        IF  ${info} == ${True}
                            Log To Console  Get text in additional info link
                            C - Click Element  //*[@id="gridTable"]/tbody/tr[${stmIterator}]/td/a[@class="grid-link-icon infolink"]
                            
                            ${infoPIR_No}           C - Get Text  //div[@class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable" and contains(@style, "display: block;")]//span[@id="fld_pirRefNo"]
                            ${infoBatch_RefNo}      C - Get Text  //div[@class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable" and contains(@style, "display: block;")]//span[@id="fld_batchRefNo"]
                            ${infoTotalAmount}      C - Get Text  //div[@class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable" and contains(@style, "display: block;")]//span[@id="fld_totalPirAmnt"]
                            ${infoBankProduct}      C - Get Text  //div[@class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable" and contains(@style, "display: block;")]//span[@id="fld_bankProd"]
                            Write Cell  ${todayStmSheet}  ${stmIteratorRow}  1  ${infoPIR_No}
                            Write Cell  ${todayStmSheet}  ${stmIteratorRow}  2  ${infoBatch_RefNo}
                            Write Cell  ${todayStmSheet}  ${stmIteratorRow}  3  ${infoTotalAmount}
                            Write Cell  ${todayStmSheet}  ${stmIteratorRow}  4  ${infoBankProduct}
                            #-------------------------------------------------------
                            #  Reconclie Transaction with statement Activity
                            #-------------------------------------------------------

                            log To Console  PIR : ${infoPIR_No}
                            C - Click Element  ${CMS_Info_Close}
                        END               
                        
                        ${stmDate}              C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[2])[${stmIterator}]
                        ${stmTime}              C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[3])[${stmIterator}]
                        ${stmTxnCode}           C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[4])[${stmIterator}]
                        ${stmRef}               C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[5])[${stmIterator}]
                        ${stmBackDate}          C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[6])[${stmIterator}]
                        ${stmDrAmount}          C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[7])[${stmIterator}]
                        ${stmCrAmount}          C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[8])[${stmIterator}]
                        ${stmBal}               C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[9])[${stmIterator}]
                        ${stmBankRefNo}         C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[10])[${stmIterator}]
                        #${stmBranch}            C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[11])[${stmIterator}]
                        #${stmITEM}              C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[12])[${stmIterator}]
                        #${stmInfo}              C - Get Text  (//table[@id="gridTable"]/tbody/tr/td[13])[${stmIterator}]

                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  5  ${stmDate}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  6  ${stmTime}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  7  ${AccData}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  8  ${stmTxnCode}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  9  ${stmRef}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  10  ${stmBackDate}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  11  ${stmDrAmount}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  12  ${stmCrAmount}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  13  ${stmBal}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  14  ${stmBankRefNo}
                        Write Cell  ${todayStmSheet}  ${stmIteratorRow}  18  Completed!!

                        ${stmIterator}  Evaluate  ${stmIterator} + 1
                         
                    END
                    F_CASH_Generate Log Transaction   Get Statement Completed!!
                    C - Click Element  ${CMS_Back_Button} 
                    Exit For Loop 
                
                END
            END
                                        

        ELSE
            Log To Console  Account not CASA
            F_CASH_Generate Log Transaction  Accounnt type not Saving or Current Account
            Exit For Loop  
        END
            
            
    END
    # Save Excel Workbook  ${workbook}  TestData\\CMS_TxnData.xlsx 
    # Close Excel Workbook  ${workbook}
TC_CMS_Status_INQ_001
    [Documentation]   inquiry batch and instrument status after debited
    [Setup]     INQ - Status Initial Setup 
    [Teardown]   F_CASH_CMS TC_STINQ Teardown
    
    #robot Tests/S_CMS_Account_INQ_20220311.robot
    Log To Console  เริ่มเปิดเว็บไซต์ Cashlink
    F_CASH_Open_Cashlink_Website
    F_CASH_Login Cashlink Maker
    Go To  ${CMS_URL_Collection_Summary}   # ไปที่หน้า collection summary
    Wait Until Element Is Visible  ${CMS_Batch_Table}  timeout=30
    ${countBatchTable}  Get Element Count  ${CMS_Batch_Table}  # Count table of batch screen
    ${batchIterator}  Set Variable  ${1}
    ${batchIteratorRow}  Set Variable  ${1}
    ${insIteratorRow}  Set Variable  ${1}
    ${uniqueBatch}  get_unique_from_json_array_by_cellDetail  ${instInqData}  Batch_RefNo  #Log To Console  ${uniqueBatch}
    ${uniqueProduct}  get_unique_from_json_array_by_cellDetail  ${instInqData}  Product
      
    Log To Console  ${uniqueBatch}
    Log To Console  ${uniqueProduct}

    FOR  ${batchName}  IN  @{uniqueBatch}
        ${batchIteratorRow}  Evaluate  ${batchIteratorRow} + 1  # Set row for write data to excel
        Log To Console   Batch No. : ${batchName}
    
        FOR  ${i}  IN RANGE  1  ${countBatchTable}
            ${batch}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[7])[${i}]
            
            IF  '${batchName}' == '${batch}'
                
                ${batchArray}  filter_json_with_cell_detail  ${instInqData}  Batch_RefNo  ${batchName}  # filter batch for loop instrument
                #Log To Console  batchArray = ${batchArray}
                Log To Console  Geting batch status.....
                Log To Console  batch row = ${batchIteratorRow}

                ${batchStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[${i}]
                Write Cell  ${batchWorksheet}  ${batchIteratorRow}  8  ${batchStatusScreen}
                Log To Console  ${batchStatusScreen}
                C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Batch_${batchStatusScreen}
                #----- View Instument----------
                C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[4]/a[@title="View Deposit"])[${i}]
                Log To Console  view instrument status
                C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instrument_${batchStatusScreen}
                
                ${insIterator}  Set Variable  ${1}  #set row table for loop
                
                FOR  ${instruction}  IN  @{batchArray}
                    ${insReconcileStatus}  Set Variable  ${True}
                    ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1

                    Log To Console  Geting instrument status..... ${instruction}[Instruction_Ref][value] 
                    
                    ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${insIterator}]
                    Log To Console  ${instructionStatusScreen}
                    Write Cell  ${insWorksheet}  ${insIteratorRow}  10  ${instructionStatusScreen}

                END
                Log To Console  Already done!!
                F_CASH_Generate Log Transaction  Completed!!
                C - Click Element  ${CMS_Back_Button}

            END
        END
    END
    
    C - Click Element  ${CMS_Navbar_Logout_Btn}
    # Save Excel Workbook  ${workbook}  TestData\\CMS_TxnData.xlsx 
    # Close Excel Workbook  ${workbook}

  
    

    

