***Settings****
Resource    ${EXECDIR}\\Resources\\__init_resources__.robot
Suite Setup    C - Initial Setup
Test Teardown  F_RTP_CMS Teardown

*** Variables ***
${cmsPaymentSummaryUrl}    https://uat2.krungsribizonline.com/GCPCW/showPaymentsListBatch.form

***Test Cases***
Test_UPLOAD_001
    [Documentation]    robot .\Tests\S_CMS_RTP_PAY_20220307r1.robot
    [Setup]  RTP - PAY Setup
    #${MakerID}  Set Variable  ${configurationsData}[MakerID][value]
    #${CheckerID}  Set Variable  ${configurationsData}[CheckerID][value]
    F_CASH_Open_Cashlink_Website
    F_CASH_Login Cashlink Maker
    F_CASH_Click Navbar Service Menu   # Click Service Menu
    F_CASH_Click link BayIneternal     # Click link to BAY Internal
    #---- Switch Browser from CMS to RTP ------#
    Switch Window  NEW  # สลับหน้าจอ RTP
    Maximize Browser Window  # ขยายจอRTP
    F_CASH_Click Sidebar RTP_Instruction Menu  
    F_CASH_Click Sidebar RTP_Receiving Menu
    Select Frame  ${RTP_iframe_Content}
    F_CMS_RTP_Wait until Loader Is Not Visible
    # C - Select From List By Value  //select[@ng-model="dayfilter"]  today
    ${rtpSuccessCount}  Set Variable  ${0}
    FOR  ${batchDetailData}  IN  @{batchDetailInputDataArray}

        ${RunFlag}  Set Variable  ${batchDetailData}[Run_Flag][value]
        ${testCaseRef}  Set Variable  ${batchDetailData}[TCS_Ref][value]
        ${Pay_Flag}  Set Variable  ${batchDetailData}[Pay_Flag][value]
        ${Product_Type}  Set Variable  ${batchDetailData}[Product_Type][value]
        ${Sender_Proxy_Type}  Set Variable  ${batchDetailData}[Sender_Proxy_Type][value]
        ${Sender_Proxy_ID}  Set Variable  ${batchDetailData}[Sender_ProxyID][value]
        ${Receiver_Proxy_Type}  Set Variable  ${batchDetailData}[Receiver_ProxyType][value]
        ${Receiver_Proxy_ID}  Set Variable  ${batchDetailData}[Receiver_ProxyID][value]
        ${Raw_Payment_Amount}  Set Variable  ${batchDetailData}[Amount][value]
        ${Payment_Amount}  Replace String  ${Raw_Payment_Amount}  '  ${EMPTY}  count=-1
        ${Entry_Date}  Set Variable  ${batchDetailData}[EntryDate][value]
        ${Ref1}  Set Variable  ${batchDetailData}[Reference1][value]
        ${Ref2}  Set Variable  ${batchDetailData}[Reference2][value]
        ${Ref3}  Set Variable  ${batchDetailData}[Reference3][value]

        C - Select From List By Value  ${RTP_Select_Dropdown_receiverId}  string:${Receiver_Proxy_ID}
        F_CMS_RTP_Wait until Loader Is Not Visible

        IF  '${RunFlag}' == 'Y'
            IF  '${Product_Type}' == 'Pay Alert'
                F_CASH_Click RTP Pay Alert Tab
                # Get Header Index Dict Bill Alert
                ${headerDict}  F_CASH_Get Pay Alert Table Header

                ${pagesArray}  Get List Items  ${RTP_Dropdown_List_Total_Page}
                ${pageCount}  Get Length  ${pagesArray}
                Log To Console  Page ทั้งหมด: ${pageCount}

                ${foundTransStatus}  Set Variable  ${False}
                FOR  ${page}  IN RANGE  0  ${pageCount}
                    Select From List By Index  ${RTP_Dropdown_List_Total_Page}  ${page}
                    F_CMS_RTP_Wait until Loader Is Not Visible
                    Log To Console  กำลังค้นหา Page Index: ${page}
                    ${rowCount}  Get Element Count  ${RTP_Result_Table_Row}
                    IF  ${rowCount} > 0
                        FOR  ${iterator}  IN RANGE  1  ${rowCount}+${1}

                            ${entryDateScreen}  Get Text  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[${headerDict}[Entry Date]]
                            ${amountScreen}  Get Text  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[${headerDict}[Amount]]
                            ${statusScreen}  Get Text  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[${headerDict}[Status]]
                            
                            IF  '${Payment_Amount}' == '${amountScreen}' and '${Entry_Date}' == '${entryDateScreen}' and '${statusScreen}' == 'New'
                                C - Click Element  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[3]/a[@ng-click="showdetail(item)"]
                                Sleep  2s
                                ${productName}  C - Get Value  ${RTP_Modal_Input_Product}
                                ${RefId}  C - Get Value  ${RTP_Modal_Input_RefId}
                                ${BillerSenderAccountNo}  C - Get Value  ${RTP_Modal_Input_BillerSenderAccountNo}
                                ${SenderProxyType}  C - Get Value  ${RTP_Modal_Input_SenderProxyType}
                                ${SenderProxyId}  C - Get Value  ${RTP_Modal_Input_SenderProxyId}
                                ${ReceiverProxyId}  C - Get Value  ${RTP_Modal_Input_ReceiverProxyId}
                                ${ReceiverProxyType}  C - Get Value  ${RTP_Modal_Input_ReceiverProxyType}
                                ${Amount}  C - Get Value  ${RTP_Modal_Input_Amount}
                                ${EntryDate}  C - Get Value  ${RTP_Modal_Input_Entry_Date}
                                ${DueDate}  C - Get Value  ${RTP_Modal_Input_Due_Date}
                                ${BillerReference1}  C - Get Value  ${RTP_Modal_Input_Biller_Reference_1}
                                ${BillerReference2}  C - Get Value  ${RTP_Modal_Input_Biller_Reference_2}
                                ${BillerReference3}  C - Get Value  ${RTP_Modal_Input_Biller_Reference_3}
                                C - Click Element  ${RTP_Close_Popup_Button}
                                C - Click Element  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[2]//input
                                ${foundTransStatus}  Set Variable  ${True}
                            END
                            Exit For Loop If  ${foundTransStatus} == ${True}
                        END
                    ELSE
                        Log To Console  Table is empty
                    END
                    Exit For Loop If  ${foundTransStatus} == ${True}
                    IF  ${page} == ${pageCount}-${1}
                        ${notFoundMessage}  Set Variable  ไม่พบ Transaction Amount = ${amountScreen} ในระบบ
                        Log to Console  ${notFoundMessage}
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Pay_ResultMessage]   ${notFoundMessage}
                    END
                END
            ELSE
                F_CASH_Click RTP Bill Alert Tab
                # Get Header Index Dict Bill Alert

                ${headerDict}  F_CASH_Get Bill Alert Table Header

                ${pagesArray}  Get List Items  ${RTP_Dropdown_List_Total_Page}
                ${pageCount}  Get Length  ${pagesArray}
                ${foundTransStatus}  Set Variable  ${False}
                FOR  ${page}  IN RANGE  0  ${pageCount}
                    Select From List By Index  ${RTP_Dropdown_List_Total_Page}  ${page}
                    F_CMS_RTP_Wait until Loader Is Not Visible
                    ${rowCount}  Get Element Count  ${RTP_Result_Table_Row}
                    IF  ${rowCount} > 0
                        FOR  ${iterator}  IN RANGE  1  ${rowCount}+${1}
                            ${entryDateScreen}  C - Get Text  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[${headerDict}[Entry Date]]
                            ${amountScreen}  C - Get Text  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[${headerDict}[Amount]]
                            ${statusScreen}  C - Get Text  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[${headerDict}[Status]]
                            ${ref1Screen}  C - Get Text  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[${headerDict}[Reference 1]]
                            ${ref2Screen}  C - Get Text  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[${headerDict}[Reference 2]]
                            
                            IF  '${Payment_Amount}' == '${amountScreen}' and '${statusScreen}' == 'New' and '${Ref1}' == '${ref1Screen}' and '${Ref2}' == '${ref2Screen}'
                                Log To Console  '${Payment_Amount}' == '${amountScreen}' and '${statusScreen}' == 'New' and '${Ref1}' == '${ref1Screen}' and '${Ref2}' == '${ref2Screen}'  stream=STDOUT  no_newline=False
                                ${foundTransStatus}  Set Variable  ${True}
                                IF  '${Entry_Date}' != ''
                                    IF  '${Entry_Date}' == '${entryDateScreen}'
                                        ${foundTransStatus}  Set Variable  ${True}
                                    ELSE
                                        ${foundTransStatus}  Set Variable  ${False}
                                    END
                                END 

                                IF  ${foundTransStatus} == ${True}
                                    C - Click Element  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[3]/a[@ng-click="showdetail(item)"]
                                    Sleep  2s
                                    ${productName}  C - Get Value  ${RTP_Modal_Input_Product}
                                    ${RefId}  C - Get Value  ${RTP_Modal_Input_RefId}
                                    ${BillerSenderAccountNo}  C - Get Value  ${RTP_Modal_Input_BillerSenderAccountNo}
                                    ${SenderProxyType}  C - Get Value  ${RTP_Modal_Input_SenderProxyType}
                                    ${SenderProxyId}  C - Get Value  ${RTP_Modal_Input_SenderProxyId}
                                    ${ReceiverProxyId}  C - Get Value  ${RTP_Modal_Input_ReceiverProxyId}
                                    ${ReceiverProxyType}  C - Get Value  ${RTP_Modal_Input_ReceiverProxyType}
                                    ${Amount}  C - Get Value  ${RTP_Modal_Input_Amount}
                                    ${EntryDate}  C - Get Value  ${RTP_Modal_Input_Entry_Date}
                                    ${DueDate}  C - Get Value  ${RTP_Modal_Input_Due_Date}
                                    ${BillerReference1}  C - Get Value  ${RTP_Modal_Input_Biller_Reference_1}
                                    ${BillerReference2}  C - Get Value  ${RTP_Modal_Input_Biller_Reference_2}
                                    ${BillerReference3}  C - Get Value  ${RTP_Modal_Input_Biller_Reference_3}
                                    C - Click Element  ${RTP_Close_Popup_Button}
                                    F_CMS_RTP_Wait until Loader Is Not Visible
                                    C - Click Element  //body[@ng-app="app"]//div[@class="wrapper"]//div[@class="table-responsive "]//table/tbody/tr[${iterator}]/td[2]//input
                                END
                            END
                            Exit For Loop If  ${foundTransStatus} == ${True}
                        END
                    ELSE
                        Log To Console  Table is empty
                    END
                    Exit For Loop If  ${foundTransStatus} == ${True}
                    IF  ${page} == ${pageCount}
                        ${notFoundMessage}  Set Variable  ไม่พบ Transaction Amount = ${amountScreen} ในระบบ
                        Log to Console  ${notFoundMessage}
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Pay_ResultMessage]  ${notFoundMessage}
                    END
                END
            END

            IF  ${foundTransStatus} == ${True}
                IF  '${Pay_Flag}' == 'Y'
                    Log To Console  Click Payment on This Transaction
                    C - Click Element  ${RTP_Pay_Button}
                    C - Click Element  ${RTP_Confirm_Pay_Button}
                    F_CMS_RTP_Wait until Loader Is Not Visible
                    ${alertMessage}  C - Get Text  ${RTP_Alert_Message}
                    ${successStatus}  Run Keyword And Return Status  Should Contain  ${alertMessage}  Success
                    IF  ${successStatus} == ${True}
                        ${rtpSuccessCount}  Evaluate  ${rtpSuccessCount}+${1}
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Pay_Result]  Pass
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Pay_ResultMessage]  ${alertMessage}
                    ELSE
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Pay_Result]  Fail
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Pay_ResultMessage]  ${alertMessage}
                    END
                    C - Click Element  ${RTP_Alert_Close_Button}
                ELSE
                    Log To Console  Skip Payment on This Transaction
                END
            ELSE
                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Pay_Result]  Fail
                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Pay_ResultMessage]  Not Found Transaction
            END
        END
    END

    F_CASH_Logout RTP and CMS

    # Submit Transaction
    F_CASH_Open_Cashlink_Website
    F_CASH_Login Cashlink Maker
    Go to  ${cmsPaymentSummaryUrl}
    ${batchDetailInputDataArray}  Read Excel Sheet  ${batchDetailInputWorksheet}  cellDetail=${True}
    ${paymentSummaryHeaderIndex}  F_CASH_Find_Payments_Summary_Table_Header_Index
    FOR  ${batchDetailData}  IN  @{batchDetailInputDataArray}
        
        ${RunFlag}  Set Variable  ${batchDetailData}[Run_Flag][value]
        ${Pay_Flag}  Set Variable  ${batchDetailData}[Pay_Flag][value]
        ${myProduct}  Set Variable  ${batchDetailData}[Product_Type][value]
        ${payResult}  Set Variable  ${batchDetailData}[Pay_Result][value]
        
        IF  '${myProduct}' == 'Pay Alert'
            ${myProduct}  Set Variable  RTPPAYCT
        ELSE
            ${myProduct}  Set Variable  RTPPAYBP
        END

        ${productConfig}  Filter Json Array  ${productConfigData}   PRODUCT  ${myProduct}  
        ${productConfig}  Filter Json Array  ${productConfig}       CLIENT   ${makerData}[CLIENT] 
        ${productConfig}  Set Variable  ${productConfig}[0]

        ${RunFlag}  Set Variable  ${batchDetailData}[Run_Flag][value]
        ${testCaseRef}  Set Variable  ${batchDetailData}[TCS_Ref][value]
        ${Pay_Flag}  Set Variable  ${batchDetailData}[Pay_Flag][value]
        ${Product_Type}  Set Variable  ${batchDetailData}[Product_Type][value]
        ${Sender_Proxy_Type}  Set Variable  ${batchDetailData}[Sender_Proxy_Type][value]
        ${Sender_Proxy_ID}  Set Variable  ${batchDetailData}[Sender_ProxyID][value]
        ${Receiver_Proxy_Type}  Set Variable  ${batchDetailData}[Receiver_ProxyType][value]
        ${Receiver_Proxy_ID}  Set Variable  ${batchDetailData}[Receiver_ProxyID][value]
        ${Raw_Payment_Amount}  Set Variable  ${batchDetailData}[Amount][value]
        ${Payment_Amount}  Replace String  ${Raw_Payment_Amount}  '  ${EMPTY}  count=-1
        ${Entry_Date}  Set Variable  ${batchDetailData}[EntryDate][value]
        ${Ref1}  Set Variable  ${batchDetailData}[Reference1][value]
        ${Ref2}  Set Variable  ${batchDetailData}[Reference2][value]
        ${Ref3}  Set Variable  ${batchDetailData}[Reference3][value]
        ${PayResult}  Set Variable  ${batchDetailData}[Pay_Result][value]

        IF  '${RunFlag}' == 'Y' and '${PayFlag}' == 'Y' and '${PayResult}' == 'Pass'
            ${foundDataStatus}  Set Variable  ${False}
            ${reloadCount}  Set Variable  ${50}
            FOR  ${reloadIterator}  IN RANGE  0  ${reloadCount}
                ${rowCount}  Get Element Count  //*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')]
                FOR  ${i}  IN RANGE  0  ${rowCount}
                    ${rowIndex}  Evaluate  ${i}+${1}
                    ${batchRefNo}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Batch Reference No]]
                    ${myProductScreen}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[My Product]]
                    ${entryDateScreen}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Entry Date]]
                    ${totalAmount}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Total Amount]]
                    ${status}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Status]]

                    ${reconcileStatus}  Set Variable  ${True}
                    IF  '${Payment_Amount}' == '${totalAmount}' and '${myProduct}' == '${myProductScreen.strip()}'

                        IF  '${Entry_Date}' != ''
                            IF  '${Entry_Date}' == '${entryDateScreen}'
                                ${foundDataStatus}  Set Variable  ${True}
                            ELSE
                                ${foundDataStatus}  Set Variable  ${False}
                            END
                        ELSE
                            ${foundDataStatus}  Set Variable  ${True}
                        END

                        IF  ${foundDataStatus} == ${True}
                            C - Click Element  (//*[@id="gridTable"]/tbody/tr/td[3]/a[@title="View Deposit"])[${rowIndex}]
                            C - Capture Screenshot  Batch_Detail_Screen
                            Sleep  2s
                            C - Click Element  //*[@id="divPayInst"]//table/tbody/tr/td[3]/a[@title="View Instrument"]
                            # Get Data From Batch Screen
                            ${paymentProductScreen}  C - Get Text  ${RTP_Detail_Payment_Product}
                            ${instructionReferenceNoScreen}  C - Get Text  ${RTP_Detail_Instruction_Reference_No}
                            ${debitAccount}  C - Get Text  ${RTP_Detail_Debit_Account}
                            ${paymentAmount}  C - Get Text  ${RTP_Detail_Payment_Amount}
                            ${taxIdScreen}  C - Get Text  ${RTP_Detail_TAXID}
                            ${RTPReferenceID}  C - Get Text  ${RTP_Detail_RTP_Reference_ID}
                            ${debitDate}  C - Get Text  ${RTP_Detail_Debit_Date}
                            ${senderProxyID}  C - Get Text  ${RTP_Detail_Sender_Proxy_ID}
                            ${receivingProxyID}  C - Get Text  ${RTP_Detail_Receiving_Proxy_ID}
                            ${reference1}  C - Get Text  ${RTP_Detail_Reference1}
                            ${reference2}  C - Get Text  ${RTP_Detail_Reference2}
                            ${reference3}  C - Get Text  ${RTP_Detail_Reference3}

                            # Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Debit_Account]  ${debitAccount.strip()}
                            # Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Payment_Date]  ${debitDate.strip()}
                            # Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Instrument_RefNo]  ${instructionReferenceNoScreen.strip()}

                            C - Capture Screenshot  Transaction_Detail_Screen
                            C - Click Element  ${CMS_Ins_Back_Button}
                            F_CMS_RTP_Wait until Loader Is Not Visible

                            # Check Auto Aprrove Config
                            IF  '${productConfig}[AUTO_SUBMIT]' == 'N'
                                ${expectedStatusForSubmit}  Set Variable  For Submit
                                IF  '${status.strip()}' == '${expectedStatusForSubmit}'
                                    C - Click Element  ${CMS_Submit_Btn}  # Click Submit
                                    C - Click Element  ${CMS_Confirm_Auth_Btn}  # Confirm Submit
                                    ${submitMessage}  C - Get Text  ${CMS_Auth_Message}
                                    ${submitStatus}  Run Keyword And Return Status  Should Contain  ${submitMessage}  ${totalAmount}
                                    IF  ${submitStatus} == ${True}
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_Result]  Pass
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_ResultMessage]  Submit Transaction Passed
                                    ELSE
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_Result]  Fail
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_ResultMessage]  Send to Bank Failed - ${submitMessage}
                                    END
                                ELSE
                                    Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_Result]  Fail
                                    Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_ResultMessage]  Auto Approve is '${productConfig}[AUTO_APPROVE]', But Status != ${expectedStatusForSubmit}
                                END
                            ELSE
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_Result]  Skip
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_ResultMessage]  Product Config AUTO_APPROVE is 'Y'
                            END
                            C - Click Element  ${CMS_Batch_Back_Button}
                            # Check Last Status is Send To Bank 
                            
                            Mouse Over  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Status]]
                            C - Capture Screenshot  Transaction_Last_Status
                            ${lastStatus}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Status]]
                            IF  '${lastStatus}' != 'For Submit'
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Overall_Result]  Failed: Expect Last Status is 'Sent to Bank' - Actual status is ${lastStatus}
                                ${reconcileStatus}  Set Variable  ${False}
                            ELSE
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Overall_Result]  Pass: Status as is Expect
                            END
                            Exit For Loop
                        END
                    END
                END

                IF  ${foundDataStatus} == ${True}
                    Exit For Loop  
                ELSE
                    IF  ${reloadIterator} == ${reloadCount}-1
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_Result]  Fail
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_ResultMessage]  Not Found Transaction on Screen for Submit
                    ELSE
                        Reload Page  
                        Sleep  2s
                    END
                END
            END
        ELSE
            Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Submit_Result]  Run flag not equal 'Y' or Pay_Result not equal 'Pass' >> Skip this transaction
        END
    END
    F_CASH_Logout CMS

    # Approve and Send 
    Pass Execution If  ${rtpSuccessCount} == ${0}  Pass Execution, Because Pay Result 'Success' Count is 0 
    F_CASH_Open_Cashlink_Website
    F_CASH_Login Cashlink Checker
    Go to  ${cmsPaymentSummaryUrl}

    ${batchDetailInputDataArray}  Read Excel Sheet  ${batchDetailInputWorksheet}  cellDetail=${True}
    ${paymentSummaryHeaderIndex}  F_CASH_Find_Payments_Summary_Table_Header_Index
    FOR  ${batchDetailData}  IN  @{batchDetailInputDataArray}
        
        ${RunFlag}  Set Variable  ${batchDetailData}[Run_Flag][value]
        ${Pay_Flag}  Set Variable  ${batchDetailData}[Pay_Flag][value]
        ${myProduct}  Set Variable  ${batchDetailData}[Product_Type][value]
        ${payResult}  Set Variable  ${batchDetailData}[Pay_Result][value]
        
        IF  '${myProduct}' == 'Pay Alert'
            ${myProduct}  Set Variable  RTPPAYCT
        ELSE
            ${myProduct}  Set Variable  RTPPAYBP
        END

        # ${productConfig}  Filter Json Array  ${productConfigData}   PRODUCT  ${myProduct}  
        # ${productConfig}  Filter Json Array  ${productConfig}       CLIENT   ${makerData}[CLIENT] 
        # ${productConfig}  Set Variable  ${productConfig}[0]

        ${RunFlag}  Set Variable  ${batchDetailData}[Run_Flag][value]
        ${testCaseRef}  Set Variable  ${batchDetailData}[TCS_Ref][value]
        ${Pay_Flag}  Set Variable  ${batchDetailData}[Pay_Flag][value]
        ${Product_Type}  Set Variable  ${batchDetailData}[Product_Type][value]
        ${Sender_Proxy_Type}  Set Variable  ${batchDetailData}[Sender_Proxy_Type][value]
        ${Sender_Proxy_ID}  Set Variable  ${batchDetailData}[Sender_ProxyID][value]
        ${Receiver_Proxy_Type}  Set Variable  ${batchDetailData}[Receiver_ProxyType][value]
        ${Receiver_Proxy_ID}  Set Variable  ${batchDetailData}[Receiver_ProxyID][value]
        ${Raw_Payment_Amount}  Set Variable  ${batchDetailData}[Amount][value]
        ${Payment_Amount}  Replace String  ${Raw_Payment_Amount}  '  ${EMPTY}  count=-1
        ${Entry_Date}  Set Variable  ${batchDetailData}[EntryDate][value]
        ${Ref1}  Set Variable  ${batchDetailData}[Reference1][value]
        ${Ref2}  Set Variable  ${batchDetailData}[Reference2][value]
        ${Ref3}  Set Variable  ${batchDetailData}[Reference3][value]
        ${PayResult}  Set Variable  ${batchDetailData}[Pay_Result][value]

        IF  '${RunFlag}' == 'Y' and '${PayFlag}' == 'Y' and '${PayResult}' == 'Pass'
            ${foundDataStatus}  Set Variable  ${False}
            ${reloadCount}  Set Variable  ${50}
            FOR  ${reloadIterator}  IN RANGE  0  ${reloadCount}
                ${rowCount}  Get Element Count  //*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')]
                FOR  ${i}  IN RANGE  0  ${rowCount}
                    ${rowIndex}  Evaluate  ${i}+${1}
                    ${batchRefNo}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Batch Reference No]]
                    ${myProductScreen}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[My Product]]
                    ${entryDateScreen}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Entry Date]]
                    ${totalAmount}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Total Amount]]
                    ${status}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Status]]

                    ${reconcileStatus}  Set Variable  ${True}
                    IF  '${Payment_Amount}' == '${totalAmount}' and '${myProduct}' == '${myProductScreen.strip()}'

                        IF  '${Entry_Date}' != ''
                            IF  '${Entry_Date}' == '${entryDateScreen}'
                                ${foundDataStatus}  Set Variable  ${True}
                            ELSE
                                 ${foundDataStatus}  Set Variable  ${False}
                            END
                        ELSE
                            ${foundDataStatus}  Set Variable  ${True}
                        END

                        IF  ${foundDataStatus} == ${True}
                            C - Click Element  (//*[@id="gridTable"]/tbody/tr/td[3]/a[@title="View Deposit"])[${rowIndex}]
                            C - Capture Screenshot  Batch_Detail_Screen
                            Sleep  2s
                            C - Click Element  //*[@id="divPayInst"]//table/tbody/tr/td[3]/a[@title="View Instrument"]
                            # Get Data From Batch Screen
                            ${paymentProductScreen}  C - Get Text  ${RTP_Detail_Payment_Product}
                            ${instructionReferenceNoScreen}  C - Get Text  ${RTP_Detail_Instruction_Reference_No}
                            ${debitAccount}  C - Get Text  ${RTP_Detail_Debit_Account}
                            ${paymentAmount}  C - Get Text  ${RTP_Detail_Payment_Amount}
                            ${taxIdScreen}  C - Get Text  ${RTP_Detail_TAXID}
                            ${RTPReferenceID}  C - Get Text  ${RTP_Detail_RTP_Reference_ID}
                            ${debitDate}  C - Get Text  ${RTP_Detail_Debit_Date}
                            ${senderProxyID}  C - Get Text  ${RTP_Detail_Sender_Proxy_ID}
                            ${receivingProxyID}  C - Get Text  ${RTP_Detail_Receiving_Proxy_ID}
                            ${reference1}  C - Get Text  ${RTP_Detail_Reference1}
                            ${reference2}  C - Get Text  ${RTP_Detail_Reference2}
                            ${reference3}  C - Get Text  ${RTP_Detail_Reference3}

                            Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Debit_Account]  ${debitAccount.strip()}
                            Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Payment_Date]  ${debitDate.strip()}
                            Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Instrument_RefNo]  ${instructionReferenceNoScreen.strip()}

                            C - Capture Screenshot  Transaction_Detail_Screen
                            C - Click Element  ${CMS_Ins_Back_Button}
                            F_CMS_RTP_Wait until Loader Is Not Visible

                            # Check Auto Aprrove Config
                            IF  '${productConfig}[AUTO_APPROVE]' == 'N'
                                ${expectedStatusForMyAuth}  Set Variable  For My Auth
                                ${expectedStatusForAuth}  Set Variable  For Auth
                                IF  '${status.strip()}' == '${expectedStatusForMyAuth}' or '${status.strip()}' == '${expectedStatusForAuth}'
                                    C - Click Element  ${CMS_Auth_Btn}  # Click Auth
                                    C - Click Element  ${CMS_Confirm_Auth_Btn}  # Confirm Auth
                                    ${authMessage}  C - Get Text  ${CMS_Auth_Message}
                                    ${authStatus}  Run Keyword And Return Status  Should Contain  ${authMessage}  ${totalAmount}
                                    IF  ${authStatus} == ${True}
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Auth_Result]  Pass
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Auth_ResultMessage]  Auth Transaction Passed
                                    ELSE
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Auth_Result]  Fail
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Auth_ResultMessage]  Send to Bank Failed - ${authMessage}
                                    END
                                ELSE
                                    Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Auth_Result]  Fail
                                    Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Auth_ResultMessage]  Auto Approve is '${productConfig}[AUTO_APPROVE]', But Status != ${expectedStatusForMyAuth}
                                END
                            ELSE
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Auth_Result]  Skip
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Auth_ResultMessage]  Product Config AUTO_APPROVE is 'Y'
                            END

                            # Check Auto Send Config
                            IF  '${productConfig}[AUTO_SEND]' == 'N'
                                ${status}  C - Get Text  //*[@id="gridTable"]//tbody/tr/td[13]
                                ${expectedStatus}  Set Variable  For Send
                                IF  '${status}' == '${expectedStatus}'
                                    C - Click Element  ${CMS_BatchSummary_Send}  # Click Send to Bank
                                    C - Click Element  ${CMS_Confirm_Auth_Btn}  # Confirm Send to Bank
                                    ${authMessage}  C - Get Text  ${CMS_Auth_Message}
                                    ${authStatus}  Run Keyword And Return Status  Should Contain  ${authMessage}  ${totalAmount}
                                    IF  ${authStatus} == ${True}
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[SentToBank_Result]  Pass
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[SentToBank_ResultMessage]  Send to Bank Passed
                                    ELSE
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[SentToBank_Result]  Fail
                                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[SentToBank_ResultMessage]  Send to Bank Failed - ${authMessage}
                                    END
                                ELSE
                                    Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[SentToBank_Result]  Fail
                                    Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[SentToBank_ResultMessage]  Expect Status is '${expectedStatus}', Actual Status is ${status}
                                END
                            ELSE
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[SentToBank_Result]  Skip
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[SentToBank_ResultMessage]  Product Config AUTO_SEND is 'Y'
                            END
                            C - Click Element  ${CMS_Batch_Back_Button}

                            # Check Last Status is Send To Bank 
                            
                            Mouse Over  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Status]]
                            C - Capture Screenshot  Transaction_Last_Status
                            ${lastStatus}  C - Get Text  (//*[@id="content"]//*[@id="gridTable"]/tbody/tr[contains(@ondblclick, 'javascript:showViewPir')])[${rowIndex}]/td[${paymentSummaryHeaderIndex}[Status]]
                            IF  '${lastStatus}' != 'Sent to Bank'
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Overall_Result]  Failed: Expect Last Status is 'Sent to Bank' - Actual status is ${lastStatus}
                                ${reconcileStatus}  Set Variable  ${False}
                            ELSE
                                Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Overall_Result]  Pass: Status as is Expect
                            END
                            Exit For Loop
                        END
                    END
                END

                IF  ${foundDataStatus} == ${True}
                    Exit For Loop  
                ELSE
                    IF  ${reloadIterator} == ${reloadCount}-1
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Auth_Result]  Fail
                        Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Auth_ResultMessage]  Not Found Transaction on Screen for Auth
                    ELSE
                        Reload Page  
                        Sleep  2s
                    END
                END
            END
        ELSE
            Write Cell By Cell Detail  ${batchDetailInputWorksheet}  ${batchDetailData}[Auth_Result]  Run flag not equal 'Y' or Pay_Result not equal 'Pass' >> Skip this transaction
        END
    END
