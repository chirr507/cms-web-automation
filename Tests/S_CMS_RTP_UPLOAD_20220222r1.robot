***Settings****
Library  ExcelLibrary
Resource    ${EXECDIR}\\Resources\\__init_resources__.robot
Suite Setup    C - Suite Setup
Test Teardown  F_RTP_CMS Teardown
# '*************************************************************************************************
#* Date                * Changed By     * Description                           * version
#---------------------------------------------------------------------------------------------------
#* 08/03/2022            Jenjira H.       Initial version                         1.0
#*                                        /Upload RTPSEND transactions
#****************************************************************************************************
***Test Cases***
Test_UPLOAD_001
    [Documentation]    The script use in RTPSEN product for generate file and upload transaction with client authorize matrix set up
    [Teardown]         F_RTP_Logout_2Window
    #robot Tests/S_CMS_RTP_UPLOAD_20220222r1.robot  
    F_CASH_Create Batch Text File
    Set Suite Variable  ${transactionLog}  ${EMPTY}        # Define Empty Variable for Write Log

    # Set transaction iterator
    ${fileIterator}  Set Variable  ${1}  
    FOR  ${file}  IN  @{fileList}  # Loop File

        ${fileIteratorRow}  Evaluate  ${fileIterator} + 1  # Set File Iterator
        ${fileName}  Get File Name From Path  ${file}  # Get File Name from Path
        F_CASH_Generate Log Transaction  ********** File: ${fileName} **********
        ${fileFormatName}  Get Substring  ${fileName}  0  4
        ${rawDataByFormat}  F_CASH_Filter File Format    ${fileFormatName}
         
        #---------------------------------------------
     
        Log To Console  เริ่มเปิดเว็บไซต์ Cashlink
        F_CASH_Open_Cashlink_Website
        ${CMSPage}  Get Window Titles  
        F_CASH_Login Cashlink Maker
        F_CASH_Click Navbar Service Menu   # Click Service Menu
        F_CASH_Click link BayIneternal     # Click link to BAY Internal
        Sleep  3s 

        #---- Switch Browser from CMS to RTP ------#
        Switch Window  NEW  
        Maximize Browser Window  #ขยายจอRTP       
        F_CASH_Click Sidebar RTP_Instruction Menu
        C - Click Element  ${RTP_Siderbar_Upload}
        Sleep  3s  
        Wait Until Element Is Not Visible    //div[@class="loader"]  timeout=10 
        Select Frame  //*[@id="content"]//iframe

        ${fileUploadStats}  Set Variable  ${False}   #  Set default upload status to false
        
        IF  '${cmsData}[UploadFlag][value]' == 'Y'
            # ------------------------------------------------------------------------------
            # Upload File Section (Maker)
            # ------------------------------------------------------------------------------
            ${reconcileParameterStatus}  Set Variable  ${True}
            ${foundFileStatus}  Set Variable  ${False}
            F_CASH_Generate Log Transaction  Maker Upload ===================
            Log To Console   Uploading ..... ${proxyIdData}
            F_RTP_Upload Text File  ${file}   ${proxyIdData}    # Step for Upload Text File to Cashlink       
            #Select Frame    //*[@id="content"]//iframe      #//iframe[@cd_frame_id_="67668d1d792f696c6400cbed80fcc468"]
            Wait Until Element Is Not Visible    //div[@class="loader"]  timeout=40 
            Wait Until Element Is Visible    ${RTP_PopUp_Message} 
            Sleep  3s
            C - Capture Full Page Screen by Headless Chrome  ${RTP_PageBody}  Upload_File_${proxyIdData}
            ${messagePopup}    C - Get Text  ${RTP_PopUp_Message}
            C - Click Element  ${RTP_PopUp_Message_Close_Button}
            Sleep  3s
            C - Capture Full Page Screen by Headless Chrome  ${RTP_PageBody}  Upload_File_${messagePopup}
            ${messageStatus}  Run Keyword And Return Status  Should Contain  ${messagePopup}  SUCCESS  #check message diplay SUCESS is true ?
            #----------- Get Message Pop-Up ---------------
            IF  ${messageStatus} == ${False}
                ${reconcileBatchStatus}  Set Variable  ${False}
                Log To Console  Message display : ${messagePopup}
                ${fileUploadStats}  Set Variable  ${False}
                F_CASH_Generate Log Transaction  Upload File: ${fileName} - Incomplete, Status is ${messagePopup}
                Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[UploadResult]  Failed!!
                #F_RTP_Logout_2Window
                Exit For Loop
            ELSE
                Log To Console  Message display : ${messagePopup}
                F_CASH_Generate Log Transaction  Upload Success!!
                ${fileNameScreen}  C - Get Text   (//table/tbody/tr/td[3])[${1}]  # get file name on screen 
            END
             
            IF  ${messageStatus} == ${True} and '${fileName}' == '${fileNameScreen}'  #RTP use original file name
                ${foundFileStatus}  Set Variable  ${True}

                # Get Data
                ${statusScreen}  C - Get Text   (//table/tbody/tr/td[10])[${1}]
                ${uploadCountScreen}  C - Get Text  (//table/tbody/tr/td[5])[${1}]
                ${totalAmountScreen}  C - Get Text  (//table/tbody/tr/td[7])[${1}]
                Write Cell  ${fileWorksheet}  ${fileIteratorRow}  4  ${statusScreen}
                Write Cell  ${fileWorksheet}  ${fileIteratorRow}  1  ${file}
                Write Cell  ${fileWorksheet}  ${fileIteratorRow}  2  ${uploadCountScreen}
                Write Cell  ${fileWorksheet}  ${fileIteratorRow}  3  ${totalAmountScreen}
                
                IF  '${statusScreen}' == 'New' or '${statusScreen}' == 'Authorized'
                    ${amountConverted}  Convert String To Number Format  ${totalAmountScreen}
                    ${rawDataLen}  Get Length  ${rawDataByFormat}
                    ${rawTotalAmount}  Sum Raw Data Amount  ${rawDataByFormat}
                    ${uniqueBatch}  Get Unique From Json Array  ${rawDataByFormat}  Batch_Reference_No
                    #  Reconcile Uploaded Data
                    IF  '${rawDataLen}' == '${uploadCountScreen}'
                        F_CASH_Generate Log Transaction  Transaction Count Pass - Expect: ${rawDataLen} == Actual: ${uploadCountScreen}
                    ELSE
                        F_CASH_Generate Log Transaction  Transaction Count Fail - Expect: ${rawDataLen} != Actual: ${uploadCountScreen}
                        ${reconcileParameterStatus}  Set Variable  ${False}
                    END
                    IF  '${rawTotalAmount}' == '${amountConverted}'
                        F_CASH_Generate Log Transaction  Total Amount Pass - Expect: ${rawTotalAmount} == Actual: ${amountConverted}
                    ELSE
                        F_CASH_Generate Log Transaction  Total Amount Fail - Expect: ${rawTotalAmount} != Actual: ${amountConverted}
                        ${reconcileParameterStatus}  Set Variable  ${False}
                    END
                    # Set File upload status
                    IF  ${reconcileParameterStatus} == ${True}
                        ${fileUploadStats}  Set Variable  ${True}
                    ELSE
                        ${fileUploadStats}  Set Variable  ${False}
                    END


                    # Exit For loop when found batch file and status change to complete
                    #Exit For Loop
                ELSE
                    IF  '${statusScreen}' != 'New' or '${statusScreen}' == 'Abort'
                        F_CASH_Generate Log Transaction  Upload File: ${fileName} - Incomplete, Status is ${statusScreen}
                        ${fileUploadStats}  Set Variable  ${False}

                        Exit For Loop
                    
                    END
                END
               
            END
            F_CASH_Generate Log Transaction    Upload File Successs!!
            Log To Console  Uploaded and Recoclie data Passed
        END  #End loop file upload

        #----- Slide Menu RTP_Sending -----------------
        Unselect Frame  
        C - Click Element  ${RTP_Siderbar_Sending}
        #----------------------------------------------
        Select Frame  //*[@id="content"]//iframe
        Wait Until Element Is Not Visible    //div[@class="loader"]  timeout=10 
        #C - Select From List By Value  ${RTP_Dropdow_DayFilter}  today        
        Log To Console  Inquiry transaction on RTP Sending Screen
        ${batchIterator}  Set Variable      ${1}
        ${insIteratorRow}  Set Variable     ${1}
        ${uniqueBatch}  Get Unique From Json Array  ${rawDataByFormat}  Batch_Reference_No
        ${uniqueProduct}    Get Unique From Json Array   ${rawDataByFormat}  MyProduct
        Wait Until Element Is Visible  ${RTP_Table_Sending}
        ${countBatchTable}  Get Element Count  ${RTP_Table_Sending}  # Count table of batch screen

        FOR  ${batchName}  IN  @{uniqueBatch}
            ${reconcileBatchStatus}  Set Variable  ${True}  # Set default status as True (Success)
            ${batchIteratorRow}  Evaluate  ${batchIterator} + 1  # Set row for write data to excel
            ${batchArray}    Filter Json Array    ${rawDataByFormat}    Batch_Reference_No    ${batchName}  # Filter Json data by Batch Array
            ${batchSummaryTrans}    Get Length    ${batchArray}  # Get Transaction count of batch
            ${batchSummaryAmt}    Sum Raw Data Amount    ${batchArray}  # Summary amount of batch
            

            FOR  ${i}  IN RANGE  1  ${countBatchTable}
                ${batch}    C - Get Text    (//table/tbody/tr/td[6])[${i}]
                Log To Console   ${batch}
                
                IF  '${batchName}' == '${batch}'  
                    #-------- get text on screen for write output-------------------
                    ${client}    C - Get Text    (//table/tbody/tr/td[4])[${i}]
                    ${senderproxyid}    C - Get Text    (//table/tbody/tr/td[5])[${i}]
                    ${batchRef}    C - Get Text    (//table/tbody/tr/td[6])[${i}]
                    ${product}    C - Get Text    (//table/tbody/tr/td[7])[${i}]
                    ${entryDate}    C - Get Text    (//table/tbody/tr/td[8])[${i}]
                    ${totalInst}    C - Get Text    (//table/tbody/tr/td[9])[${i}]
                    ${totalAmt}    C - Get Text    (//table/tbody/tr/td[11])[${i}]
                    ${batchStatusScreen}    C - Get Text    (//table/tbody/tr/td[12])[${i}]
                                            
                    Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  1  ${file}
                    Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  2  ${client}
                    Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  3  ${senderproxyid}
                    Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  4  ${batchRef}
                    Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  5  ${product}
                    Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  6  ${entryDate}
                    Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  7  ${totalInst}
                    Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  8  ${totalAmt}
                    Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  9  ${batchStatusScreen}
                    
                    #---- trim product ------
                    #${productStr}  Set Variable  ${product.strip()} 

                    #----------Reconclie Data---------------------------------------
                    IF  '${batchStatusScreen}' == 'New' or '${statusScreen}' == 'Authorized'
                        # -----------------------------
                        IF  '${proxyIdData}' == '${senderproxyid}'
                            F_CASH_Generate Log Transaction    Proxy ID Pass - Expect: ${proxyIdData} == Actual: ${senderproxyid}
                        ELSE
                            F_CASH_Generate Log Transaction    Proxy ID Fail - Expect: ${proxyIdData} != Actual: ${senderproxyid}
                            ${reconcileBatchStatus}  Set Variable  ${False}
                        END
                        # -----------------------------
                        IF  '${batchSummaryTrans}' == '${totalInst}'
                            F_CASH_Generate Log Transaction    Batch Transaction Count Pass - Expect: ${batchSummaryTrans} == Actual: ${totalInst}
                        ELSE
                            F_CASH_Generate Log Transaction    Batch Transaction Count Fail - Expect: ${batchSummaryTrans} != Actual: ${totalInst}
                            ${reconcileBatchStatus}  Set Variable  ${False}
                        END
                        # -----------------------------
                        IF  '${batchSummaryAmt}' == '${totalAmt}'
                            F_CASH_Generate Log Transaction    Batch Amount Pass - Expect: ${batchSummaryAmt} == Actual: ${totalAmt}
                        ELSE
                            F_CASH_Generate Log Transaction    Batch Amount Fail - Expect: ${batchSummaryAmt} != Actual: ${totalAmt}
                            ${reconcileBatchStatus}  Set Variable  ${False}
                        END
                        
                    ELSE 
                        F_CASH_Generate Log Transaction    Status != New - Actual Status: ${batchStatusScreen}
                        ${reconcileBatchStatus}  Set Variable  ${False}
                        Exit For Loop
                    END
                    #------------ Maker Submit ---------------------
                    IF  '${autoSubmitData}' == 'N' and '${batchSummaryAmt}' == '${totalAmt}'
                        Log To Console  Submit Batch ref : ${batch}
                        C - Select Checkbox  (//input[@ng-click="addItem(item);getDecision();"])[${i}]
                        Wait Until Element Is Not Visible    //div[@class="loader"]  timeout=40 
                        Wait Until Element Is Visible  ${RTP_Submit_Button}  timeout=10s
                        C - Capture Full Page Screen by Headless Chrome  ${RTP_PageBody}  Sending_${batch}  # Capture screen
                        C - Click Element  ${RTP_Submit_Button}

                        #---- Click Close PopUp---------------
                        Wait Until Element Is Visible    ${RTP_PopUp_Message} 
                        ${messagePopup}    C - Get Text  ${RTP_PopUp_Message}
                        C - Click Element  ${RTP_PopUp_Message_Close_Button}
                        ${successStatus}  Run Keyword And Return Status  Should Contain  ${messagePopup}  SUCCESS
                        #----------- Get Message Pop-Up ---------------
                        IF  ${successStatus} == ${True}

                            Log To Console  Message display Submit : ${messagePopup}
                            F_CASH_Generate Log Transaction  Submitted Success!!
                        ELSE
                            
                            ${reconcileBatchStatus}  Set Variable  ${False}
                            Log To Console  Message display Submit : ${messagePopup}
                            F_CASH_Generate Log Transaction  Submitted Unsuccess!!
                            
                        END
                    ELSE 
                        Log To Console  Skip submit Batch status : ${batchStatusScreen}  
                        F_CASH_Generate Log Transaction  Skip Submit Process because Auto Submit Flag: ${autoSubmitData} | Batch Status : ${batchStatusScreen}
                        # Exit For Loop  
                    END
                    #-----------------------------------------------
                    #  View Instrument detail
                    #-----------------------------------------------
                    Wait Until Element Is Not Visible    //div[@class="loader"]  timeout=40 
                    C - Click Element  (//table/tbody/tr/td[3]/a)[${i}]
                    ${insIterator}  Set Variable  ${1}
                    FOR  ${instruction}  IN  @{batchArray}

                        ${insReconcileStatus}  Set Variable  ${True}
                        ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1

                        ${instRefScreen}    C - Get Text    (//table/tbody/tr[@ng-repeat="item in rtplist | orderBy:'id'"]/td[3])[${insIterator}]
                        ${instReceiverTypeScreen}    C - Get Text    (//table/tbody/tr[@ng-repeat="item in rtplist | orderBy:'id'"]/td[4])[${insIterator}]
                        ${instReceiverProxyScreen}    C - Get Text    (//table/tbody/tr[@ng-repeat="item in rtplist | orderBy:'id'"]//td[5])[${insIterator}]
                        ${instBillRef1Screen}    C - Get Text    (//table/tbody/tr[@ng-repeat="item in rtplist | orderBy:'id'"]/td[6])[${insIterator}]
                        ${instBillRef2Screen}    C - Get Text    (//table/tbody/tr[@ng-repeat="item in rtplist | orderBy:'id'"]/td[7])[${insIterator}]
                        ${instDueDateScreen}    C - Get Text    (//table/tbody/tr[@ng-repeat="item in rtplist | orderBy:'id'"]/td[9])[${insIterator}]
                        ${instAmountScreen}    C - Get Text    (//table/tbody/tr[@ng-repeat="item in rtplist | orderBy:'id'"]/td[10])[${insIterator}]
                        ${instStatusScreen}    C - Get Text    (//table/tbody/tr[@ng-repeat="item in rtplist | orderBy:'id'"]/td[14])[${insIterator}]

                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  1  ${client}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  2  ${senderproxyid}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  3  ${batchRef}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  4  ${product}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  5  ${entryDate}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  6  ${instRefScreen}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  7  ${instReceiverTypeScreen}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  8  ${instReceiverProxyScreen}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  9  ${instBillRef1Screen}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  10  ${instBillRef2Screen}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  11  ${instDueDateScreen}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  12  ${instAmountScreen}
                        Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  13  ${instStatusScreen}
                        
                        C - Capture Full Page Screen by Headless Chrome   ${RTP_PageBody}  Instrument_${instRefScreen}
                        IF  '${instStatusScreen}' == 'Authorized'
                            Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  17  Skip process || Auto Submit :${autoSubmitData} / Auto Auth :${autoAuthData}
                            Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  16  Passed!!
                        END

                        ${insIterator}  Evaluate  ${insIterator} + 1
                    END
                    Exit For Loop  
                END
   
            END
            IF  '${reconcileBatchStatus}' == '${True}'
                
                Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  9  ${instStatusScreen}
                Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  10  Passed!!
                Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  11  ${messagePopup}
                
                 
            ELSE
                Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  9  ${instStatusScreen}    
                Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  10  Failed!!
                Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  11  ${messagePopup}
                Continue For Loop 
            END
            C - Click Element  ${RTP_Back_Button}

        END
        
        F_RTP_Logout_2Window

        #----------------------------------------------------------------------------------------------------
        #  Checker Authorize Txn
        #----------------------------------------------------------------------------------------------------
       
        IF  '${cmsData}[AuthFlag][value]' == 'Y' and '${autoAuthData}' == 'N'
            Log To Console  เริ่มเปิดเว็บไซต์ Cashlink
            F_CASH_Open_Cashlink_Website
            F_CASH_Login Cashlink Checker
            F_CASH_Click Navbar Service Menu   # Click Service Menu
            F_CASH_Click link BayIneternal     # Click link to BAY Internal
            Sleep  3s
            #---- Switch Browser from CMS to RTP ------#
            Switch Window  NEW  
            Maximize Browser Window  #ขยายจอRTP       
            F_CASH_Click Sidebar RTP_Instruction Menu
            #----- Slide Menu RTP_Sending -----------------
            # Unselect Frame  
            C - Click Element  ${RTP_Siderbar_SendingAuth}
            #----------------------------------------------
            Select Frame  //*[@id="content"]//iframe
            Wait Until Element Is Not Visible    //div[@class="loader"]  timeout=10 
            
            ${sessionTimeout}  Run Keyword And Return Status   Wait Until Element Is Visible  ${RTP_SessionTimeout}
            IF  ${sessionTimeout} == ${True} 
                ${sessionText}  C - Get Text  ${RTP_SessionTimeout}
                Log To Console  ${sessionText}
                F_RTP_Logout_2Window
                F_CASH_Login Cashlink Checker
                F_CASH_Click Navbar Service Menu   # Click Service Menu
                F_CASH_Click link BayIneternal
                #---- Switch Browser from CMS to RTP ------#
                Switch Window  NEW  
                Maximize Browser Window  #ขยายจอRTP       
                F_CASH_Click Sidebar RTP_Instruction Menu
                #----- Slide Menu RTP_Sending -----------------
                Unselect Frame  
                C - Click Element  ${RTP_Siderbar_SendingAuth}
                #----------------------------------------------
            END 
            #Select Frame  //*[@id="content"]//iframe
            Wait Until Element Is Not Visible    //div[@class="loader"]  timeout=10 
            F_CASH_Generate Log Transaction    Checker ===================
            Log To Console  Checker Authorize Sending transactions
            ${batchIterator}  Set Variable      ${1}
            ${insIteratorRow}  Set Variable     ${1}
            ${uniqueBatch}  Get Unique From Json Array  ${rawDataByFormat}  Batch_Reference_No
            ${uniqueProduct}    Get Unique From Json Array   ${rawDataByFormat}  MyProduct
            Wait Until Element Is Visible  ${RTP_Table_Sending}   timeout=30
            ${countBatchTable}  Get Element Count  ${RTP_Table_Sending}  # Count table of batch screen
            FOR  ${batchName}  IN  @{uniqueBatch}
                
                ${insReconcileStatus}  Set Variable  ${True}  # Set default status as True (Success)
                ${batchIteratorRow}  Evaluate  ${batchIterator} + 1  # Set row for write data to excel
                ${batchArray}    Filter Json Array    ${rawDataByFormat}    Batch_Reference_No    ${batchName}  # Filter Json data by Batch Array
                ${batchSummaryTrans}    Get Length    ${batchArray}  # Get Transaction count of batch
                ${batchSummaryAmt}    Sum Raw Data Amount    ${batchArray}  # Summary amount of batch
                
                FOR  ${i}  IN RANGE  1  ${countBatchTable}
                    ${batch}    C - Get Text    (//table/tbody/tr/td[6])[${i}]
                    
                    Log To Console   ${batch}
                    IF  '${batchName}' == '${batch}'  
                        #-------- get text on screen for write output-------------------
                        ${client}    C - Get Text    (//table/tbody/tr/td[4])[${i}]
                        ${senderproxyid}    C - Get Text    (//table/tbody/tr/td[5])[${i}]
                        ${batchRef}    C - Get Text    (//table/tbody/tr/td[6])[${i}]
                        ${product}    C - Get Text    (//table/tbody/tr/td[7])[${i}]
                        ${entryDate}    C - Get Text    (//table/tbody/tr/td[8])[${i}]
                        ${totalInst}    C - Get Text    (//table/tbody/tr/td[9])[${i}]
                        ${totalAmt}    C - Get Text    (//table/tbody/tr/td[11])[${i}]
                        ${batchStatusScreen}    C - Get Text    (//table/tbody/tr/td[12])[${i}]
                                                
                        #----------Reconclie Data---------------------------------------
                        IF  '${batchStatusScreen}' == 'For Auth' or '${batchStatusScreen}' == 'For My Auth'
                            # -----------------------------
                            IF  '${proxyIdData}' == '${senderproxyid}'
                                F_CASH_Generate Log Transaction    Proxy ID Pass - Expect: ${proxyIdData} == Actual: ${senderproxyid}
                            ELSE
                                F_CASH_Generate Log Transaction    Proxy ID Fail - Expect: ${proxyIdData} != Actual: ${senderproxyid}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                            # -----------------------------
                            IF  '${batchSummaryTrans}' == '${totalInst}'
                                F_CASH_Generate Log Transaction    Batch Transaction Count Pass - Expect: ${batchSummaryTrans} == Actual: ${totalInst}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Transaction Count Fail - Expect: ${batchSummaryTrans} != Actual: ${totalInst}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                            # -----------------------------
                            IF  '${batchSummaryAmt}' == '${totalAmt}'
                                F_CASH_Generate Log Transaction    Batch Amount Pass - Expect: ${batchSummaryAmt} == Actual: ${totalAmt}
                            ELSE
                                F_CASH_Generate Log Transaction    Batch Amount Fail - Expect: ${batchSummaryAmt} != Actual: ${totalAmt}
                                ${reconcileBatchStatus}  Set Variable  ${False}
                            END
                            
                        ELSE
                            F_CASH_Generate Log Transaction    Status != For Auth - Actual Status: ${batchStatusScreen}
                            ${reconcileBatchStatus}  Set Variable  ${False}
                            Exit For Loop
                        END
                        #------------ Checker Authorize ---------------------
                        IF  '${autoAuthData}' == 'N' and '${batchSummaryAmt}' == '${totalAmt}'
                            Log To Console  Auth Batch ref : ${batch}
                            C - Select Checkbox  (//input[@ng-click="addItem(item);getDecision();"])[${i}]
                            Wait Until Element Is Not Visible    //div[@class="loader"]  timeout=40 
                            C - Capture Full Page Screen by Headless Chrome   ${RTP_PageBody}  Authorize_Batch_${batch}
                            Wait Until Element Is Visible  ${RTP_Auth_Button}  timeout=10s
                            C - Click Element  ${RTP_Auth_Button}

                            #---- Click Close PopUp---------------
                            Wait Until Element Is Visible    ${RTP_PopUp_Message} 
                            ${messagePopup}    C - Get Text  ${RTP_PopUp_Message}                           
                            ${successStatus}  Run Keyword And Return Status  Should Contain  ${messagePopup}  SUCCESS  
                            C - Click Element  ${RTP_PopUp_Message_Close_Button}
                            IF  ${successStatus} == ${True}        
                                Log To Console  Message display Authorize : ${messagePopup}
                                F_CASH_Generate Log Transaction  Authorized Success!!
                                

                            ELSE   
                                
                                ${insReconcileStatus}  Set Variable  ${False} 
                                Log To Console  Message display Authorize : ${messagePopup}
                                
                            END
                        ELSE 
                            Log To Console  Batch status : ${batchStatusScreen}
                            F_CASH_Generate Log Transaction  Auto Auth Flag: ${autoAuthData} | Batch Status  : ${batchStatusScreen}
                            Exit For Loop  
                        END
                        #-----------------------------------------------
                        #  View Instrument detail
                        #-----------------------------------------------
                        Wait Until Element Is Not Visible    //div[@class="loader"]  timeout=40 
                        Unselect Frame  
                        C - Click Element  ${RTP_Siderbar_SendingInquiry}  #select menu inquiry
                        Select Frame  //*[@id="content"]//iframe
                        Wait Until Element Is Not Visible    //div[@class="loader"]  timeout=10 
                        Sleep  2s
                        ${batchStatusScreen}    C - Get Text    (//table/tbody/tr/td[11])[${i}]
                        C - Click Element  (//table/tbody/tr/td[2]/a)[${i}]  # view instrument detail
                        Wait Until Element Is Not Visible    //div[@class="loader"]  timeout=10 
                        ${insIterator}  Set Variable  ${1}
                        FOR  ${instruction}  IN  @{batchArray}
                            ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1 
                            ${instRefScreen}    C - Get Text    (//table/tbody/tr[@ng-repeat="item in rtplist | orderBy:'id'"]/td[3])[${insIterator}]                            
                            ${instStatusScreen}    C - Get Text    (//table/tbody/tr[@ng-repeat="item in rtplist | orderBy:'id'"]/td[14])[${insIterator}]
                            C - Capture Full Page Screen by Headless Chrome  ${RTP_PageBody}  SendingInquiry_${instRefScreen}
                            Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  13  ${instStatusScreen}
                            
                            IF  '${insReconcileStatus}' == '${False}'
                                Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  9  ${instStatusScreen} 
                                Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  16  Failed!!
                                Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  17  ${messagePopup}
                                
                                Continue For Loop  
                            ELSE
                                Write Cell  ${RTP_Batchsheet}  ${batchIteratorRow}  9  ${instStatusScreen} 
                                Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  16  Passed!!
                                Write Cell  ${RTP_Instsheet}  ${insIteratorRow}  17  Message display Authorize : ${messagePopup}
                                
                            END
                            ${insIterator}  Evaluate  ${insIterator} + 1
                            
                        END
                        C - Click Element  ${RTP_Back_Button}
                        Exit For Loop 
                    END
                    
                    
                END
            END
            F_RTP_Logout_2Window

        
        END
        
        # Write Result
        IF  '${fileUploadStats}' == '${True}'
            Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[UploadResult]  Passed!!
        ELSE
            Write Cell By Cell Detail  ${mainWorksheet}   ${cmsData}[UploadResult]  Failed!!
        END
        # Set File Iterator
        ${fileIterator}  Evaluate  ${fileIterator} + 1

        

    END
#End Testig#