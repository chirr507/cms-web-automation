*** Settings ***
Resource    ${EXECDIR}\\Resources\\__init_resources__.robot
Suite Setup    C - Initial Setup
Test Teardown  F_CASH_CMS Teardown

# '*************************************************************************************************
#* Date                * Changed By     * Description                           * version
#---------------------------------------------------------------------------------------------------
#* 22/03/2022            Jenjira H.        Initial version                         1.0
#* 23/03/2022            Jenjira H.        Inst & Txn Output  version              2.0
#****************************************************************************************************

***Test Cases***    
TC_CMS_ECHANEL_001
     [Documentation]  key eChanel transaction from simulator and continue process on CMS 
     [Setup]  C - Initial eChanel Setup
     [Teardown]     F_CASH_eChanel Teardown
    #robot Tests/S_CMS_eChanel_Txn_20220322r1.robot
    #/ECustomsTxnSimulator.robot
    
    ${eBatchArrayY}   filter_json_with_cell_detail  ${eChanelDataTxn}   Run_Flag  'Y'
    #${eBatchArray}  get_unique_from_json_array_by_cellDetail  ${eChanelDataTxn}   Run_Flag
    ${eBatchArray}  get_unique_from_json_array_by_cellDetail  ${eBatchArrayY}    referenceNo
    ${eLastRows}  get_last_row  ${echanelWorksheet}
    Log To Console  Rows: ${eLastRows}
    Log To Console  eBatchArray : ${eBatchArray}

    ${batchIteratorRow}  Set Variable   ${1}

    FOR  ${eTxn}  IN  @{eBatchArray}
        ${batchIteratorRow}  Evaluate  ${batchIteratorRow} + 1
        Log To Console  เปิดหน้าเว็บ eChanel Simulator
        C - Open Browser    D:/CMS_Drop5/(newGW)ECustomsNewSimulator.cmsECustom2.test.GCP.html                   # Old link error D:/ECustomsTxnSimulator.html
        Maximize Browser Window 

        ${eDataArray}  filter_json_with_cell_detail  ${eChanelDataTxn}  referenceNo  ${eTxn}  #filter data with REF_No
        #Log To Console  ${eDataArray}
  
        FOR  ${eInst}  IN  @{eDataArray}
            ${eRunFlag}    Set Variable         ${eInst}[Run_Flag][value]
            ${efilename}    Set Variable        ${eInst}[File_name][value]
            ${eRef}         Set Variable        ${eInst}[referenceNo][value]
            ${eDecNo}      Set Variable         ${eInst}[DeclarationNo][value]
            ${eBranch}      Set Variable        ${eInst}[branch][value]
            ${eName}      Set Variable          ${eInst}[Name][value]
            ${ePayerAcc}      Set Variable      ${eInst}[payerAccountNo][value]
            ${ePayerBr}      Set Variable       ${eInst}[payerBankBranch][value]
            ${ePayerTaxNo}      Set Variable    ${eInst}[PayerTaxNo][value]
            ${ePayAmount}      Set Variable     ${eInst}[PayAmount][value]
            ${eAccName}      Set Variable       ${eInst}[AccountName][value]
            #${eTxnDate}       Set Variable      ${eInst}[TransactionDate][value]
            ${eReceivAcc}      Set Variable     ${eInst}[receiverAccountNo][value]
            ${eReceivBr}      Set Variable      ${eInst}[receiverBankBranch][value]
            ${rowData}      Set Variable        ${eInst}[Result][row]
            ${colData}      Set Variable        ${eInst}[Result][column]

            IF  '${eRunFlag}' == 'Y'
                Log To Console  ${efilename}
                C - Input Text    ${fileNameTXT1}    ${efilename}
                C - Input Text    ${referenceNoTXT}    ${eRef}
                C - Input Text    ${DeclarationRefTXT}    ${eRef}
                C - Input Text    ${DeclarationNoTXT}    ${eDecNo}
                C - Input Text    ${branchTXT}    ${eBranch}
                C - Input Text    ${NameTXT}    ${eName}
                C - Input Text    ${payerAccountNoTXT}    ${ePayerAcc}
                C - Input Text    ${payerBankBranchTXT}    ${ePayerBr}
                C - Input Text    ${PayerTaxNoTXT}    ${ePayerTaxNo}
                C - Input Text    ${PayAmountTXT}    ${ePayAmount}
                #C - Input Text    ${TransmitDateTimeTXT}    ${eTxnDate}
                C - Input Text    ${receiverAccountNoTXT}    ${eReceivAcc}
                C - Input Text    ${receiverBankBranchTXT}    ${eReceivBr}
                C - Input Text    ${receiverAccountName}    ${eAccName}
                C - Input Text    ${PayerTaxIDTXT}    ${ePayerTaxNo}
                
                C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${eDecNo}  # Capture screen
                Sleep  1s  
                
                C - Click Element  ${submitBTN}
                
            END    
            ${eErr}  C - Get Text  //body
            Log To Console  ${eErr}
            write_cell  ${echanelWorksheet}   ${rowData}   ${colData}   ${eErr}
           
        END
        
    END
    
    Log To Console  เริ่มเปิดเว็บไซต์ Cashlink
    F_CASH_Open_Cashlink_Website
    # Login Cashlink
    F_CASH_Login Cashlink Maker
    
    Go To  https://uat2.krungsribizonline.com/GCPCW/showPaymentsListBatch.form

    # -- get any data -----
    Log To Console  get data array
    ${eBatchArrayY}   filter_json_with_cell_detail  ${eChanelDataTxn}   Run_Flag  'Y'
    ${uniqueBatch}  get_unique_from_json_array_by_cellDetail  ${eBatchArrayY}   DeclarationNo  #get batch in data
    ${countBatchTable}  Get Element Count  ${CMS_Batch_Table}  # Count table of batch screen
    ${batchIteratorRow}  Set Variable  ${1}
    ${insIteratorRow}  Set Variable  ${1}

    Log To Console  Batch No.: ${uniqueBatch}
    Log To Console  Table rows :${countBatchTable} 
        
    Wait Until Element Is Visible  //*[@id="mbar_Payments"]  timeout=10
    FOR  ${batchName}  IN  @{uniqueBatch}
        ${reconcileBatchStatus}  Set Variable  ${True}  # Set default status as True (Success)
        ${batchIteratorRow}  Evaluate  ${batchIteratorRow} + 1  # Set row for write data to excel
        Log To Console  Batch name == ${batchName}
        
        ${batchArray}  filter_json_with_cell_detail  ${eChanelDataTxn}    DeclarationNo    ${batchName}  # Filter DeclarationNo data by Batch Array
        #Log To Console  data filter by batch = ${batchArray}
               
        FOR  ${i}  IN RANGE  1  ${countBatchTable}
            ${batch}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${i}] 

            IF  '${batchName}' == '${batch}'

                ${totalInst}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[8])[${i}]
                ${product}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[6])[${i}] 
                ${totalAmt}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${i}]
                ${batchStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[11])[${i}]
                Log To Console  Batch Rows : ${batchIteratorRow}
                Write Cell  ${batchWorksheet}  ${batchIteratorRow}  2  ${product}
                Write Cell  ${batchWorksheet}  ${batchIteratorRow}  3  ${batch}
                Write Cell  ${batchWorksheet}  ${batchIteratorRow}  4  ${totalInst}
                Write Cell  ${batchWorksheet}  ${batchIteratorRow}  5  ${totalAmt}
                Write Cell  ${batchWorksheet}  ${batchIteratorRow}  6  ${batchStatusScreen}
                Write Cell  ${batchWorksheet}  ${batchIteratorRow}  7   Passed : found TXN : ${batchName}  # found batch ref on screen mark passed

                C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[3]/a[@title="View Deposit"])[${i}]  #click view instruction 

                ${insIterator}  Set Variable  ${1}
                
                FOR  ${instruction}  IN  @{batchArray}
                    
                    ${insReconcileStatus}  Set Variable  ${True}
                    ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1
                    ${instructionRefScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${insIterator}]
                    ${instructionProductScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[6])[${insIterator}]
                    ${instructionBankBranchScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[9])[${insIterator}]
                    ${instructionBeneficiaryAccountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${insIterator}]
                    ${instructionEffectiveDateScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[11])[${insIterator}]
                    ${instructionAmountScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[12])[${insIterator}]
                    ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[${insIterator}]

                    Write Cell  ${insWorksheet}  ${insIteratorRow}  1  ${instructionProductScreen}
                    Write Cell  ${insWorksheet}  ${insIteratorRow}  2  ${batch}
                    Write Cell  ${insWorksheet}  ${insIteratorRow}  3  ${instructionRefScreen}
                    Write Cell  ${insWorksheet}  ${insIteratorRow}  4  ${instructionBeneficiaryAccountScreen}
                    Write Cell  ${insWorksheet}  ${insIteratorRow}  6  ${instructionAmountScreen}
                    Write Cell  ${insWorksheet}  ${insIteratorRow}  7  ${instructionStatusScreen}

                    Log To Console   Instruction Ref ${InstructionRefScreen} **********************
                    C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_${InstructionRefScreen}  # Capture screen

                    #--------------------------------------------------------------------------
                    #   Reconcile Transaction | เมื่อ Status เท่ากับ For Submit จะตรวจสอบข้อมูลใน Instruction
                    #--------------------------------------------------------------------------
                    ${eProduct}  Set Variable  ${instruction}[bankProduct][value]
                    #${eProduct}  Set Suite Variable  ${eProduct}  ${eProduct}
                    ${eBatchRef}  Set Variable  ${instruction}[DeclarationNo][value]
                    ${eBenefAcc}  Set Variable  ${instruction}[receiverAccountNo][value]
                    ${eAmount}  Set Variable  ${instruction}[PayAmount][value]
                    ${amountConverted}  Convert String To Number Format  ${eAmount}
                    ${productStr}  Set Variable  ${instructionProductScreen.strip()} 
                    Log To Console  Bank Product : ${eProduct}

                    IF  '${instructionStatusScreen}' == 'For Repair'
                        ${insReconcileStatus}  Set Variable  ${False}
                        Log To Console   ${instruction}: Status != For Submit - Actual Status: ${batchStatusScreen}

                    ELSE   # For Submit,For Auth,For Send,Sent to Bank
                        #----------------------------------------------
                        IF  '${eBatchRef}' == '${InstructionRefScreen}'
                            Log To Console    Instruction Ref Pass - Expect: ${eBatchRef} == Actual: ${InstructionRefScreen}
                        ELSE
                            Log To Console    Instruction Ref Fail - Expect: ${eBatchRef} != Actual: ${InstructionRefScreen}
                            ${insReconcileStatus}  Set Variable  ${False}
                        END
                        #----------------------------------------------${str.strip()} 
                        IF  '${eProduct}' == '${productStr}'
                            Log To Console    Instruction Product Pass - Expect: ${eProduct} == Actual: ${productStr}
                        ELSE
                            Log To Console    Instruction Product Fail - Expect: ${eProduct} != Actual: ${productStr}
                            ${insReconcileStatus}  Set Variable  ${False}
                        END
                        #----------------------------------------------
                        IF  '${eBenefAcc}' == '${instructionBeneficiaryAccountScreen}'
                            Log To Console    Instruction Beneficiary Pass - Expect: ${eBenefAcc} == Actual: ${instructionBeneficiaryAccountScreen}
                        ELSE
                            Log To Console    Instruction Beneficiary Fail - Expect: ${eBenefAcc} != Actual: ${instructionBeneficiaryAccountScreen}
                            ${insReconcileStatus}  Set Variable  ${False}
                        END
                        #----------------------------------------------
                        IF  '${amountConverted}' == '${instructionAmountScreen}'
                            Log To Console    Instruction Payment Amount Pass - Expect: ${amountConverted} == Actual: ${instructionAmountScreen}
                        ELSE
                            Log To Console    Instruction Payment Amount Fail - Expect: ${amountConverted} != Actual: ${instructionAmountScreen}
                            ${insReconcileStatus}  Set Variable  ${False}
                        END



                        # Click to View Instruction Detail
                        C - Click Element By Javascript  (//*[@id='gridTable']/tbody/tr/td[3]/a)[${insIterator}]
                        Wait Until Element Is Visible  ${CMS_Ins_Title}
                        ${debitAccount}  C - Get Text  ${CMS_Ins_DebitAcc_Field}
                        Log To Console    Debit Account: ${debitAccount}
                        Write Cell  ${insWorksheet}  ${insIteratorRow}  5  ${debitAccount}
                        #Write Cell  ${txnWorksheet}  ${insIteratorRow}  5  ${debitAccount}
                        C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Instruction_Detail_${instructionRefScreen}  # Capture screen
                        C - Click Element  ${CMS_Ins_Back_Button}
                    END

                    IF  '${insReconcileStatus}' == '${True}'
                        Write Cell  ${insWorksheet}  ${insIteratorRow}  8  Pass!!
                    ELSE
                        Write Cell  ${insWorksheet}  ${insIteratorRow}  8  Fail!!
                       
                    END

                    ${insIterator}  Evaluate  ${insIterator} + 1
                      
                END
                F_CASH_Get Data Product Config  ${eProduct}
                
                #--------------------------------------------------------------------------
                #   Submit Transaction | Maker
                #--------------------------------------------------------------------------
                IF  '${autoSubmitData}' == 'N' and '${instructionStatusScreen}' == 'For Submit'
                    C - Click Element  ${CMS_Batch_Submit_All_Button}  # Click Submit All Button
                    C - Click Element  ${CMS_Batch_Submit_All_OK_Button}  # Click Ok Button for Confirm to Submit All
                    Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                    ${instructionMessageScreen}   C - Get Text  //*[@id="messageArea"]/h3/b
                    Log To Console  ${instructionMessageScreen}
                    C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Submit_Inst_${instructionRefScreen}  # Capture screen
                    C - Click Element  ${CMS_Batch_Back_Button}
                    Exit For Loop
                ELSE
                    Log To Console  By pass Submit Process | Actual status == ${instructionStatusScreen}
                END
                C - Click Element  ${CMS_Batch_Back_Button}
                Exit For Loop 
            END
            
        END   
        #${batchIteratorRow}  Evaluate  ${batchIteratorRow} + 1
        
    END
   
    C - Click Element  ${CMS_Navbar_Logout_Btn}

    #-------------------------------------------------
    # Checker User Process
    #-------------------------------------------------
    ${batchIterator}  Set Variable  ${1}
    ${insIteratorRow}  Set Variable  ${1}
    ${batchIteratorRow}  set Variable  ${1}
    ${eBatchArrayY}   filter_json_with_cell_detail  ${eChanelDataTxn}   Run_Flag  'Y'
    ${uniqueBatch}     get_unique_from_json_array_by_cellDetail  ${eBatchArrayY}   DeclarationNo  #get product in data
    
    Log To Console  Batch No.: ${uniqueBatch}
     

    FOR  ${batchName}  IN  @{uniqueBatch}
        
        ${batchArray}  filter_json_with_cell_detail  ${eChanelDataTxn}    DeclarationNo    ${batchName}  # Filter DeclarationNo data by Batch Array

        FOR  ${txn}  IN  @{batchArray}

            ${reconcileBatchStatus}  Set Variable  ${True}  # Set default status as True (Success)
            ${batchIteratorRow}  Evaluate  ${batchIteratorRow} + 1  # Set row for write data to excel
            ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1  # Set row for write data to excel
            #------ get data in array ---------
            ${Product_Type}     Set Variable  ${txn}[bankProduct][value]
            ${batchName}        Set Variable  ${txn}[DeclarationNo][value]
            
            #Log To Console  Bank Product : ${Product_Type}
            Log To Console  Batch name : ${batchName}
            Log To Console  Inst row : ${insIteratorRow}

            F_CASH_Get Data Product Config  ${Product_Type}
            
            IF  '${autoAuthData}' == 'Y'
                Log To Console  By pass Auth Process | Auto Auth Flag = ${autoAuthData}

            ELSE IF  '${autoAuthData}' == 'Y' and '${autoSend}' == 'N'   #-- In case Auto auth but require send to bank--
                Log To Console  Go to Send Process | Auto Auth Flag = ${autoAuthData}
                Log To Console  เริ่มเปิดเว็บไซต์ Cashlink
                F_CASH_Open_Cashlink_Website
                F_CASH_Login Cashlink Checker
                Go To  https://uat2.krungsribizonline.com/GCPCW/showPaymentsListBatch.form

                ${countBatchTable}  Get Element Count  ${CMS_Batch_Table}  # Count table of batch screen
                              
                FOR  ${i}  IN RANGE  1  ${countBatchTable}
                    ${batch}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${i}] 

                    IF  '${batchName}' == '${batch}'
                        Log To Console  Currect Batch go to Authorize!
                        ${totalInst}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[8])[${i}]
                        ${product}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[6])[${i}] 
                        ${totalAmt}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${i}]
                        ${batchStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[11])[${i}]
                        
                        C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[3]/a[@title="View Deposit"])[${i}]  #click view instruction 
                        ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[1]   # Only row 1 becuse 1 batch 1 txn.
                        ${insIterator}  Set Variable  ${1}
                        ${productStr}  Set Variable  ${product.strip()} 
                        Log To Console  Bank Product : ${productStr}
                    
                        #--------------------------------------------------------------------------
                        #   Send Transaction | Checker
                        #--------------------------------------------------------------------------
                        IF  '${autoSend}' == 'N' and '${instructionStatusScreen}' == 'For Send'
                            C - Click Element  ${CMS_Send_Btn}
                            C - Click Element  ${CMS_SendAll_Confirm} 
                            Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                            ${instructionMessageScreen}   C - Get Text  //*[@id="messageArea"]/h3/b
                            Log To Console  ${instructionMessageScreen}
                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Send_Inst_${instructionRefScreen}  # Capture screen
                            Log To Console  excel row = ${insIteratorRow}
                            # Write Datato Excel
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  7  ${instructionStatusScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  9  ${instructionMessageScreen}
                        ELSE
                            Log To Console  By pass SendToBank Process | Actual status == ${instructionStatusScreen}
                        END
                        C - Click Element  ${CMS_Batch_Back_Button}
                        #${insIteratorRow}  Evaluate  ${insIteratorRow} + 1
                        Exit For Loop  
                    END

                    
                    
                END   
                C - Click Element  ${CMS_Navbar_Logout_Btn}
            ELSE
                Log To Console  Go to Auth Process | Auto Auth Flag = ${autoAuthData}
                Log To Console  เริ่มเปิดเว็บไซต์ Cashlink
                F_CASH_Open_Cashlink_Website
                F_CASH_Login Cashlink Checker
                Go To  https://uat2.krungsribizonline.com/GCPCW/showPaymentsListBatch.form

                ${countBatchTable}  Get Element Count  ${CMS_Batch_Table}  # Count table of batch screen
                                    
                FOR  ${i}  IN RANGE  1  ${countBatchTable}
                    ${batch}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[5])[${i}] 

                    IF  '${batchName}' == '${batch}'
                        Log To Console  Currect Batch go to Authorize!
                        ${totalInst}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[8])[${i}]
                        ${product}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[6])[${i}] 
                        ${totalAmt}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[10])[${i}]
                        ${batchStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[11])[${i}]
                        
                        C - Click Element    (//*[@id="gridTable"]/tbody/tr/td[3]/a[@title="View Deposit"])[${i}]  #click view instruction 
                        ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[1]   # Only row 1 becuse 1 batch 1 txn.
                        ${insIterator}  Set Variable  ${1}
                       
                        #--------------------------------------------------------------------------
                        #   Authorize and Send Transaction | Checker
                        #--------------------------------------------------------------------------
                        IF  '${autoAuthData}' == 'N' and '${instructionStatusScreen}' == 'For Auth' 
                            C - Click Element  ${CMS_Auth_Btn}
                            C - Click Element  ${CMS_Auth_Confirm_Btn}
                            Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                            ${instructionMessageScreen}   C - Get Text  //*[@id="messageArea"]/h3/b
                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Auth_Inst_${instructionRefScreen}  # Capture screen
                            Log To Console  ${instructionMessageScreen}
                            Log To Console  excel row = ${insIteratorRow}
                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[1]   # Only row 1 becuse 1 batch 1 txn.
                            # Write Datato Excel
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  7  ${instructionStatusScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  9  ${instructionMessageScreen}

                            # ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1

                        ELSE IF  '${autoAuthData}' == 'N' and '${instructionStatusScreen}' == 'For My Auth' 
                            C - Click Element  ${CMS_Auth_Btn}
                            C - Click Element  ${CMS_Auth_Confirm_Btn}
                            Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                            ${instructionMessageScreen}   C - Get Text  //*[@id="messageArea"]/h3/b
                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Auth_Inst_${instructionRefScreen}  # Capture screen
                            Log To Console  ${instructionMessageScreen}
                            Log To Console  excel row = ${insIteratorRow}
                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[1]   # Only row 1 becuse 1 batch 1 txn.
                            # Write Datato Excel
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  7  ${instructionStatusScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  9  ${instructionMessageScreen}

                            # ${insIteratorRow}  Evaluate  ${insIteratorRow} + 1

                        ELSE
                            Log To Console  By pass Authorize Process | Actual status == ${instructionStatusScreen}
                        END
                        #--------------------------------------------------------------------------
                        #   Send Transaction | Checker
                        #--------------------------------------------------------------------------
                        ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[1]   # Only row 1 becuse 1 batch 1 txn.

                        IF  '${autoSend}' == 'N' and '${instructionStatusScreen}' == 'For Send'
                            C - Click Element  ${CMS_Send_Btn}
                            C - Click Element  ${CMS_SendAll_Confirm} 
                            Element Should Be Visible  ${CMS_Auth_SummitAll_Msg}
                            ${instructionMessageScreen}   C - Get Text  //*[@id="messageArea"]/h3/b
                            C - Capture Full Page Screen by Headless Chrome  ${CMS_Container}  Send_Inst_${instructionRefScreen}  # Capture screen
                            Log To Console  ${instructionMessageScreen}
                            Log To Console  excel row = ${insIteratorRow}
                            ${instructionStatusScreen}    C - Get Text    (//*[@id="gridTable"]/tbody/tr/td[13])[1]   # Only row 1 becuse 1 batch 1 txn.
                            # Write Datato Excel
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  7  ${instructionStatusScreen}
                            Write Cell  ${insWorksheet}  ${insIteratorRow}  9  ${instructionMessageScreen}

                            #${insIteratorRow}  Evaluate  ${insIteratorRow} + 1

                        ELSE
                            Log To Console  By pass SendToBank Process | Actual status == ${instructionStatusScreen}
                        END
                        C - Click Element  ${CMS_Batch_Back_Button}
                        Exit For Loop
                    END
                    
                    
                    
                END   

                C - Click Element  ${CMS_Navbar_Logout_Btn}

            END
        END
    END

 

    

